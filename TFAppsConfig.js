'use strict';
var TFApps = {
    config: {
        // Libraries
        libraries  : {
            webUrl   : '../../Libraries/Web',
            mobileUrl: '../../Libraries/Mobile'
        },
        // Data Requests
        dataRequest: {
            url: '/tfw'
        },

        // SignalR Push Hub
        pushHub    : {
            debug           : false,
            hubConnectionUrl: '/tfw/signalr'
        },

        getResourceUrl: function () {
            var theme = location.href.match(/theme=([\w-]+)/);
                theme = (theme && theme[1]) || 'standard';

            var flag = false;

            if ( TFAppConfig.theme && Ext.typeOf( TFAppConfig.theme.themes ) === 'array' ) {

                Ext.each( TFAppConfig.theme.themes, function ( item ) {

                    if ( item.defaultValue.toLowerCase() === theme ) {
                        theme = item.defaultValue.toLowerCase();
                        flag  = true;
                        // exits from the iterations once the
                        // theme matched to the current condition
                        return false;
                    }

                } );
            }

            if ( !flag ) {
                theme = 'standard';
            }

            return Ext.String.format('../TFThemes/{0}/resources', theme);

            // return Ext.String.format( '{0}/resources', theme );
        }
    }
};