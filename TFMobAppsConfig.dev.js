var TFApps = {
    hello : 'world',
    config: {
        loginApp   : 'TFM.Login',
        // Libraries
        libraries  : {
            webUrl   : '../../Libraries/Web',
            mobileUrl: '../../Libraries/Mobile'
        },
        // Data Requests
        dataRequest: {
            url: '/Apps'
        },
        // SignalR Push Hub
        pushHub    : {
            debug: true
        }
    }
};