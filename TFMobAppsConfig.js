var TFApps = {
    config    : {
        // Libraries
        libraries: {
            webUrl: '../../Libraries/Web',
            mobileUrl: '../../Libraries/Mobile'
        },
        // Data Requests
        dataRequest: {
            url: '/tfw'
        },
        // SignalR Push Hub
        pushHub: {
            debug: false
        }
    }
};