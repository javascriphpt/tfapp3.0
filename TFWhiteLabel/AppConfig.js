'use strict';
window.AppConfig = {
    appTitle          : 'White Label - Risk Monitor',
    appHeaderTitle    : 'Client Monitor',
    defaultHeaderCls  : '',

    defaultHeader: {
        cls  : 'tfcmonptrader',
        key  : 'tfcmon.htxt.ptrader',
        value: 'Web Front'
    },

    appNameSpace: 'TFWhiteLabel',
    AppCode     : 'TFWL.RiskMon',
    AppName     : 'TFWL.RiskMon',
    AppId       : 300,

    features    : {
        SignalR: true
    },

    // Locale key type to load
    // localeKeyType   : "'TF.WebApp','TF.Alert','TF.Common'",

    // roles: {
    //     MenuTypes: ['TF.WebApp', 'TF.Web']
    // },

    preferences: {
        showAlertPriority: false,
        // 8 => Order/Fill updates
        defaultValueItemIds: [8]
    },

    refreshSetting: {
        autoRefreshActSmyHeatMap: 2000
    },

    // Grids
    // Config for grids
    grids: {
        defaults: {
            // For updating the store records
            // true to use SignalR push data
            usePushData: false
        },
        // Market Prices grid
        MarketPrices: {
            usePushData: true
        },

        MarketDepth : {
            usePushData : true
        }
    },

    // @overrided the parent preferenceType config
    preferenceType: {
        8: 'orderUpdate'
    },

    // Enable centralised notification manager
    notificationManager: false,

    getApp: function () {
        return eval( this.appNameSpace );
    }
};

window.TFAppConfig = Object.assign(CommonConfig, AppConfig);