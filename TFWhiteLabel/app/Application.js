/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */

Ext.Loader.setConfig({
    enabled: true,
    disableCaching: true
});

Ext.Loader.loadScript( TFApps.config.libraries.webUrl + '/async/async.js' );
Ext.Loader.loadScript( TFApps.config.libraries.webUrl + '/Scripts/jquery-1.6.4.js' );
Ext.Loader.loadScript( TFApps.config.libraries.webUrl + '/Scripts/jquery.signalR-2.1.2.js' );
Ext.Loader.loadScript( TFApps.config.libraries.webUrl + '/soundmanager2/soundmanager2-nodebug-jsmin.js' );

Ext.Loader.loadScript( {
    url: '//d3js.org/d3.v3.min.js',
    onError: function (){
        
        alert('Error occured when downloading graphical heatmap. Reloading the page.');

        setTimeout( function () {
            window.location.reload(true);
        }, 1000 );
    }
} );

Ext.define('TFWhiteLabel.Application', {
    extend: 'Ext.app.Application',
    
    name: 'TFWhiteLabel',

    views : [
        'main.Main',
        'login.Login',

        'app.AppMain',

        'whitelabel.WhiteLabel',
        'whitelabel.monitor.Monitor',
        'whitelabel.monitor.MainContainerWrap',
        'whitelabel.monitor.GraphRiskMonitor'
    ],

    controllers : [
        'Main',
        'Locale',
        'Session',
        'User',
        'Preferences',
        'Permission',
        'PushHubMgr',
        'Connection',
        'Notifications',
        'GridPreferences',

        'whitelabel.WhiteLabel'
    ],

    stores: [
        // TODO: add global / shared stores here
        'GetUser',
        'Notifications',
        'PreferenceType',
        'PushHubs',
        'User',
        'UserActions',
        'UserMenu',
        'UserPreferences'
    ],
    
    launch: function () {
        // TODO - Launch the application
    },

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
