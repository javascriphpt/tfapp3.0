'use strict';
Ext.define('TFWhiteLabel.controller.GridPreferences', {
    extend: 'TFCommon.controller.GridPreferences',

    appStoreNamesSortSave: [
    	'clientMon.LedgerList',
    	'user.OrderHistory',
    	'user.Bookinglist',
    	'user.WorkingOrders',
    	'user.VirtualPosition',
    	'user.FillItem'
    ]
});