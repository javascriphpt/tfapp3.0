'use strict';
Ext.define('TFWhiteLabel.controller.Preferences', {
    extend: 'TFCommon.controller.Preferences',

    getAppName: function () {
        return TFAppConfig.AppName.substr(3);
    },

    createDefaultPreferences: function () {
        // for notification only
        this.createDefaultNotificatonPreference();
    },

    createDefaultNotificatonPreference: function () {

        var defaultValues   = TFAppConfig.preferences.defaultValueItemIds;
        var notificationCfg = this.getPreference( 'appnotifconfig' );
        var flag            = false;
        var name            = this.getAppName();
        var that            = this;
        var itemIds;


        // Immediately stop the process when userModified = true
        if ( notificationCfg && notificationCfg.modifiedApps &&
                notificationCfg.modifiedApps[ name ] === true ) {

            this._cb( null );
            return false;
        }

        // Checks if notificationCfg has modifiedApps, if not, then inject the property to prevent error
        if ( notificationCfg && !notificationCfg.modifiedApps ) {
            notificationCfg.modifiedApps = {};
        }

        // Checks if there is a notification cfg and wether the property modifiedApps is exist or set to false
        if ( notificationCfg && !notificationCfg.modifiedApps[ name ] ) {

            itemIds = notificationCfg.itemId;

            // Initialize the userModified if not set for future reference
            if ( !notificationCfg.modifiedApps[ name ] ) {
               // Sets the app name for reference if modified or not
                notificationCfg.modifiedApps[name] = false;
            }

            Ext.each( defaultValues, function ( itemId ) {
                if ( Ext.Array.indexOf( itemIds, itemId ) === -1 ) {
                    // Sets the flag to determine that there was a changes
                    flag = true;

                    itemIds.push( itemId );
                }
            } );

        } else if ( !notificationCfg ) {
            // default notification config
            notificationCfg = {
                itemId            : [],
                playSound         : 0,
                modifiedApps      : {},
                showPopup         : null,
                orderConfirmation : {
                    show          : true
                }
            };

            // Sets the app name for reference if modified or not
            notificationCfg.modifiedApps[name] = false;

            // Sets the flag to determine that there was a changes
            flag = true;

            // iterate defaults values for app
            Ext.each( defaultValues, function ( itemId ) {
                notificationCfg.itemId.push( itemId );
            } );
        }


        if ( flag === true ) {
            // Save the new notificatoin configuration
            this.saveUserPreferences( 2, 'appnotifconfig', notificationCfg, true, function () {
                that.loadStore( that._cb, 'appnotifconfig' );
            } );
        } else {
            this._cb( null );
        }
    },

    //override user preferences
    saveUserPreferences : function( ) {

        var that = this,
            timeout;

        this.superclass.saveUserPreferences.apply( this, arguments );

        // timeout = setTimeout( function() {
        //     that.getController( 'clientMon.ClientMon' ).triggerOrdConfIndicator();
        //     clearTimeout( timeout );
        // }, 1000);

        
    }

});
