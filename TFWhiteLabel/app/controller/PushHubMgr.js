'use strict';

Ext.define( 'TFWhiteLabel.controller.PushHubMgr', {
	extend : 'TFCommon.controller.PushHubMgr',

	// override Add to subscription collection
    addToSubscriptionCollection: function (subscriptionConfig) {
        var key,
            isAddedToCollection = false,
            rec;
        // Check if the data request is already on the colleciton

        // if subscription type is for market depth, key will be dataRequestId-InstrumentId

        if( subscriptionConfig.subscriptionType === 'MarketDepth' ) {

        	key = parseInt( subscriptionConfig.dataRequestId + '' + subscriptionConfig.storeIdOrComponentId, 10 );

        	if (!this.subcriptionCollection.containsKey( key )) {
	            // Add to collection
	            // Serves as a copy so subscription can be subscribed again when disconnected

	            this.subcriptionCollection.add( key, subscriptionConfig );

                isAddedToCollection = true;
	        }



        } else {
        	if (!this.subcriptionCollection.containsKey(subscriptionConfig.dataRequestId)) {
	            // Add to collection
	            // Serves as a copy so subscription can be subscribed again when disconnected
	            this.subcriptionCollection.add(subscriptionConfig.dataRequestId, subscriptionConfig);
                isAddedToCollection = true;
	        }
        }

        // Check if PushHub is connected
        // If true, then invoke subscribe
        if (this.connectionStatus === 'connected') {

            if( isAddedToCollection ) {
                this.doSubscribe(subscriptionConfig);
            }
        }
    },

    noPermissionToSubscribe : function( subscriptionConfig ) {
        var dataRequesName = '',
            message,
            code;

        code = subscriptionConfig.dataRequestId;

        switch( subscriptionConfig.dataRequestId ) {
            case 515 : 
                dataRequesName = 'Market Depth Push Data';
                break;
        }

        message = TFCommon.util.Exception.logError(
            TFAppConfig.error.msgView.STATUS,
            code,
            'User does not have permission to subscribe ',
            dataRequesName
        );

        //this.getController( 'clientMon.ClientMon' ).showHeaderMsg( message, 'warning' );
    },

    // override Subscribe success callback
    // @subscriptionConfig - subscription config
    subscribeSuccessCb: function (subscriptionConfig) {
        var that = this;
        // Returns a function that the SignalR can invoke
        return function () {
            console.log( 'Subscribe Sucess DataRequestId: ' + subscriptionConfig.dataRequestId );
            // Update the connection status of this subscription
            
            that.invokeCustomCallbacks( that, subscriptionConfig);

        };
    },

    // Subscribe error callback
    // @subscriptionConfig - subscription config
    subscribeErrorCb: function (subscriptionConfig) {
        var that = this;
        // Returns a function that the SignalR can invoke
        return function (response) {
            console.log( 'Subscribe ERROR DataRequestId: ' + subscriptionConfig.dataRequestId );
            console.log( response );

            that.invokeCustomCallbacks( that, subscriptionConfig, false);
        };
    },

    invokeCustomCallbacks : function( scope, subscriptionConfig, success ) {
        var that = scope,
            key,
            record;

        success = success || true;

        if( subscriptionConfig.subscriptionType === 'MarketDepth' ) {

            key = parseInt( subscriptionConfig.dataRequestId + '' + subscriptionConfig.storeIdOrComponentId, 10 );

            record = that.pushHubStore.findRecord('storeIdOrComponentId', subscriptionConfig.storeIdOrComponentId);
            record.set('subscribed', success, {dirty: false});

            if( success ) {
                if (record.get('subscribed') === true && that.subcriptionCollection.get( key ).connectCallback) {
                    that.subcriptionCollection.get( key ).connectCallback( record );
                }    
            } else {
                if (record.get('subscribed') === false && that.subcriptionCollection.get( key ).errorCallback) {
                    that.subcriptionCollection.get( key ).errorCallback( record );
                }
            }
            

        } else {
            record = that.pushHubStore.findRecord('dataRequestId', subscriptionConfig.dataRequestId);
            record.set('subscribed', success, {dirty: false});

            if( success ) {
                if (record.get('subscribed') === true && that.subcriptionCollection.get(record.get('dataRequestId')).connectCallback) {
                    that.subcriptionCollection.get(record.get('dataRequestId')).connectCallback( record );
                }

                if( record.get('subscribed') === true ) {
                    that.fireEvent( 'pushMessageSubscribed', record );
                }
            } else {
                if (record.get('subscribed') === false && that.subcriptionCollection.get(record.get('dataRequestId')).errorCallback) {
                    that.subcriptionCollection.get(record.get('dataRequestId')).errorCallback( record );
                }
            }
        }
    },

    // Invoke the disconnect callbacks on each subscriptions
    disconnectSubscriptionCallback: function () {
    	var key;

        this.pushHubStore.each(function (record) {
            // Check if connectfed
            // Check if there's a disconnect callback

            if( record.get('subscriptionType') === 'MarketDepth' ) {

            	key = parseInt( record.get('dataRequestId') + '' + record.get('storeIdOrComponentId'), 10 );

            	if (record.get('subscribed') === true && this.subcriptionCollection.get( key ).disconnectCallback) {
	                // Invoke the disconnect callback
	                this.subcriptionCollection.get( key ).disconnectCallback();
	            }
            } else {
            	if (record.get('subscribed') === true && this.subcriptionCollection.get(record.get('dataRequestId')).disconnectCallback) {
	                // Invoke the disconnect callback
	                this.subcriptionCollection.get(record.get('dataRequestId')).disconnectCallback();
	            }
            }

        }, this);
    },

    // override Unsubscribe success callback
    // @pushHubStore - reference for the pushHubStore
    // @rec - PushHub model to remove
    unsubscribeSuccessCb: function (rec) {
        var that = this;
        var key;

        return function (response) {

            // reference for specific subscription collection
            var subscriptionRec = that.getSubscriptionCollectionRec( rec );

            console.log( '===========' );
            console.log( 'unusbscribe to signal ( ' + rec.get( 'dataRequestId' ) + ', ' + rec.getId() + ' ) ' );
            console.log(response);
            console.log( '===========' );

            // Delete callback
            delete that.subscriptionKeyToCallback[rec.get('subscriptionId')];

            // Remove from store
            that.pushHubStore.remove( rec );

            // if( rec.get('subscriptionType') === 'MarketDepth' ) {
            //     key = parseInt( rec.get('dataRequestId') + '' + rec.get('storeIdOrComponentId'), 10 );

            //     console.log('### remove from collection for market depth', that.subcriptionCollection);

            //     that.subcriptionCollection.remove( that.subcriptionCollection.get( key ) );
            // }
            // remove from subscription collection
            
            if ( subscriptionRec ) {
                // remove from subscription collection
                that.subcriptionCollection.remove( subscriptionRec );    
            }

            // notifies the views that listens on the event name @unSubscribePushMessage
            that.fireEvent( 'unSubscribePushMessage', rec );
            console.log( '@fired eventname unSubscribPUshMessage from pushHubMgr');
        };
    }
} );