'use strict';
Ext.define( 'TFWhiteLabel.controller.User', {
    extend: 'TFCommon.controller.User',
    init  : function () {

        this.UserViewsCb = [];
        // Client Monitor views
        this.UserViewsCb.push( this.getController( 'whitelabel.WhiteLabel' ).getUserViewsCb() );
        // use superclass instead of callParent to avoid errors due to use strict
        this.superclass.init.call(arguments);
    },

    userHasMenu   : function ( menu ) {
        var menuTypes = TFAppConfig.roles.MenuTypes;
        var rec = Ext.getStore( 'UserMenu' ).findRecord( 'MenuItemCode', menu );

        if ( rec && Ext.Array.contains( menuTypes, rec.get( 'MenuType' )) ) {
            return true;
        } else {
            return false;
        }
    },

    // Finds a user menu
    // @menu - the menu to find
    // @return - null if no record found. UserMenu record if found
    findUserMenu  : function ( menu ) {
        var menuTypes = TFAppConfig.roles.MenuTypes;
        var rec      = Ext.getStore( 'UserMenu' ).findRecord( 'MenuItemCode', menu );

        if ( rec && Ext.Array.contains( menuTypes, rec.get( 'MenuType' )) ) {
            return rec;
        } else {
            return null;
        }
    }
} );
