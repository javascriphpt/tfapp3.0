'use strict';

Ext.define( 'TFWhiteLabel.controller.whitelabel.WhiteLabel', {
	extend : 'Ext.app.Controller',
	id     : 'whitelabel.WhiteLabel',

	refs : [
		{
            ref     : 'appContainer',
            selector: '#app-subContainer'
        }
	],

	/**
	 * Get views for the application
	 * @version 3.0
	 * @return {Objec} array of views/functions
	 */
	getUserViewsCb: function () {

        var that    = this;
        var viewsCb = [

            function (){
                that.createMainAppView.call( that );
            }

        ];

        return viewsCb;

    },

    /**
     * Setup Views
     * @version 3.0
     */
    createMainAppView: function () {

        this.getAppContainer().add( {
            xtype: 'app-whitelabel-main'
        } );

    },
});