Ext.define( 'TFWhiteLabel.model.riskmon.ActAdvisor', {
    extend: 'Ext.data.Model',
    fields: [
        'TrdActId', 
        'LglEntityId', 
        'RatingClsId', 
        'LongPos', 
        'ShortPos', 
        'MaxLoss', 
        'OneShotOrderLimit', 
        'TodayRealPL', 
        'ActStatusId'
    ]
} );