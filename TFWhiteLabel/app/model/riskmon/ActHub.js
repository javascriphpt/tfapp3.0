Ext.define( 'TFWhiteLabel.model.riskmon.ActHub', {
    extend: 'Ext.data.Model',
    fields: [
        'ProcessId', 
        'MessageHubId', 
        'MessageHubCode'
    ]
} );