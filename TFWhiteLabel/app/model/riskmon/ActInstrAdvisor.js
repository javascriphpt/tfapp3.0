Ext.define( 'TFWhiteLabel.model.riskmon.ActInstrAdvisor', {
    extend: 'Ext.data.Model',
    fields: [
        'TrdActId', 
        'RatingClsId', 
        'ProductId', 
        'InstrClsId', 
        'InstrumentId', 
        'LongPos', 
        'ShortPos', 
        'WrkBuyOrders', 
        'WrkSellOrders', 
        'PendBuyOrders', 
        'PendSellOrders', 
        'TrdBuyLots', 
        'TrdSellLots', 
        'AvgBuyPrice', 
        'AvgSellPrice', 
        'TodayRealPL'
    ]
} );