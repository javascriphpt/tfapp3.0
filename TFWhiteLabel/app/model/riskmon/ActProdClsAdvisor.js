Ext.define( 'TFWhiteLabel.model.riskmon.ActProdClsAdvisor', {
    extend: 'Ext.data.Model',
    fields: [
        'TrdActId', 
        'RatingClsId', 
        'ProductId', 
        'InstrClsId', 
        'LongPos', 
        'ShortPos', 
        'TodayRealPL'
    ]
} );