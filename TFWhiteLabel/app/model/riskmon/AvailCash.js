Ext.define( 'TFWhiteLabel.model.riskmon.AvailCash', {
    extend: 'Ext.data.Model',
    fields: [
        'TrdActId', 
        'TrdActCode', 
        'TrdActName', 
        'LglEntityId', 
        'SalesGrpId', 
        'SalesGrpCode', 
        'SalesGrpName', 
        'SalesGrpTypeCode', 
        'CashBalSOD', 
        'PostPlExp', 
        'PostCash', 
        'CashBalEOD', 
        'AvailCash', 
        'ParentActId'
    ]
} );