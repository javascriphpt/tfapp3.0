Ext.define( 'TFWhiteLabel.model.riskmon.BlazeActStatusType', {
    extend: 'Ext.data.Model',
    fields: [
        'BlazeActStatusTypeId', 
        'BlazeActStatusTypeCode', 
        'BlazeActStatusTypeName'
    ]
} );