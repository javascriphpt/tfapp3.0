Ext.define('TFWhiteLabel.model.riskmon.CancelOffset', {
    extend: 'Ext.data.Model',
    fields: [
	    'TrdActId',
	    'TrdActCode',
	    'TrdActName',
	    'InstrumentId',
	    'InstrumentCode',
	    'InstrumentName',
	    'VenueId',
	    'VenueCode',
	    'VenueName',
	    'SalesGrpId',
	    'SalesGrpCode',
	    'SalesGrpName',
	    'SellOrdLots',
	    'BuyOrdLots',
	    'PosShortLots',
	    'PosLongLots'
    ]
});