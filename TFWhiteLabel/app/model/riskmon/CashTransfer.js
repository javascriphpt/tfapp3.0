Ext.define( 'TFWhiteLabel.model.riskmon.CashTransfer', {
    extend: 'Ext.data.Model',
    fields: [
        'TradeDate', 
        'PostingDate', 
        'FinTransTypeName', 
        'Deposit', 
        'Withdrawal', 
        'ReqWithdrawal', 
        'CashDeleted', 
        'SavedTime', 
        'TrdActId', 
        'ParentLevel', 
        'SalesGrpId', 
        'SalesGrpTypeCode', 
        'ParentActId', 
        'FinTransId', 
        'AllowDelete'
    ]
} );