Ext.define( 'TFWhiteLabel.model.riskmon.InstrCls', {
    extend: 'Ext.data.Model',
    fields: [
        'InstrClsId', 
        'InstrClsCode', 
        'InstrClsName'
    ]
} );