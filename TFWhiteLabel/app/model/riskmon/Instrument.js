Ext.define( 'TFWhiteLabel.model.riskmon.Instrument', {
    extend: 'Ext.data.Model',
    fields: [
        'InstrumentId', 
        'InstrumentCode', 
        'InstrumentName'
    ]
} );