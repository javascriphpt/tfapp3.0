Ext.define( 'TFWhiteLabel.model.riskmon.Ledger', {
    extend: 'Ext.data.Model',
    fields: [
        'TrdActId', 
        'TrdActName', 
        'LedgerTypeId', 
        'LedgerTypeCode', 
        'LedgerTypeName', 
        'ProductCode', 
        'Amount', 
        'SortSeq', 
        'ParentActId', 
        'SalesGrpId', 
        'SalesGrpTypeCode'
    ]
} );