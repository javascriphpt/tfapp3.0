Ext.define( 'TFWhiteLabel.model.riskmon.OrderHistorySmy', {
    extend: 'Ext.data.Model',
    fields: [
       'TrdActId',
       'LglEntityName',
       'RegGrpName',
       'TrdActCode',
       'TrdActName',
       'NumOrders',
       'NumFills',
       'OrderQuantity',
       'FillQuantity'
    ]
} );