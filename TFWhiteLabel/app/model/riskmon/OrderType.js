Ext.define( 'TFWhiteLabel.model.riskmon.OrderType', {
    extend: 'Ext.data.Model',
    fields: [
	    'VenueId',
	    'OrdTypeId',
	    'OrdTypeCode',
	    'OrdTypeName'
    ]
} );