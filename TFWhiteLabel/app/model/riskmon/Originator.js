Ext.define( 'TFWhiteLabel.model.riskmon.Originator', {
    extend: 'Ext.data.Model',
    fields: [
        'OriginatorId',
        'OriginatorCode',
        'OriginatorName'
    ]
} );