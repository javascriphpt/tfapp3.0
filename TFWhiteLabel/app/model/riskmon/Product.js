Ext.define( 'TFWhiteLabel.model.riskmon.Product', {
    extend: 'Ext.data.Model',
    fields: [
        'ProductId', 
        'ProductCode', 
        'ProductName'
    ]
} );