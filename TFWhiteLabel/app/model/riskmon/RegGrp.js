Ext.define( 'TFWhiteLabel.model.riskmon.RegGrp', {
    extend: 'Ext.data.Model',
    fields: [
        'RegGrpId',
        'RegGrpCode',
        'RegGrpName',
        {
            name    : 'RegGrp',
            convert : function( val, rec ) {
                if( TFCommon.Locales.showByName() ) {
                    return rec.get( 'RegGrpName' );
                } else {
                    return rec.get( 'RegGrpCode' );
                }
            }
        }
    ]
} );