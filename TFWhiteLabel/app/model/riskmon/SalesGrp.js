Ext.define( 'TFWhiteLabel.model.riskmon.SalesGrp', {
    extend: 'Ext.data.Model',
    fields: [
        'SalesGrpId', 
        'SalesGrpTypeCode', 
        'SalesGrpCode', 
        'SalesGrpName', 
        'PosShortLots', 
        'PosLongLots', 
        'BuyTrdLots', 
        'SellTrdLots', 
        'Comm'
    ]
} );