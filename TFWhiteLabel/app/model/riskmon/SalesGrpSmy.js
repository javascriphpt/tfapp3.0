'use strict';
Ext.define( 'TFWhiteLabel.model.riskmon.SalesGrpSmy', {
    extend: 'Ext.data.Model',
    fields: [
        'SalesGrpId',
        'SalesGrpTypeCode',
        'SalesGrpCode',
        'SalesGrpName',
        {
            name    : 'SalesGrp',
            convert : function( val, rec ) {
                if( TFCommon.Locales.showByName() ) {
                    return rec.get( 'SalesGrpName' );
                } else {
                    return rec.get( 'SalesGrpCode' );
                }
            }
        },
        'PosShortLots',
        'PosLongLots',
        'BuyTrdLots',
        'SellTrdLots',
        'Comm'
    ]
} );
