Ext.define( 'TFWhiteLabel.model.riskmon.SalesGrpType', {
    extend: 'Ext.data.Model',
    fields: [
        'SalesGrpTypeCode', 
        'PosShortLots', 
        'PosLongLots', 
        'BuyTrdLots', 
        'SellTrdLots'
    ]
} );