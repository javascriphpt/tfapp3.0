Ext.define( 'TFWhiteLabel.model.riskmon.TradeValue', {
    extend: 'Ext.data.Model',
    fields: [
        'ParentActId', 
        'TrdActId', 
        'TrdActCode', 
        'TrdActName', 
        'InstrumentName', 
        'VenueId', 
        'ProductId', 
        'InstrumentId', 
        'SalesGrpId', 
        'SalesGrpTypeCode', 
        'BuyLots', 
        'SellLots', 
        'BuyVal', 
        'SellVal', 
        'AvgBuyPrc', 
        'AvgSellPrc', 
        'Prc', 
        'Chg'
    ]
} );