Ext.define( 'TFWhiteLabel.model.riskmon.WorkingOrderSmy', {
    extend: 'Ext.data.Model',
    fields: [
        'TrdActId',
        'TrdActCode',
        'TrdActName',
        'WorkingOrders',
        'WorkingBuyQty',
        'WorkingSellQty',
        'PendingOrders',
        'InjectorProcessId'
    ]
} );