'use strict';
Ext.define( 'TFWhiteLabel.model.widget.LegalEntity', {
    extend: 'Ext.data.Model',
    fields: [
    	'LglEntityId',
        'LglEntitySeq',
        'LglEntityCode',
        'LglEntityName',
        'LglEntityKana',
        'SortSeq',
        'ParentLglEntityId',
        'Address1',
        'Address2',
        'Address3',
        'Address4',
        'Address5',
        'EmailAddress',
        'TelNo',
        'ExtRef1',
        'ExtRef2',
        'LglEntityTypeId',
        'LglEntityTypeCode',
        'LglEntityTypeName',
        'RestrictOneUser',
        'RestrictOneActPerRegGrp',
        'IsMetaTrdAct',
        'RequiresBankAct',
        'DefTrdActTypeId',
        'DefUserRoleId'
    ]
    
});