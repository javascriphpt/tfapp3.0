'use strict';
Ext.define( 'TFWhiteLabel.model.widget.TradeAct', {
    extend: 'Ext.data.Model',
    fields: [
    	'ItemType',
        'TrdActId',
        'LglEntityId',
        'TrdActCode',
        'ItemName'
    ]
    
});