'use strict';
Ext.define( 'TFWhiteLabel.model.widget.UserAct', {
    extend: 'Ext.data.Model',

    fields : [
    	'ParentLglEntityId',
		'LglEntityId',
		'UserId',
		'UserCode',
		'UserName',
		'UserRoleId',
		'UserRoleCode',
		'UserRoleName',
		'SortSeq',
		'Email',
		'LocaleId',
		'LocaleCode',
		'TimezoneId',
		'TimezoneName',
		'UserSeq',
		'LglEntityCode',
		'LglEntityName',
		'LglEntityKana'
    ]
    
});