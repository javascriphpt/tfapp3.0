Ext.define( 'TFWhiteLabel.store.riskmon.ActAdvisor', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.ActAdvisor',
    alias       : 'store.actAdvisorStore',
    remoteSort  : true,
    remoteFilter: true,
    autoLoad    : false,
    pageSize    : -1,
    sorters: {
        property: 'TrdActId',
        direction: 'ASC'
    },
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 601
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }
} );