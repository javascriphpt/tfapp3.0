Ext.define( 'TFWhiteLabel.store.riskmon.ActHub', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.ActHub',
    alias       : 'store.actHubStore',
    autoLoad    : false,
    pageSize    : -1,
    sorters: {
        property: 'TrdActId',
        direction: 'ASC'
    },
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 158
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }
} );