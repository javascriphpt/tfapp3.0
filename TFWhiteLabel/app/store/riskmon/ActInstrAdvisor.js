Ext.define( 'TFWhiteLabel.store.riskmon.ActInstrAdvisor', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.ActInstrAdvisor',
    alias       : 'store.actInstrAdvisorStore',
    remoteSort  : true,
    remoteFilter: true,
    autoLoad    : false,
    pageSize    : -1,
    sorters: {
        property: 'TrdActId',
        direction: 'ASC'
    },
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 608
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }
} );