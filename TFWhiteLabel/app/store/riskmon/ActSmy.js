'use strict';
Ext.define( 'TFWhiteLabel.store.riskmon.ActSmy', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.ActSmy',
    alias       : 'store.actSmyStore',
    remoteSort  : true,
    remoteFilter: true,
    autoLoad    : false,
    pageSize    : 20,
    sorters     : {
        property : 'TrdActId',
        direction: 'ASC'
    },
    proxy       : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 104
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }

} );