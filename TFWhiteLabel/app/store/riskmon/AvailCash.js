Ext.define( 'TFWhiteLabel.store.riskmon.AvailCash', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.AvailCash',
    alias       : 'store.availCashStore',
    remoteSort  : true,
    remoteFilter: true,
    autoLoad    : false,
    pageSize    : -1,
    sorters     : {
        property: 'TrdActCode',
        direction: 'ASC'
    },
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 254
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }
} );