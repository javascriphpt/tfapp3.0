'use strict';
Ext.define( 'TFWhiteLabel.store.riskmon.BlazeActStatusType', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.BlazeActStatusType',
    alias       : 'store.BlazeActStatusTypeStore',
    remoteSort  : true,
    remoteFilter: true,
    autoLoad    : false,
    pageSize    : -1,
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 159
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }

} );