Ext.define( 'TFWhiteLabel.store.riskmon.Booking', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.Booking',
    remoteSort  : true,
    remoteFilter: true,
    autoLoad    : false,
    pageSize    : 50,
    sorters     : {
        property: 'RegGrp',
        direction: 'ASC'
    },
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 303
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }
} );