Ext.define( 'TFWhiteLabel.store.riskmon.BrokerLvl', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.BrokerLvl',
    remoteSort  : true,
    remoteFilter: true,
    autoLoad    : false,
    pageSize    : -1,
    sorters     : {
        property: 'TrdActId',
        direction: 'ASC'
    },
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 105
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }
} );