Ext.define( 'TFWhiteLabel.store.riskmon.CancelOffset', {
    extend    : 'Ext.data.Store',
    model     : 'TFWhiteLabel.model.riskmon.CancelOffset',
    remoteSort: false,
    autoLoad  : false,
    sorters   : [{
        property : 'InstrumentId',
        direction: 'ASC'
    }],
    pageSize     : 25,
    proxy     : {
        type: 'memory'
    }
} );