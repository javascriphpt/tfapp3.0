Ext.define( 'TFWhiteLabel.store.riskmon.CashTransfer', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.CashTransfer',
    alias       : 'store.cashTransferStore',
    remoteSort  : true,
    remoteFilter: true,
    autoLoad    : false,
    pageSize    : -1,
    sorters     : {
        property: 'TradeDate',
        direction: 'ASC'
    },
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 252
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }
} );