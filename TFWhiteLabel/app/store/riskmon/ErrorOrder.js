'use strict';
Ext.define( 'TFWhiteLabel.store.riskmon.ErrorOrder', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.ErrorOrder',
    remoteSort  : true,
    remoteFilter: true,
    autoLoad    : false,
    autoDestroy : true,
    sorters     : {
        property : 'OrdRcvTime',
        direction: 'DESC'
    },
    pageSize    : 50,
    proxy       : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url + '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 763
        },
        reader       : {
            rootProperty   : 'data',
            totalProperty  : 'total',
            messageProperty: 'message',
            successProperty: 'success'
        }
    }
} );