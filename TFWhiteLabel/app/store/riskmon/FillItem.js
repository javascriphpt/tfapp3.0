'use strict';
Ext.define( 'TFWhiteLabel.store.riskmon.FillItem', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.FillItem',
    remoteSort  : true,
    remoteFilter: true,
    sorters     : [{
        property: 'TradeDate',
        direction: 'DESC'
    }],
    // groupField  : 'RegGrpCode',
    autoLoad    : false,
    pageSize    : 25,
    proxy       : {
        type   : 'ajax',
        url    : TFApps.config.dataRequest.url + '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id    : 302,
            filter: ''
        },
        reader       : {
            rootProperty   : 'data',
            totalProperty  : 'total',
            messageProperty: 'message',
            successProperty: 'success'
        }
    }
} );