Ext.define( 'TFWhiteLabel.store.riskmon.InstrCls', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.InstrCls',
    alias       : 'store.instrClsStore',
    remoteSort  : true,
    remoteFilter: true,
    autoLoad    : false,
    pageSize    : -1,
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 163
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }
} );