Ext.define( 'TFWhiteLabel.store.riskmon.Instrument', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.Instrument',
    alias       : 'store.instrumentStore',
    remoteSort  : true,
    remoteFilter: true,
    autoLoad    : false,
    pageSize    : -1,
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 162
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }
} );