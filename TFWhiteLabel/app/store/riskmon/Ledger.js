'use strict';
Ext.define( 'TFWhiteLabel.store.riskmon.Ledger', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.Ledger',
    alias       : 'store.LedgerStore',
    remoteSort  : true,
    remoteFilter: true,
    autoLoad    : false,
    pageSize    : -1,
    sorters: {
        property: 'SortSeq',
        direction: 'ASC'
    },
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 130
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }
} );