'use strict';
Ext.define( 'TFWhiteLabel.store.riskmon.OpenPosition', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.OpenPosition',
    alias       : 'store.OpenPosition',
    remoteSort  : true,
    remoteFilter: true,
    autoLoad    : false,
    pageSize    : 50,
    sorters     : {
        property: 'RegGrp',
        direction: 'ASC'
    },
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 511
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }
} );