Ext.define( 'TFWhiteLabel.store.riskmon.OrderControl', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.OrderControl',
    remoteSort  : true,
    remoteFilter: true,
    autoLoad    : false,
    pageSize    : -1,
    sorters     : [{
        property: 'TFOrderSeq',
        direction: 'ASC'
    }],
    // groupField: 'TFOrderId',
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 53
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }
} );