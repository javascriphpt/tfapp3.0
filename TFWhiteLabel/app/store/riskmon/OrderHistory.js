'use strict';
Ext.define( 'TFWhiteLabel.store.riskmon.OrderHistory', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.OrderHistory',
    remoteSort  : true,
    remoteFilter: true,
    autoLoad    : false,
    pageSize    : 10, //25
    sorters     : [{
        property: 'TradeDate',
        direction: 'DESC'
    }],
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 111
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }
} );