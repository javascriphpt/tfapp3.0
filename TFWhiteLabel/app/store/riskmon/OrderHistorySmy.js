'use strict';
Ext.define( 'TFWhiteLabel.store.riskmon.OrderHistorySmy', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.OrderHistorySmy',
    remoteSort  : true,
    remoteFilter: true,
    autoLoad    : false,
    pageSize    : 5,
    sorters: [
        {
            property: 'LglEntityName',
            direction: 'ASC' 
        }
    ],
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 110
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }
} );