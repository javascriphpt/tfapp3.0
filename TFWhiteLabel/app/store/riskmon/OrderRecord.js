Ext.define( 'TFWhiteLabel.store.riskmon.OrderRecord', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.OrderRecord',
    remoteSort  : true,
    remoteFilter: true,
    autoLoad    : false,
    pageSize    : -1,
    sorters     : [{
        property : 'TFOrderId',
        direction: 'ASC'
    }, {
        property : 'TFOrderSeq',
        direction: 'ASC'
    }],
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 53
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }
} );