Ext.define( 'TFWhiteLabel.store.riskmon.OrderType', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.OrderType',
    remoteSort  : true,
    remoteFilter: true,
    autoLoad    : false,
    pageSize    : -1,
    sorters     : [{
        property : 'OrdTypeName',
        direction: 'ASC'
    }],
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 121
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }
} );