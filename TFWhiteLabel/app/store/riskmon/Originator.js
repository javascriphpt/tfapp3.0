'use strict';
Ext.define( 'TFWhiteLabel.store.riskmon.Originator', {
    extend    : 'Ext.data.Store',
    model     : 'TFWhiteLabel.model.riskmon.Originator',
    remoteSort: false,
    autoLoad  : false,
    pageSize  : -1,
    fields    : [
        'OriginatorId', 'OriginatorCode', 'OriginatorName'
    ],
    proxy     : {
        type       : 'ajax',
        url        : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams: {
            id: 381
        },
        reader     : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }

} );