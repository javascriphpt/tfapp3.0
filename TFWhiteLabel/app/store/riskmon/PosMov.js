Ext.define( 'TFWhiteLabel.store.riskmon.PosMov', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.PosMov',
    alias       : 'store.posMovStore',
    remoteSort  : true,
    remoteFilter: true,
    autoLoad    : false,
    pageSize    : -1,
    sorters     : {
        property : 'InstrumentName',
        direction: 'ASC'
    },
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 108
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }
} );