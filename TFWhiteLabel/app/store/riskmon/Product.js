Ext.define( 'TFWhiteLabel.store.riskmon.Product', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.Product',
    alias       : 'store.productStore',
    remoteSort  : true,
    remoteFilter: true,
    autoLoad    : false,
    pageSize    : -1,
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 160
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }
} );