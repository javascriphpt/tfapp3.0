Ext.define( 'TFWhiteLabel.store.riskmon.RegGrp', {
    extend    : 'Ext.data.Store',
    model     : 'TFWhiteLabel.model.riskmon.RegGrp',
    remoteSort: false,
    autoLoad  : false,
    pageSize  : -1,
    sorters   : {
        property : 'RegGrpName',
        direction: 'ASC'
    },
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 733
        },
        reader       : {
            type           : 'json',
            rootProperty   : 'data',
            totalProperty  : 'total',
            successProperty: 'success'
        }
    }
} );