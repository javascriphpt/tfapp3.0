Ext.define( 'TFWhiteLabel.store.riskmon.SalesGrp', {
    extend    : 'Ext.data.Store',
    model     : 'TFWhiteLabel.model.riskmon.SalesGrp',
    remoteSort: false,
    autoLoad  : false,
    pageSize  : -1,
    sorters   : {
        property: 'SalesGrpId',
        direction: 'ASC'
    },
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 106
        },
        reader       : {
            type           : 'json',
            rootProperty   : 'data',
            totalProperty  : 'total',
            successProperty: 'success'
        }
    }
} );