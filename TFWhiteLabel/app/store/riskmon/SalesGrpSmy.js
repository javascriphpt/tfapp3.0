'use strict';
Ext.define( 'TFWhiteLabel.store.riskmon.SalesGrpSmy', {
    extend    : 'Ext.data.Store',
    model     : 'TFWhiteLabel.model.riskmon.SalesGrpSmy',
    remoteSort: true,
    autoLoad  : false,
    pageSize  : -1,
    sorters   : {
        property : 'SalesGrpId',
        direction: 'ASC'
    },
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 731
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }

} );