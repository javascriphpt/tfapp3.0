'use strict';
Ext.define( 'TFWhiteLabel.store.riskmon.SalesGrpType', {
    extend    : 'Ext.data.Store',
    model     : 'TFWhiteLabel.model.riskmon.SalesGrpType',
    remoteSort: true,
    autoLoad  : false,
    pageSize  : -1,
    sorters: {
        property: 'SalesGrpTypeCode',
        direction: 'ASC'
    },
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 107
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }

} );