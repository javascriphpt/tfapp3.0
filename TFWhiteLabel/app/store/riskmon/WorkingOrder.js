Ext.define( 'TFWhiteLabel.store.riskmon.WorkingOrder', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.WorkingOrder',
    remoteSort  : true,
    remoteFilter: true,
    autoLoad    : false,
    pageSize    : 50,
    sorters     : [{
        property: 'TFTransactionId',
        direction: 'ASC'
    }],
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 52
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }
} );