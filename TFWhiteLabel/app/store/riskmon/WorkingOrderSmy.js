Ext.define( 'TFWhiteLabel.store.riskmon.WorkingOrderSmy', {
    extend      : 'Ext.data.Store',
    model       : 'TFWhiteLabel.model.riskmon.CashTransfer',
    remoteSort  : true,
    remoteFilter: true,
    autoLoad    : false,
    pageSize    : 50,
    sorters     : [{
        property: 'TrdActCode',
        direction: 'ASC'
    }],
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url+ '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id: 51
        },
        reader       : {
            rootProperty : 'data',
            totalProperty: 'total'
        }
    }
} );