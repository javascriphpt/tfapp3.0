'use strict';
Ext.define( 'TFWhiteLabel.store.widget.LegalEntity', {
    extend  : 'Ext.data.Store',
    model   : 'TFWhiteLabel.model.widget.LegalEntity',
    pageSize: -1,
    autoLoad: false,
    sorters : {
        property: 'LglEntityId',
        direction: 'ASC'
    },
    proxy    : {
        type : 'ajax',
        url  : TFApps.config.dataRequest.url + '/TFData/Get',
        extraParams: {
            id: 214,
            filter : ''
        },
        actionMethods : {
            create    : 'POST',
            read      : 'POST',
            update    : 'POST',
            destroy   : 'POST'
        },

        reader: {
            rootProperty   : 'data',
            totalProperty  : 'total',
            messageProperty: 'message',
            successProperty: 'success'
        }
    }
});