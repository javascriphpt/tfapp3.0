'use strict';
Ext.define( 'TFWhiteLabel.store.widget.TradeAct', {
    extend  : 'Ext.data.Store',
    model   : 'TFWhiteLabel.model.widget.TradeAct',
    pageSize: 0,
    autoLoad: false,
    sorters : {
        property: 'TrdActId',
        direction: 'ASC'
    },
    proxy    : {
        type : 'ajax',
        url  : TFApps.config.dataRequest.url + '/TFData/Get',
        extraParams: {
            id: 823
        },
        actionMethods : {
            create    : 'POST',
            read      : 'POST',
            update    : 'POST',
            destroy   : 'POST'
        },

        reader: {
            rootProperty   : 'data[0]',
            totalProperty  : 'total',
            messageProperty: 'message',
            successProperty: 'success'
        }
    }
});