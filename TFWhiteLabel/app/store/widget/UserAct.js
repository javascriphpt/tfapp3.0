'use strict';
Ext.define( 'TFWhiteLabel.store.widget.UserAct', {
    extend  : 'Ext.data.Store',
    model   : 'TFWhiteLabel.model.widget.UserAct',
    pageSize: -1,
    autoLoad: false,
    sorters : {
        property: 'UserId',
        direction: 'ASC'
    },
    proxy    : {
        type : 'ajax',
        url  : TFApps.config.dataRequest.url + '/TFData/Get',
        extraParams: {
            id: 373,
            filter : ''
        },
        actionMethods : {
            create    : 'POST',
            read      : 'POST',
            update    : 'POST',
            destroy   : 'POST'
        },

        reader: {
            rootProperty   : 'data',
            totalProperty  : 'total',
            messageProperty: 'message',
            successProperty: 'success'
        }
    }
});