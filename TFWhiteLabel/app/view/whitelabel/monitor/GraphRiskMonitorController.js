'use strict';
Ext.define('TFWhiteLabel.view.whitelabel.monitor.GraphRiskMonitorController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.graphRiskMonitorController',

	config: {
		control: {
			'app-whitelabel-graphRiskMonitor': {
				activate  : 'onActivateGraphRiskmon',
				deactivate: 'onDeactivateGraphRiskmon'
			}
		}
	},

	onRenderNorthTabPanel: function (panel) {

		var getLocale = TFCommon.Locales.getLocaleValue;

		panel.add([
			{
				xtype: 'app-whitelabel-actSmyMap',
				title: getLocale( 'RiskMon.ActSmyHeatmap.Title', 'Account Summary Heatmap' )
			},
            {
                xtype    : 'app-whitelabel-actSmy',
                title    : getLocale( 'RiskMon.ActSmy.Title', 'Account' ),
                store    : 'whitelabel.ActSmy'
            }
        ]);

        panel.setActiveItem(0);
	},

	onRenderCenterTabPanel: function (panel) {
	
		var getLocale = TFCommon.Locales.getLocaleValue;

		panel.add([{
            xtype: 'app-whitelabel-posMov',
            title: getLocale( 'RiskMon.PosMov.Title', 'Position Movements' ),
            store: {
            	type: 'posMovStore',
            	storeId: 'alias.whitelabel.ActSmyPosMovStore'
            }
        }]);

        panel.setActiveItem(0);
	},

	onActivateGraphRiskmon: function () {
		
		this.initActiveTab();
	},

	getNorthActiveTab: function (){
		return this.lookupReference('northTabPanel').getActiveTab();
	},

	initActiveTab: function (){

		var activeTab = this.getNorthActiveTab();

		activeTab.getController().initActivePanel();
	},

	onDeactivateGraphRiskmon: function () {

		var activeTab = this.getNorthActiveTab();

		activeTab.getController().initDestructorPanel();
	}
});