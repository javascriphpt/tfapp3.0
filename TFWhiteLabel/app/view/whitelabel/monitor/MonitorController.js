'use strict';

Ext.define( 'TFWhiteLabel.view.whitelabel.monitor.MonitorController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.monitorController',

	config: {
        control: {
            'app-whitelabel-monitor #monitorNavMenu': {
                selectionchange: 'onSelectionNavigationNav'
                // afterrender: 'onAfterRenderMenu'
            },

            'app-whitelabel-monitor #contentPanel': {
                render: 'onRenderMonitorPanel'
            }
            
        }
    },

    init: function (){
        // create a manual selection on navigation
        this.navSelected = null;

        // List of panels added to panel - card layout
        this.cardLayoutItems = [];


        this.initNodeSelection();
    },

    initNodeSelection: function () {

        var monitorNavMenu = this.lookupReference('monitorNavMenu');
        var rootNode = monitorNavMenu.getStore().getRootNode();
        var childNode = rootNode.firstChild.childNodes[0];
        var item;

        this.navSelected = monitorNavMenu.getItem( childNode );

        this.navSelected.setSelected(true);

    },

    onAfterRenderMenu: function ( tree ){

        var rootNode = tree.getStore().getRootNode(),
            selModel = tree.getSelectionModel();

        selModel.select(rootNode.firstChild.childNodes[0]);
    },

    onSelectionNavigationNav: function ( treepanel, record ){

        var monitorContent = this.lookupReference('mainCardPanel');
        var componentId = record.get('id');


        if (this.navSelected) {
            this.navSelected.setSelected( false );
            this.navSelected = null;
        }

        this.addItemMonitor( 'app-whitelabel-'+ record.get('id'), record.get('id'));
    },


    addItemMonitor: function ( xtype, itemId ) {
        var monitorContentPanel = this.lookupReference('mainCardPanel');

        if ( this.cardLayoutItems.indexOf( xtype ) < 0 ) {
            // add the item
            monitorContentPanel.add( {
                xtype : xtype,
                itemId: itemId
            });    

            this.cardLayoutItems.push( xtype );

            console.log('ni exist navigation')
        }

        // set as active item
        monitorContentPanel.getLayout().setActiveItem( itemId );
    },

    getMonitorNavMenu: function (){

        var navigation = [
            {
                xtype: 'app-riskmon-graphRiskMonitor'
            },
            {
                xtype: 'app-riskmon-riskMonitor'
            },
            {
                xtype: 'app-riskmon-orderHistoryLayout'
            }, 
            {
                xtype: 'app-riskmon-workingOrderLayout'
            }, 
            {
                xtype: 'app-riskmon-rejectOrder'
            }, 
            {
                xtype: 'app-riskmon-fill'
            }, 
            {
                xtype: 'app-riskmon-bookingList'
            }, 
            {
                xtype: 'app-riskmon-position'
            }
        ];

        return navigation;
    },

    onRenderMonitorPanel: function ( panel ){

        panel.add({
            xtype: 'app-whitelabel-graphRiskMonitor',
            itemId: 'graphRiskMonitor'
        });

        this.cardLayoutItems.push( 'app-whitelabel-graphRiskMonitor' );
    }
});