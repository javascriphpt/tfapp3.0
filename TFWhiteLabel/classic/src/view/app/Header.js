/**
 * App Header
 */
Ext.define( 'TFWhiteLabel.view.app.Header', {
    extend: 'TFCommon.view.app.Header',

    xtype : 'app-header',
    cls   : 'app-header',

    items: [
        {
            header     : false,
            dockedItems: [
                {
                    xtype  : 'toolbar',
                    dock   : 'top',
                    cls    : 'sencha-dash-dash-headerbar shadow',
                    height : 60,
                    items  : [
                        {
                            margin : '0 0 0 8',
                            iconCls: 'x-fa fa-navicon',
                            handler: 'onToggleNavigationSize'
                        }, {
                            xtype   : 'component',
                            reference : 'logo',
                            height  : 38,
                            cls     : 'defaultLogo'
                        }, {
                            bind : {
                                text: '{appTitle}'
                            },
                            xtype: 'tbtext',
                            cls  : 'tbtext-title'
                        }, '->',

                        // component holder for status error messages
                        {
                            text    : '',
                            xtype   : 'tbtext',
                            itemId  : 'statusText',
                            cls     : 'header-error-message',
                            hidden  : true
                        }, '->', {
                            xtype         : 'combobox',
                            matchFieldWidth: false,
                            bind          : {
                                fieldLabel: '{labelTheme}'
                            },
                            cls           : 'labelTheme',
                            width         : 150,
                            itemId        : 'comboTheme',
                            name          : 'theme',
                            value         : 'standard',
                            store         : Ext.create( 'Ext.data.Store', {
                                fields: ['value', 'name'],
                                data  : [
                                ]
                            } ),
                            labelWidth    : 50,

                            hidden        : true,
                            queryMode     : 'local',
                            displayField  : 'value',
                            valueField    : 'name',
                            editable      : false,
                            forceSelection: true,
                            allowBlank    : false,
                            tabIndex      : 3,
                            listeners     : {
                                beforerender: 'onBeforeRenderThemeSwitcherCombo',
                                select      : 'onSelectThemeSwitcher'
                            }
                        }, {
                            xtype         : 'combobox',
                            itemId        : 'comboLocale',
                            name          : 'locale',
                            value         : 'ja',
                            store         : Ext.create( 'Ext.data.Store', {
                                fields: ['localeId', 'localeCode', 'localeName'],
                                data  : [
                                ]
                            } ),
                            width         : 100,
                            queryMode     : 'local',
                            displayField  : 'localeName',
                            valueField    : 'localeCode',
                            editable      : false,
                            forceSelection: true,
                            allowBlank    : false,
                            tabIndex      : 3,
                            listeners     : {
                                select      : 'onSelectComboLocale',
                                beforerender: 'onBeforeRenderComboLocale'
                            }
                        }, {
                            bind    : {
                                text: '{labelHelp}'
                            },
                            hidden  : true,
                            cls     : 'labelHelp',
                            itemId  : 'labelHelp',
                            handler : 'onClickBtnHelp'
                        }, '-', {
                            cls       : 'labelMyAccount',
                            itemId    : 'userAccountBtn',
                            reference : 'userAccountBtn',
                            handler   : 'onMyAccountBtnClick'
                        }, {
                            bind     : {
                                text : '{labelLogout}'
                            },
                            cls      : 'labelLogout',
                            itemId   : 'userLogoutBtn',
                            listeners: {
                                click: 'onClickUserLogoutBtn'
                            }
                        }, '-', {
                            iconCls: 'application_view_tile',
                            cls    : 'appSwith',
                            menu   : {
                                itemId   : 'menuAppSwitch',
                                items    : [
                                    {
                                        text: 'Loading apps...'
                                    }
                                ],
                                listeners: {
                                    afterrender: 'onRenderMenuAppSwitch'
                                }
                            }
                        }
                    ]
                }
            ]
        }
    ]
} );
