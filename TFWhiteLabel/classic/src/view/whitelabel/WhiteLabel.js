'use strict';

Ext.define( 'TFWhiteLabel.view.whitelabel.WhiteLabel', {
	extend     : 'Ext.Container',
	xtype      : 'app-whitelabel-main',

	requires   : [
		'TFWhiteLabel.view.whitelabel.WhiteLabelController'
	],

	controller : 'whiteLabelController',
	layout     : 'border',
	border     : false,
	defaults   : {
		border : false
	},

	items      : [
		{
			region    : 'center',
			layout    : 'card',
			reference : 'contentCenterPanel',
			items     : [
				{
					xtype : 'app-whitelabel-monitor'
				}, {
					html : 'Coming Soon :)'
				}
			]
		}
	]
} );