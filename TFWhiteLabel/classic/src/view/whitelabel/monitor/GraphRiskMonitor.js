'use strict';
Ext.define( 'TFWhiteLabel.view.whitelabel.monitor.GraphRiskMonitor', {
    extend: 'Ext.Container',
    xtype : 'app-whitelabel-graphRiskMonitor',

    viewModel: {
        type: 'graphRiskMonitorModel'
    },
    controller: 'graphRiskMonitorController',
    layout    : 'border',
    border    : false,
    defaults  : {
        border: false
    },
    itemId: 'parentContainer',
    height: 1,
    items: [
        {
            region: 'north',
            layout: 'fit',
            split : true,
            height: '50%',
            itemId: 'whiteLabelMainNorthPanel',
            items : [
                {
                    xtype    : 'tabpanel',
                    itemId   : 'northTabPanel',
                    reference: 'northTabPanel',
                    listeners: {
                        // render: 'onRenderNorthTabPanel'
                    }
                }
            ]
        },
        {
            region: 'center',
            layout: 'fit',
            items : [
                {
                    xtype    : 'tabpanel',
                    itemId   : 'bottomPanel',
                    reference: 'bottomPanel',
                    listeners: {
                        // render: 'onRenderCenterTabPanel'
                    }
                }
            ]
        }
    ]
} );
