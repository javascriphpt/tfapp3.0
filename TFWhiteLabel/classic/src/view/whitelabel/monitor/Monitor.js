'use strict';

Ext.define( 'TFWhiteLabel.view.whitelabel.monitor.Monitor', {
	extend : 'Ext.Container',
	xtype  : 'app-whitelabel-monitor',
	layout : 'border',
	cls    : 'x-screen-monitor',

	requires : [
		'TFWhiteLabel.view.whitelabel.monitor.MonitorController',
		'TFWhiteLabel.view.whitelabel.monitor.MonitorController'
	],

	controller : 'monitorController',
	viewModel  : {
		type : 'monitorModel'
	},

	items : [
		{
			xtype    : 'maincontainerwrap',
            region   : 'center',
            // id       : 'main-view-detail-wrap',
            reference: 'mainContainerWrap',
            itemId   : 'mainContainerWrap',
            flex     : 1,
            items 	 : [
            	{
                    xtype        : 'treelist',
                    useArrows    : true,
                    border       : false,
                    expanderFirst: false,
                    expanderOnly : false,
                    itemId       : 'monitorNavMenu',
                    reference    : 'monitorNavMenu',
                    ui           : 'navigation',
                    store    : Ext.create('Ext.data.TreeStore', {

                        storeId: 'monitorNavMenuStore',
                        fields: [{
                            name: 'text'
                        }],

                        root: {
                            expanded: true,
                            children: [
                                {
                                    text    : 'Graphical Risk Monitor',
                                    iconCls   : ' fa fa-area-chart',
                                    selectable: false,
                                    expanded  : true,
                                    children  : [
                                        {
                                            selection: 1,
                                            text     : 'Account Summary',
                                            iconCls  : 'fa fa-users',
                                            id       : 'graphRiskMonitor',
                                            leaf     : true
                                        }
                                    ]
                                },
                                {
                                    text    : 'Monitor',
                                    expanded: true,
                                    iconCls   : 'fa fa-television',
                                    selectable: false,
                                    children: [
                                        { 
                                            text   : 'Risk Monitor',
                                            iconCls: 'fa fa-bolt',
                                            id     : 'riskMonitor'
                                            // leaf   : true 
                                            ,children: [
                                        { 
                                            text   : 'sample',
                                            iconCls: 'fa fa-bolt',
                                            id     : 'riskMonitor'
                                            // leaf   : true 
                                        }

                                    ]
                                        }

                                    ]
                                },
                                {
                                    text      : 'Orders',
                                    expanded  : true,
                                    selectable: false,
                                    iconCls   : 'fa fa-cart-plus',
                                    children  : [
                                        {
                                            text   : 'Order History',
                                            id     : 'orderHistoryLayout',
                                            iconCls: 'fa fa-list-ol',
                                            leaf   : true
                                        },
                                        {
                                            text   : 'Working Order',
                                            id     : 'workingOrderLayout',
                                            iconCls: 'fa fa-hourglass-half',
                                            leaf   : true
                                        },
                                        {
                                            text   : 'Rejected Order',
                                            id     : 'rejectOrder',
                                            iconCls: 'fa fa-exclamation-triangle',
                                            leaf   : true
                                        },
                                        {
                                            text   : 'Client Fills',
                                            iconCls: ' fa fa-stack-overflow',
                                            id     : 'fill',
                                            leaf   : true
                                        },
                                        {
                                            text   : 'BookingList',
                                            id     : 'bookingList',
                                            iconCls: 'fa fa-tasks',
                                            leaf   : true
                                        },
                                        {
                                            text   : 'Positions',
                                            iconCls: 'fa fa-street-view',
                                            id     : 'position',
                                            leaf   : true
                                        }
                                    ]
                                }
                            ]
                        }
                    }),
                    rootVisible: false
                }, {
                    xtype    : 'container',
                    flex     : 1,
                    border   : false,
                    reference: 'mainCardPanel',
                    cls      : 'sencha-dash-right-main-container',
                    itemId   : 'contentPanel',
                    layout   : {
                        type: 'card',
                        anchor: '100%'
                    }
                }
            ]
		}
	]
} );