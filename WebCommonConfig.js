/**
 * Created on 12/15/2014.
 */
'use strict';

if (typeof Object.assign != 'function') {
  Object.assign = function(target) {
    'use strict';
    if (target == null) {
      throw new TypeError('Cannot convert undefined or null to object');
    }

    target = Object(target);
    for (var index = 1; index < arguments.length; index++) {
      var source = arguments[index];
      if (source != null) {
        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }
    }
    return target;
  };
}

window.CommonConfig = {
    debug                  : true,
    showGridLoadMask       : false,
    autoRefreshGrids       : true,
    autoRefreshGridInterval: 15000,
    locale                 : 'ja',
    theme                  : {
        show  : true,
        themes: [
            {
                defaultTheme: true,
                resourceKey : 'Theme.theme.Standard',
                defaultValue: 'Standard',
                appTheme    : 'ext-theme-standard'

            },
            {
                resourceKey : 'Theme.theme.Touch',
                defaultValue: 'Touch',
                appTheme    : 'ext-theme-touch'
            }
        ]
    },

    // Locales available
    locales: {
        en: {
            label     : 'English',
            showByName: false,
            id        : 1
        },
        ja: {
            label     : '日本語',
            showByName: true,
            id        : 2
        }
    },

    // Use to identify type of message with its corresponding Id
    alertPriorityCode: {
        NONE: {
            id  : 0,
            code: 'None'
        },
        INFO: {
            id  : 2,
            code: 'Info'
        },
        WARN: {
            id  : 3,
            code: 'Warning'
        },
        ERROR: {
            id  : 4,
            code: 'Error'
        }
    },

    // Locale key type to load
    localeKeyType   : "'TF.WebAdm','TF.Alert','TF.Common'",

    displayFormats: {
        fmtInt       : '0,000',
        fmtLots      : '0,000',
        fmtPrice     : '0,000.00',
        fmtPriceDepth: '0,000',
        fmtCashAmount: '0,000',
        fmtPercent   : '0,000.00'
    },

    // signalr data request ID
    dataRequestIds: {
        riskAlert   : [504, 505],
        processAlert: [506],
        marketPrice : [507],
        orderUpdate : [508],
        fillUpdate  : [509],
        marketDepth : [515]
    },

    // Contains list of defined @id and its corresponding @signalType.
    // key = @id
    // value = @signalType
    // @signalType - riskAlert, processAlert, and others are defined in the subclasses
    // Override this config on the subclass only if you want to display specific Signals on preference
    preferenceType: {
        3: 'riskAlert',
        6: 'processAlert'
    },

    // Enable centralised notification manager
    notificationManager: false,

    getCodeOrNameByLocale: function ( code, name ) {
        return ( TFAppConfig.locale === 'en' ) ? code : name;
    },

    error: {
        msgView: {
            PROMPT: 1,
            TOAST : 2,
            STATUS: 3
        },

        code: {
            SRV_CONN_ERR: 0,
            SESS_ERR    : 0
        },

        msg: {
            SRV_CONN_ERR_KEY: 'Error.Message.SRV_CONN_ERR',
            SRV_CONN_ERR    : 'Server Connection failed',

            SESS_ERR_KEY: 'Error.Message.SESS_ERR',
            SESS_ERR    : 'Invalid session'
        },

        title: {
            SRV_CONN_ERR_KEY: 'Error.Title.SRV_CONN_ERR',
            SRV_CONN_ERR    : 'Connection failure',

            SESS_ERR_KEY: 'Error.Title.SESS_ERR',
            SESS_ERR    : 'Client session failure'
        }
    }
};