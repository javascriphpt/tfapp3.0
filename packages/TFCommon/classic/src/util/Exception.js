'use strict';

Ext.define( 'TFCommon.util.Exception', {
	singleton   : true,
	silenced    : false,
	toastLoaded : false,
	alertWindow : null,
	prevErrMsgs : [],

	requires : [
        'Ext.form.Label',
        'Ext.form.field.Checkbox'
    ],

	logError : function( messageView, errorCode, title, message, callback) {
		var errorMessage    = '';
        var code            = TFCommon.Locales.getLocaleValue( 'Error.Label.Code',  'Code' );

		// temporary values for message view
		// 1 - Prompt
		// 2 - Toast
		// 3 - Status

		errorMessage = this.logMessage( errorCode, message );

		if ( !callback ) {
			callback = function () {};
		}

		switch ( messageView ) {
			case 1 :

				if( errorCode === TFAppConfig.error.code.SESS_ERR) {

					this.silenced = true;
					this.prevErrMsgs.push(errorMessage);

					Ext.Msg.alert( title, errorMessage, callback );
				} else {
					if(!this.silenced) {

						this._createAlertErrorMessage( title, errorMessage, callback);
					}
					else
					{
						if( Ext.Array.indexOf( this.prevErrMsgs, errorMessage ) < 0) {
							this._createAlertErrorMessage( title, errorMessage, callback);
						}
					}
				}

				break;

			case 2 :

				if(!this.silenced) {
					if( !this.toastLoaded ) {
						var that = this;

						this.toastLoaded = true;
						Ext.toast({
							html        : errorMessage,
                            title       : title,
                            align       : 't',
                            closeAction : 'destroy',
                            listeners   : {
                                close   : function () {
                                    that.toastLoaded = false;
                                }
                            }
						});
					}
				}

				break;

			case 3 :
				// error message will be returned to the controller
				// so that the controller will reference the statusText item from the header view

				return title + ' : ' + message + ' ( '+ code +' : ' + errorCode + ' )';

			default :

				if( errorCode === TFAppConfig.error.code.SESS_ERR) {

					this.silenced = true;
					this.prevErrMsgs.push(errorMessage);

					Ext.Msg.alert( title, errorMessage, callback );
				} else {
					if(!this.silenced) {

						this._createAlertErrorMessage( title, errorMessage, callback);
					}
					else
					{
						if( Ext.Array.indexOf( this.prevErrMsgs, errorMessage ) < 0) {
							this._createAlertErrorMessage( title, errorMessage, callback);
						}
					}
				}

				break;
		}

		return false;
	},

	logMessage	: function ( errorCode, errorMessage ) {

        var getLocale = TFCommon.Locales.getLocaleValue;

		return getLocale( 'Error.Label.ErrorCode', 'Error Code: ') + errorCode + '<br/>'+ getLocale( 'Error.Label.Message', 'Message: ') + errorMessage;
	},

	// only this class should access this
	// custom alert
	_createAlertErrorMessage : function ( title, errorMessage, callback ) {
		var _that           = this,
            _title          = title,
			_errorMessage   = errorMessage,
            _callback       = callback;


        if( this.alertWindow !== null) {
            this.alertWindow.close();
        }

        this.alertWindow = Ext.create( 'Ext.window.Window', {
            title   : _title,

            closeAction : 'destroy',
            id          : 'customAlertWindow',
            resizable : false,

            layout  : 'fit',

            items   : [
                {
                    xtype   : 'panel',
                    frame	: false,
                    border	: false,
                    layout	: 'vbox',
                    bodyPadding : 5,

                    items	: [
                        {
                            xtype   : 'label',
                            html    : _errorMessage,
                            width   : 400
                        },
                        {
                            xtype       : 'checkbox',
                            boxLabel    : TFCommon.Locales.getLocaleValue( 'Error.Message.Silence',  'Hide this same error in the future.' ),
                            width       : 400,
                            id          : 'silenceCheckbox',
                            margin		: '10 0 0 0'
                        }
                    ]
                }
            ],

            buttons : [
                {
                    text    : 'OK',
                    handler : function () {
                        if ( Ext.getCmp( 'silenceCheckbox' ).getValue() ) {
                            _that.silenced = true;
                        }

                        _that.prevErrMsgs.push(_errorMessage);

                        _that.alertWindow.close();

                        _callback();
                    }
                }
            ]
        } );

        this.alertWindow.show();
	}
});