
Ext.define( 'TFCommon.ux.dashboard.Column', {
    extend: 'Ext.dashboard.Column',
    xtype : 'app-ux-dashboard-column',

    listeners: {
        remove      : function () {

        },
        beforeremove: function () {

        },
        add         : function (col, cmp, index) {
            setTimeout(function () {
                col.setWidth( 253 );
            }, 3000);
        },
        added         : function (col, cmp, index) {
            col.setWidth(0.33);
        },
        beforeadd   : function (col, cmp, index) {
            col.setWidth( 0.33 );
        },
        resize: function () {

        },
        move: function (col, cmp, index) {
                col.setWidth( 0.33 );
        },
        beforemove: function (col, cmp, index) {
            col.setWidth( 0.33 );
        }
    },

    onRemove: function () {

    }
} );