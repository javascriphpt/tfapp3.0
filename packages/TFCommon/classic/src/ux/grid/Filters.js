Ext.define( 'TFCommon.ux.grid.Filters', {
    extend: 'Ext.grid.filters.Filters',
    alias : 'plugin.tf-gridfilters',

    pluginId: 'tf-gridfilters',

    bindStore: function ( store ) {
        var me = this;

        // Set up correct listeners
        if ( store ) {
            if ( me.store ) {
                me.store.destroy();
                me.store.getFilters().un( 'remove', me.onFilterRemove, me );
            }

            // `local` used to be a config, but it should be determined by the store.
            me.local = !store.remoteFilter;
            store.getFilters().on( 'remove', me.onFilterRemove, me );
        }

        me.store = store;
    }
} );
