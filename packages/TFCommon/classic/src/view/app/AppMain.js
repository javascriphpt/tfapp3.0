/**
 * App main layout
 */
Ext.define('TFCommon.view.app.AppMain', {
    extend: 'Ext.Container',

    xtype: 'app-main',

    layout: 'border',

    defaults: {
        border: false
    },
    border: false,

    requires : [
        'TFCommon.view.app.Header'
    ],

    items: [
        {
            region: 'north',
            xtype: 'app-header'
        }, {
            region: 'center',
            itemId: 'app-subContainer',
            layout: 'fit',
            items: []
        }
    ]
});
