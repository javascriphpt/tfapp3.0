/**
 * App Header
 */
Ext.define( 'TFCommon.view.app.Header', {
    extend: 'Ext.panel.Panel',

    xtype: 'app-header',

    requires : [
        'TFCommon.view.app.HeaderController',
        'TFCommon.view.app.HeaderModel'
    ],

    viewModel: {
        type: 'headerModel'
    },

    controller: 'headerController',

    layout: 'fit',

    cls: 'app-header',

    defaults: {
        border: false
    },

    border: false,

    items: [
        {
            header     : false,
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock : 'top',
                    items: [
                        {
                            xtype   : 'component',
                            reference : 'logo',
                            height  : 38,
                            cls     : 'defaultLogo'
                        },
                        {
                            bind : {
                                text: '{appTitle}'
                            },
                            xtype: 'tbtext',
                            cls  : 'tbtext-title'
                        },
                        '->',

                        // component holder for status error messages
                        {
                            text    : '',
                            xtype   : 'tbtext',
                            itemId  : 'statusText',
                            // cls     : 'header-error-message',
                            hidden  : false
                        },
                        '->',
                        {
                            xtype         : 'combobox',
                            matchFieldWidth: false,
                            bind          : {
                                fieldLabel: '{labelTheme}'
                            },
                            cls           : 'labelTheme',
                            width         : 150,
                            itemId        : 'comboTheme',
                            name          : 'theme',
                            value         : 'standard',
                            store         : Ext.create( 'Ext.data.Store', {
                                fields: ['value', 'name'],
                                data  : [
                                ]
                            } ),
                            labelWidth    : 50,

                            hidden        : true,
                            queryMode     : 'local',
                            displayField  : 'value',
                            valueField    : 'name',
                            editable      : false,
                            forceSelection: true,
                            allowBlank    : false,
                            tabIndex      : 3,
                            listeners     : {
                                beforerender: 'onBeforeRenderThemeSwitcherCombo',
                                select      : 'onSelectThemeSwitcher'
                            }
                        },
                        {
                            xtype         : 'combobox',
                            itemId        : 'comboLocale',
                            name          : 'locale',
                            value         : 'ja',
                            store         : Ext.create( 'Ext.data.Store', {
                                fields: ['localeId', 'localeCode', 'localeName'],
                                data  : [
                                ]
                            } ),
                            width         : 100,
                            queryMode     : 'local',
                            displayField  : 'localeName',
                            valueField    : 'localeCode',
                            editable      : false,
                            forceSelection: true,
                            allowBlank    : false,
                            tabIndex      : 3,
                            listeners     : {
                                select      : 'onSelectComboLocale',
                                beforerender: 'onBeforeRenderComboLocale'
                            }
                        },
                        {
                            bind    : {
                                text: '{labelHelp}'
                            },
                            hidden  : true,
                            cls     : 'labelHelp',
                            itemId  : 'labelHelp',
                            handler : 'onClickBtnHelp'
                        },
                        '-',
                        {
                            cls     : 'labelMyAccount',
                            itemId  : 'userAccountBtn',
                            reference : 'userAccountBtn',
                            handler : 'onMyAccountBtnClick'
                        },
                        {
                            bind     : {
                                text : '{labelLogout}'
                            },
                            cls      : 'labelLogout',
                            itemId   : 'userLogoutBtn',
                            listeners: {
                                click: 'onClickUserLogoutBtn'
                            }
                        },
                        '-',
                        {
                            iconCls: 'application_view_tile',
                            cls    : 'appSwith',
                            menu   : {
                                itemId   : 'menuAppSwitch',
                                items    : [
                                    {
                                        text: 'Loading apps...'
                                    }
                                ],
                                listeners: {
                                    afterrender: 'onRenderMenuAppSwitch'
                                }
                            }
                        }
                    ]
                }
            ]
        }
    ]
} );
