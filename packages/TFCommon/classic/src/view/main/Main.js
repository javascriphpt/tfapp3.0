'use strict';

Ext.define( 'TFCommon.view.main.Main', {
	extend: 'Ext.container.Viewport',

    xtype: 'app-mainViewport',

    requires : [
        'TFCommon.view.main.MainController'
    ],

    controller : 'main',

    layout: 'fit',

    items: [
        {
            layout: 'center',
            itemId: 'mainLoader',

            items: [{
                cls   : 'main-tf-loader',
                border: false,
                width : 160
            }]
        }
    ]
} );