Ext.define( 'TFCommon.ux.window.Toast', {
    extend: 'Ext.Toast',

    getCmpId: function () {
        var id = Ext.emptyString;

        try {
    	   id = this.getEl().getId();
        } catch ( e ) {}

        return id;
    },

    getCmpView: function () {
    	return Ext.get( this.getCmpId() );
    },

    // <debug> show active toasts</debug>
    // slideBack: function () {
    //     var me = this,
    //         anchor = me.anchor,
    //         anchorEl = anchor && anchor.el,
    //         el = me.el,
    //         activeToasts = me.getToasts(),
    //         index = Ext.Array.indexOf(activeToasts, me);

    //     if (!me.isHiding && el && el.dom && anchorEl && anchorEl.isVisible()) {
    //         if (index) {
    //             me.xPos = me.getXposAlignedToSibling(activeToasts[index - 1]);
    //             me.yPos = me.getYposAlignedToSibling(activeToasts[index - 1]);
    //         } else {
    //             me.xPos = me.getXposAlignedToAnchor();
    //             me.yPos = me.getYposAlignedToAnchor();
    //         }

    //         me.stopAnimation();

    //         el.animate({
    //             to: {
    //                 x: me.xPos,
    //                 y: me.yPos
    //             },
    //             easing: me.slideBackAnimation,
    //             duration: me.slideBackDuration,
    //             dynamic: true
    //         });
    //     }
    // },

    onMouseEnter: function () {

    	this.mouseIsOver = true;

    	if ( this.isVisible() && this.isHiding ) {
            this.closeOnMouseOut = true;
            this.isHiding = false;

            this.getCmpView().stopAnimation().setOpacity( 1 );
            // this.el.stopAnimation().setOpacity( 1 );
	    }

    },

    // added delay to prevent from hiding directly the window when cursor is not focus
    onMouseLeave: function () {
    	var me = this, task, initialConfig = this.getInitialConfig();

        me.mouseIsOver = false;

        task = new Ext.util.DelayedTask( function () {

            if ( me.closeOnMouseOut && !me.mouseIsOver ) {
                me.isFading = true;
                me.isHiding = true;

                // me.el.animate( {
                me.getCmpView().animate( {
					to: {
						opacity: 0
					},
                    // duration: me.hideDuration,
					duration: 2500,
					listeners: {
						lastframe: function () {
                            me.closeOnMouseOut  = false;

							me.close();
						},

                        afteranimate: function () {
                            me.isFading = false;
                        }
					}
				} );
            }

        }, me );

        task.delay( 4000 );
    },

    hide: function () {
        var me = this,
            el = me.el;

        me.cancelAutoClose();

        if ( me.isHiding ) {
            if (!me.isFading) {
                me.callParent(arguments);
                // Must come after callParent() since it will pass through hide() again triggered by destroy()
                me.removeFromAnchor();
                me.isHiding = false;
            }

        } else {
            // Must be set right away in case of double clicks on the close button
            me.isHiding = true;
            me.isFading = true;

            me.cancelAutoClose();

            if ( el ) {

                // me.el.animate( {
                me.getCmpView().animate( {
					to: {
						opacity: 0
					},
					easing: 'easeIn',
                    duration: me.hideDuration,
					listeners: {
				    	afteranimate: function () {
                            me.isFading = false;

				    		if( !me.mouseIsOver ) {
				    			me.hide(me.animateTarget, me.doClose, me);
				    		}
                        }
				    }
				} );

            }
        }

        return me;
    }
},
function ( UXToast ) {
	Ext.ux.toast = function ( message, title, align, iconCls ) {
		var config = message,
		   	toast;

		if( Ext.isString( message ) ) {
			config = {
				title   : title,
				html    : message,
				iconCls : iconCls
			};

			if( align ) {
				config.align = align;
			}
		}

		toast = new UXToast( config );
		toast.show();

		return toast;
	};
} );