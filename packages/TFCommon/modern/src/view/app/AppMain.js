/**
 * App main layout
 */
Ext.define('TFCommon.view.app.AppMain', {
    extend: 'Ext.Container',

    xtype: 'app-main',

    layout: 'border',

    defaults: {
        border: false
    },
    border: false,

    items: [
        {
            region: 'north',
            items: [{
                border: false,
                xtype: 'app-header'
            }]
        }, {
            region: 'center',
            itemId: 'app-subContainer',
            layout: 'fit',
            items: []
        }
    ]
});
