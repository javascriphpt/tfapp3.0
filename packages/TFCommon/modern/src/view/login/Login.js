/**
 * Login Viewport
 */
Ext.define( 'TFCommon.view.login.Login', {
    /*extend: 'Ext.Container',

    xtype: 'app-loginPanel',

    viewModel: {
        type: 'loginModel'
    },

    controller: 'loginController',

    layout: 'center',

    items: [
        {
            xtype    : 'form',
            itemId   : 'loginForm',
            reference: 'loginForm',
            bind      : {
                title: '{appTitle}'
            },
            bodyStyle: 'padding:10px 20px;',
            frame    : true,
            width    : 350,
            layout   : 'anchor',
            defaults : {
                anchor: '100%'
            },
            items    : [
                {
                    xtype     : 'textfield',
                    bind      : {
                        fieldLabel: '{fieldLabelUserName}'
                    },
                    itemId     : 'fieldLabelUserName',
                    reference  : 'fieldLabelUserName',
                    name       : 'UserName',
                    allowBlank : false,
                    tabIndex   : 1,
                    vtype      : 'loginField',
                    msgTarget  : 'side',
                    listeners : {
                        specialkey   : 'onLoginFieldKeyPressSpecial',
                        beforerender : 'onUsernameBeforeRender'
                    }
                },
                {
                    xtype     : 'textfield',
                    bind      : {
                        fieldLabel: '{fieldLabelPassword}'
                    },
                    name       : 'Password',
                    inputType  : 'password',
                    vtype      : 'loginField',
                    msgTarget  : 'side',
                    allowBlank : false,
                    tabIndex   : 2,
                    listeners  : {
                        specialkey : 'onLoginFieldKeyPressSpecial'
                    }
                },
                {
                    xtype         : 'combobox',
                    bind          : {
                        fieldLabel: '{fieldLabelLanguage}'
                    },
                    itemId        : 'comboLocale',
                    name          : 'locale',
                    value         : 'ja',
//                    store         : Ext.create( 'Ext.data.Store', {
//                        fields: ['name', 'lang'],
//                        data  : [
//                            {
//                                "name": "English",
//                                "lang": "en"
//                            },
//                            {
//                                "name"  : "日本語",
//                                "lang": "ja"
//                            }
//                        ]
//                    } ),
                    store         : Ext.create( 'Ext.data.Store', {
                        fields: ['localeId', 'localeCode', 'localeName'],
                        data  : [
//                            {
//                                "localeId"  : 1,
//                                "localeCode": "en",
//                                "localeName": "English"
//                            },
//                            {
//                                "localeId"  : 2,
//                                "localeName": "日本語",
//                                "localeCode": "ja"
//                            }
                        ]
                    } ),
                    queryMode     : 'local',
//                    displayField  : 'name',
                    displayField  : 'localeName',
//                    valueField    : 'lang',
                    valueField    : 'localeCode',
                    editable      : false,
                    forceSelection: true,
                    allowBlank    : false,
                    tabIndex      : 3,
                    listeners     : {
                        select      : 'onSelectComboLocale',
                        beforerender: 'onBeforeRenderComboLocale'
                    }
                },
                {
                    xtype         : 'combobox',
                    bind          : {
                        fieldLabel: '{fieldLabelTheme}'
                    },
                    itemId        : 'comboTheme',
                    name          : 'theme',
                    value         : 'standard',
                    store         : Ext.create( 'Ext.data.Store', {
                        fields: ['value', 'name'],
                        data  : [
                        ]
                    } ),
                    hidden        : true,
                    queryMode     : 'local',
                    displayField  : 'value',
                    valueField    : 'name',
                    editable      : false,
                    forceSelection: true,
                    tabIndex      : 3,
                    listeners     : {
                        beforerender: 'onBeforeRenderThemeSwitcherCombo',
                        select      : 'onSelectThemeSwitcher'
                    }
                }
            ],
            buttons  : [
                {
                    xtype      : 'button',
                    bind       : {
                        text   : '{btnLogin}'
                    },
                    formBind   : true,
                    formaction : 'submit',
                    tabIndex   : 4,
                    handler    : 'onFormSubmit'
                }
            ]
        }
    ]*/
} );
