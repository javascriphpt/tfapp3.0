'use strict';

Ext.define( 'TFCommon.view.main.Main', {
	extend: 'Ext.Container',

    xtype: 'app-mainViewport',

    layout: 'fit',

    items: [
        {
            layout: 'center',
            itemId: 'mainLoader',
            xtype : 'panel',

            items: [{
                cls   : 'main-tf-loader',
                border: false,
                width : 160
            }]
        }
    ]
} );