# TFCommon/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    TFCommon/sass/etc
    TFCommon/sass/src
    TFCommon/sass/var
