'use strict';
Ext.define( 'TFCommon.cmp.grid.ColumnRenderer', {
    singleton: true,

    prcFormatMrktPriceBlackRedColor: function ( value, metadata, record ) {
        var prcFormat = record.get( 'PrcFormat' );

        // Check if the record has defined prcFormat, if not, then use the prcformat defined in config file
        prcFormat = Ext.isEmpty( prcFormat ) ? TFAppConfig.displayFormats.fmtPrice : prcFormat;

        if ( value > 0 ) {
            return '<span class="black-text">'+ Ext.util.Format.number( value, prcFormat ) +'</span>';
        } else if ( value < 0 ) {
            return '<span class="red-text">'+ Ext.util.Format.number( value, prcFormat ) +'</span>';
        }
    },

    mrktPriceIndArrowFormat: function ( value, metadata, record ) {

        //var fmtInstPrcFormat = record.get( 'PrcFormat' );
        var prcFormat = record.get('PrcFormat');
        var fmtInstPrcFormat = prcFormat;
        var cellVal = '';

        //fmtInstPrcFormat = (Ext.isEmpty(fmtInstPrcFormat)) ? TFAppConfig.displayFormats.fmtPrice : fmtInstPrcFormat;
        //THIS IS FOR TEMPORARY FIX. we use the config format instead on the server format
        // fmtInstPrcFormat = TFAppConfig.displayFormats.fmtPrice;

        fmtInstPrcFormat = (Ext.isEmpty( fmtInstPrcFormat )) ? TFAppConfig.displayFormats.fmtPrice : fmtInstPrcFormat;

        if (!Ext.isEmpty(value) && prcFormat !== null && prcFormat !== '' && value !== 0) {
            if ( value >= 0 ) {
                cellVal = '<span class="positive-ind up-arrow">' + Ext.util.Format.number( value, fmtInstPrcFormat ) + '</span>';
            } else if ( value < 0 ) {
                cellVal = '<span class="negative-ind down-arrow">' + Ext.util.Format.number( value, fmtInstPrcFormat ) + '</span>';
            }
        }

        return cellVal;

    },

    mrktPriceIndFormat: function ( value, metadata, record ) {

        var fmtInstPrcFormat = record.get( 'PrcFormat' );
        var cellVal = '';

        fmtInstPrcFormat = (Ext.isEmpty( fmtInstPrcFormat )) ? TFAppConfig.displayFormats.fmtPrice : fmtInstPrcFormat;

        if ( value >= 0 ) {
            cellVal = '<span class="positive-ind">' + Ext.util.Format.number( value, fmtInstPrcFormat ) + '</span>';
        } else if ( value < 0 ) {
            cellVal = '<span class="negative-ind">' + Ext.util.Format.number( value, fmtInstPrcFormat ) + '</span>';
        }



        return cellVal;

    },

    lotsFormat     : function ( value ) {

        var returnVal = '';

        if ( !Ext.isEmpty( value ) ) {
            if ( value >= 0 ) {
                returnVal = '<span class="blue-cell">' + Ext.util.Format.number( value, TFAppConfig.displayFormats.fmtLots ) + '</span>';
            } else if ( value < 0 ) {
                returnVal = '<span class="red-cell">' + Ext.util.Format.number( value, TFAppConfig.displayFormats.fmtLots ) + '</span>';
            }
        }


        return returnVal;
    },
    longLotsFormat : function ( value ) {

        var returnVal = '';
        if ( !Ext.isEmpty( value ) ) {
            return '<span class="blue-cell">' + Ext.util.Format.number( value, TFAppConfig.displayFormats.fmtLots ) + '</span>';
        } else {
            return returnVal;
        }
    },
    shortLotsFormat: function ( value ) {

        var returnVal = '';
        if ( !Ext.isEmpty( value ) ) {
            return  '<span class="red-cell">' + Ext.util.Format.number( value, TFAppConfig.displayFormats.fmtLots ) + '</span>';
        } else {
            return returnVal;
        }
    },

    prcFormat: function ( value, metadata, record ) {
        var remPrcFormat = record.get( 'PrcFormat' );
        var numFormat = (Ext.isEmpty( remPrcFormat )) ? TFAppConfig.displayFormats.fmtPrice : remPrcFormat;

        return Ext.util.Format.number( value, numFormat );
    },

    buySell: function ( value ) {
        var buyLabel = TFCommon.Locales.getLocaleValue( 'General.Columns.Buy', 'Buy' );
        var sellLabel = TFCommon.Locales.getLocaleValue( 'General.Columns.Sell', 'Sell' );
        var cellVal = '';

        if ( value > 0 ) {
            cellVal = '<span class="buy">' + buyLabel + '</span>';
        } else if ( value < 0 ) {
            cellVal = '<span class="sell">' + sellLabel + '</span>';
        }

        return cellVal;
    },

    numberCashAmount: function ( value ) {
        var cellVal = '';
        if ( value >= 0 ) {
            cellVal = '<span class="black-text">' + Ext.util.Format.number( value, TFAppConfig.displayFormats.fmtCashAmount ) + '</span>';
        } else if ( value < 0 ) {
            cellVal = '<span class="red-text">' + Ext.util.Format.number( value, TFAppConfig.displayFormats.fmtCashAmount ) + '</span>';
        }

        return cellVal;
    },

    numberCashAmtBuy: function ( value ) {
        return '<span class="buy">' + Ext.util.Format.number( value, TFAppConfig.displayFormats.fmtCashAmount ) + '</span>';
    },

    numberCashAmtSell: function ( value ) {
        return '<span class="sell">' + Ext.util.Format.number( value, TFAppConfig.displayFormats.fmtCashAmount ) + '</span>';
    },

    bookingOpngPrice: function ( value, metadata, record ) {
        var OpClCode = record.get( 'OpClCode' );

        if(OpClCode === 'Opn') {
            return '';
        } else {
            var fmtInstPrcFormat = record.get( 'PrcFormat' );
            var cellVal = '';

            fmtInstPrcFormat = (Ext.isEmpty( fmtInstPrcFormat )) ? TFAppConfig.displayFormats.fmtPrice : fmtInstPrcFormat;

            if ( value >= 0 ) {
                cellVal = '<span class="black-text">' + Ext.util.Format.number( value, fmtInstPrcFormat ) + '</span>';
            } else if ( value < 0 ) {
                cellVal = '<span class="red-text">' + Ext.util.Format.number( value, fmtInstPrcFormat ) + '</span>';
            }

            return cellVal;
        }
    },

    bookingOpngDate: function ( value, metadata, record ) {
        var OpClCode = record.get( 'OpClCode' );
        return (OpClCode === 'Opn') ? '' : value;
    },

    redNumberLots: function ( value ) {
        if ( value > 0 ) {
            return '<span style="color:green;">' + Ext.util.Format.number( value, TFAppConfig.displayFormats.fmtLots ) + '</span>';
        } else if ( value < 0 ) {
            return '<span style="color:red;">' + Ext.util.Format.number( value, TFAppConfig.displayFormats.fmtLots ) + '</span>';
        }
    },

    redNumberInt: function ( value ) {
        if ( value > 0 ) {
            return '<span style="color:green;">' + Ext.util.Format.number( value, TFAppConfig.displayFormats.fmtInt ) + '</span>';
        } else if ( value < 0 ) {
            return '<span style="color:red;">' + Ext.util.Format.number( value, TFAppConfig.displayFormats.fmtInt ) + '</span>';
        }
    },

    redNumberCashAmount: function ( value ) {
        if ( value > 0 ) {
            return '<span style="color:green;">' + Ext.util.Format.number( value, TFAppConfig.displayFormats.fmtCashAmount ) + '</span>';
        } else if ( value < 0 ) {
            return '<span style="color:red;">' + Ext.util.Format.number( value, TFAppConfig.displayFormats.fmtCashAmount ) + '</span>';
        }
    },

    percentCover: function ( value ) {
        if ( value > 0 ) {
            return '<span style="color:green;">' + Ext.util.Format.number( value, TFAppConfig.displayFormats.fmtPercent ) + '% </span>';
        } else if ( value < 0 ) {
            return '<span style="color:red;">' + Ext.util.Format.number( value, TFAppConfig.displayFormats.fmtPercent ) + '% </span>';
        }
    },

    shortFormat : function( value ) {
        if ( !Ext.isEmpty( value ) ) {
            return '<span class="sell">' + Ext.util.Format.number( value, TFAppConfig.displayFormats.fmtCashAmount ) + '</span>';
        } else {
            return '';
        }
    },

    longFormat : function( value ) {
        if ( !Ext.isEmpty( value ) ) {
            return '<span class="buy">' + Ext.util.Format.number( value, TFAppConfig.displayFormats.fmtCashAmount ) + '</span>';
        } else {
            return '';
        }
    },

    volumeFormat: function (value) {
        var notAllowedValue = ['0', 0, null, '', undefined];

        if ( Ext.Array.contains( notAllowedValue, value ) ) {
            value = '';
        }

        return value;
    },

    // A number column formatter that has no styles on HTML dom.
    // It only formats the number value.
    defaultColNumberFormat: function ( value, metadata, record ) {
        // Use the default format price if record has a defined price format
        var defaultPrcFormat = record.get( 'PrcFormat' );

        // Use the default formatter on the config if the record doesn't provide a price format
        if ( Ext.isEmpty( defaultPrcFormat ) ) {
            defaultPrcFormat = TFAppConfig.displayFormats.fmtPrice;
        }

        return Ext.util.Format.number( value, defaultPrcFormat );

    },

    // Added new price format the has fill color
    // Black - number that is greater than or equal to 0
    // Red   - number that is lesser than 0
    prcFormatBlackRedColor: function ( value, metadata, record ) {

        var prcFormat = record.get( 'PrcFormat' );

        // Check if the record has defined prcFormat, if not, then use the prcformat defined in config file
        prcFormat = Ext.isEmpty( prcFormat ) ? TFAppConfig.displayFormats.fmtPrice : prcFormat;

        if ( value >= 0 ) {
            return '<span class="black-text">'+ Ext.util.Format.number( value, prcFormat ) +'</span>';
        } else if ( value < 0 ) {
            return '<span class="red-text">'+ Ext.util.Format.number( value, prcFormat ) +'</span>';
        }
    }
} );