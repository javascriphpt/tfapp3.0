'use strict';
Ext.define( 'TFCommon.controller.Connection', {
    extend: 'Ext.app.Controller',

    requires : [
        'TFCommon.util.Exception'
    ],

    refs: [
        {
            ref     : 'statusText',
            selector: 'app-header #statusText'
        }
    ],

    config : {
        isDisconnected             : false,
        isResumedFromDisconnection : false
    },

    init: function () {


        Ext.Ajax.setTimeout(60000);
        Ext.util.Observable.observe(Ext.Ajax);

        this.checkRequestComplete();
        this.checkRequestException();

    },

    checkRequestComplete : function () {
        var that = this;
        var errorResourceKeys = ['Ref1', 'InvalidSession', 'SessionExpired', 'UnauthorizedRequest', 'Error', 'DownloadFailed'];
        var getLocale = TFCommon.Locales.getLocaleValue;
        var statusText;
        var t = null;

        Ext.Ajax.on( 'requestcomplete', function ( dataconn, response, opt ) {

            statusText = that.getStatusText();
            that.setIsDisconnected( false );

            if ( statusText ) {
                if( statusText.isVisible() ) {
                    if( t === null ) {
                        t = setTimeout(function(){
                            console.log('do hide here');
                            statusText.hide();
                            clearTimeout(t);
                            t = null;
                        },3000);
                    }

                    /*statusText.animate({
                        to : {
                            display: 'none',
                        },
                        duration : 3000,
                        listeners: {
                            afteranimate: function () {
                                statusText.hide();
                            }
                        }
                    });*/
                }
            }

            if ( response.responseText !== null ) {

                var responseText = Ext.JSON.decode( response.responseText );
                var msg;

                // Ext.Msg.hide();

                if ( responseText.success === 'false' && Ext.Array.contains( errorResourceKeys, responseText.resourcekey ) ) {

                    msg = responseText.messagekey;

                    if (responseText.resourceKey !== 'UnauthorizedRequest' && opt.params.id) {
                        msg += '<br />'+ getLocale( 'Error.Label.DataRequestId', 'DataRequestId: ' ) + opt.params.id;
                    }

                    TFCommon.util.Exception.logError(
                        TFAppConfig.error.msgView.PROMPT,
                        TFAppConfig.error.code.SESS_ERR,
                        responseText.messagekey,
                        msg,
                        function () {
                            setTimeout(
                                function () {
                                    if (responseText.resourceKey !== 'DownloadFailed') {
                                        TFAppConfig.getApp().getApplication().getController( 'Session' ).onClickUserLogoutBtn();
                                    }
                                },
                                100
                            );
                        }
                    );

                }
            }

        });
    },

    checkRequestException : function () {
        var that = this,
            cssHeaders = [
                'header-error-message',
                'header-warning-message',
                'header-info-message',
                'header-success-message'
            ],
            statusText;

        Ext.Ajax.on('requestexception', function ( dataconn, response ) {

            console.log( 'requestexception', response );


            Ext.Msg.hide();


            if( response.status === 0 && !response.timedout) {

                that.setIsDisconnected( true );

                var retValue = TFCommon.util.Exception.logError(
                    TFAppConfig.error.msgView.STATUS,
                    TFAppConfig.error.code.SRV_CONN_ERR,
                    TFCommon.Locales.getLocaleValue( TFAppConfig.error.title.SRV_CONN_ERR_KEY,  TFAppConfig.error.title.SRV_CONN_ERR ),
                    TFCommon.Locales.getLocaleValue( TFAppConfig.error.msg.SRV_CONN_ERR_KEY,    TFAppConfig.error.msg.SRV_CONN_ERR )
                    );

                if( retValue !== false)
                {
                    statusText = that.getStatusText();
                    statusText.removeCls(cssHeaders);

                    if(statusText) {
                        statusText.addCls('header-error-message');

                        if( !statusText.isVisible() ) {
                            statusText.show();
                        }

                        statusText.setText(retValue);
                    }
                }
            }
        });
    }
} );