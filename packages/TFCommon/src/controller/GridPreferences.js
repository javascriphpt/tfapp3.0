'use strict';

Ext.define('TFCommon.controller.GridPreferences', {
	extend : 'TFCommon.controller.Preferences',

	config: {
		// List of stores to save on sort
		appStoreNamesSortSave: [],
		// List of stores with sort configs
		appStoreSortConfigsMap: {}

	},

	loadGridChanges : function( ctx, grid, alternateXType, callback ){
		/*filter:[{"SetOp":"OR","FilterSet":[{"Field":"ItemId","Operator":"LIKE","Value":["appnotifconfig"],"ElementOp":"OR"},{"Field":"ItemId","Operator":"LIKE","Value":["appgridconfig"],"ElementOp":"OR"}]},{"SetOp":"AND","FilterSet":[{"Field":"LocaleId","Operator":"LIKE","Value":[1],"ElementOp":"AND"}]}]
		id:693
		page:1
		start:0
		limit:-1
		sort:[{"property":"SortSeq","direction":"ASC"}]*/




		var gridDefaultCols	= grid && grid.headerCt && grid.headerCt.getGridColumns(),
			cmp				= alternateXType || grid.getXType(),
			getLocaleId		= TFCommon.Locales.getLocaleId,
			that			= this,
			filter,
			params,
			results,
			data,
			dataLen,
			dataIndex,
			cols,
			colsLen,
			colIndex,
			col,
			defaultCol,
			defaultColIndex,
            isAutoSize = false;

		// check if Save Grid changes from preferences is checked
		if ( !!TFAppConfig.preferences.appgridconfig && TFAppConfig.preferences.appgridconfig.length > 0 ) {

			filter = Ext.encode(
				[
					{
	                    "SetOp": "AND",
	                    "FilterSet": [
		                    {
		                        "Field"    : "ItemId",
		                        "Operator" : "LIKE",
		                        "Value"    : [cmp],
		                        "ElementOp": "AND"
		                    }, {
		                        "Field"    : "PreferenceTypeId",
		                        "Operator" : "EQ",
		                        "Value"    : [1],
		                        "ElementOp": "AND"
		                    }, {
		                        "Field"    : "LocaleId",
		                        "Operator" : "EQ",
		                        "Value"    : [getLocaleId()],
		                        "ElementOp": "AND"
		                    }
		                ]
		            }
		        ]
		   	);

		   	params = {
		   		filter	: filter,
		   		id		: 693,
		   		page	: 1,
		   		start	: 0,
		   		limit	: -1,
		   		sort	: Ext.encode([
		   			{
		   				property	: 'SortSeq',
		   				direction	: 'ASC'
		   			}
		   		])
		   	};

		   	//send request to server to save user preferences
            Ext.Ajax.request( {
                url          : TFApps.config.dataRequest.url + '/TFData/Get',
                method : 'POST',
                timeout : 120000,
                params : params,
                success: function ( response ) {
                    results	= Ext.decode( response.responseText );
                    data	= results.data;
                    dataLen	= data.length;

                    if ( grid.isVisible() ) {
                        for( dataIndex = dataLen; dataIndex--; ) {
                            cols = Ext.decode( data[dataIndex].ItemValue );

                            colsLen = cols.length;

                            for( colIndex = colsLen; colIndex--;){
                                col = cols[colIndex];
                                defaultColIndex = that._getDefaultColIndex( col, gridDefaultCols );


                                defaultCol = gridDefaultCols[defaultColIndex];

                                if ( typeof defaultCol !== 'undefined' ) {

                                    isAutoSize = false;

                                    if( defaultCol.hasOwnProperty( 'autoSizeColumn' ) && defaultCol.autoSizeColumn === true ) {
                                        // Change the status of autoSize to true
                                        isAutoSize = true;
                                        // Do the autosizing of columns
                                        defaultCol.autoSize();
                                    }

                                    if ( defaultCol.hasOwnProperty( 'configurable' ) === true && defaultCol.configurable === false ) {
                                        continue;
                                    }

                                    if( defaultCol.flex ) {
                                        delete defaultCol.flex;
                                    }

                                    if ( isAutoSize === false ) {
                                        defaultCol.setWidth( col.width );
                                    }

                                    // if( defaultCol.hasOwnProperty( 'autoSizeColumn' ) && defaultCol.autoSizeColumn === true ) {
                                    //  defaultCol.autoSize();
                                    // } else {
                                    //  defaultCol.setWidth( col.width );
                                    // }

                                    if ( col.hidden === true ) {
                                        defaultCol.setVisible( false );
                                    } else {
                                        defaultCol.setVisible( true );
                                    }
                                }
                            }
                        }
                    }



                    callback( ctx );

                },
                failure: function ( response ) {
                    results = Ext.decode( response.responseText );
                    Ext.Msg.alert( 'Config', results.errorMessage );
                },

                scope : this
            } );
		}
	},

	_getDefaultColIndex : function( col, defaultCols ) {
        var defaultColsLen = defaultCols.length,
            ctr,
            column;

        for( ctr = defaultColsLen; ctr--;) {
            column = defaultCols[ ctr ];

            if( column.hasOwnProperty( 'dataIndex' ) && col.hasOwnProperty( 'dataIndex' ) ) {
                if(
                    col.dataIndex === column.dataIndex ||
                    col.dataIndex === (column.dataIndex + 'Code') ||
                    col.dataIndex === (column.dataIndex + 'Name')
                ) {
                    return ctr;
                }
            }
        }

        return false;
    },

	saveGridChanges : function( component, hdrContainer ) {
		var gridColumns = hdrContainer.getGridColumns(),
			itemValue   = [],
			gridColumnIndex;

        for ( gridColumnIndex in gridColumns ) {

        	if( gridColumns.hasOwnProperty( gridColumnIndex ) ) {
        		var col = gridColumns[gridColumnIndex];

                var colConfig = {
                    'dataIndex': col.dataIndex,
                    'width'    : col.width,
                    'hidden'   : col.hidden
                };

                itemValue.push( colConfig );
        	}

        }

        if ( !!TFAppConfig.preferences.appgridconfig && TFAppConfig.preferences.appgridconfig.length > 0 ) {
            this.saveUserPreferences( 1, component, itemValue, true, Ext.emptyFn );
        }
	},

	// Store Sort
	// Used on grids

    // Applies the saved app store sort
    // @cb - callback
    applyAppStoreSort: function (cb) {
    	var that = this;

    	if ( !!TFAppConfig.preferences.appgridconfig && TFAppConfig.preferences.appgridconfig.length > 0 ) {

            this.getStoreSortPreference(function (success, rec) {
            	if (success === true) {
            		if (!Ext.isEmpty(rec)) {
            			that.restoreSavedStoreSort(Ext.decode(rec));
            		}
            		cb(null);
            	} else {
            		cb(false);
            	}
            	// Add listner on store sort
            	// Attach after restoring so saving won't trigger
    			that.attachStoreSortListener();
            });

        } else {
        	cb(false);
        }
    },

    // Save remotely the store sort
	saveStoreSortPreference: function () {
        var preferencesCtlr = TFAppConfig.getApp().getApplication().getController( 'Preferences' );

        var itemValue = this.getAppStoreSortConfigsMap();

        preferencesCtlr.saveUserPreferences( 1, 'app-' + TFAppConfig.appNameSpace + '-store-sort', itemValue, true, Ext.emptyFn );

	},
	// Get the remote saved store sort preference
	getStoreSortPreference: function (cb) {
		// Valud name will be
		// e.g. app-TFClientMon-store-sort
		var params = {
			filter: Ext.encode([ {
				"SetOp": "AND",
				"FilterSet": [ {
					"Field": "ItemId",
					"Operator": "LIKE",
					"Value": [ "app-" + TFAppConfig.appNameSpace + "-store-sort" ],
					"ElementOp": "AND"
				}, {
					"Field": "PreferenceTypeId",
					"Operator": "EQ",
					"Value": [ 1 ],
					"ElementOp": "AND"
				} ]
			} ]),
			id: 693,
            page: 1,
			start: 0,
			limit: -1,
			sort: Ext.encode([ {
				"property": "SortSeq",
				"direction": "ASC"
			} ])
		};

		Ext.Ajax.request( {
				url     : TFApps.config.dataRequest.url + '/TFData/Get',
				method 	: 'POST',
				timeout : 120000,
				params 	: params,
				success	: function ( response ) {
					var results	= Ext.decode( response.responseText );

					if (results.data && results.data.length) {
						cb(true, results.data[0].ItemValue);
					} else {
						cb(null, null);
					}
				},
				failure	: function ( response ) {
					results = Ext.decode( response.responseText );
					Ext.Msg.alert( 'Config', results.errorMessage );

					cb(false, null);
				},
				scope : this
		} );
	},

	// Restores the Saved sorting
	// Example
	// {
	// 	'clientMon.Bookinglist': [
	// 		{"property": "RegGrpCode", "direction": "ASC"},
	// 		{"property": "FillTime", "direction": "DESC"},
	// 		{"property": "Booking", "direction": "DESC"}
	// 	]
	// }
	restoreSavedStoreSort: function (recs) {
		var store;

		var storeName;
		// Loop through the saved store sort
		for (storeName in recs) {
			if (recs.hasOwnProperty(storeName)) {
				store = Ext.getStore(storeName);
				if (store) {

					// Clear the default sort
					store.getSorters().clear();
					// BUG FIX
					// Tried using suspendEvent but store load was still triggering
					// Using the private method blockLoad to prevent store from loading
					// when settings sorters
					store.blockLoad(true);
					// Set the previous store sort
					store.setSorters(recs[storeName]);
					// Unblock Load
					store.unblockLoad(true);
					// store.resumeEvents(true);
				}
			}
		}
	},

	// Map the store sort on first load to cache
	attachStoreSortListener: function () {
		var i, j, storeConfig, storeName, store, configItem = {};
		var appStoreNamesSortSave = this.getAppStoreNamesSortSave();

		if (appStoreNamesSortSave.length) {

			for (i = 0; i < appStoreNamesSortSave.length; i++) {
				// Get the store object
				store = Ext.getStore(appStoreNamesSortSave[i]);

				if (store) {
					// Get the store name
					storeName =  appStoreNamesSortSave[i];
					// Extract sort config
					storeConfig = this.extractStoreSortConfig(store);
					// Build the object property first
					configItem[storeName] = storeConfig;
					// Add to store configs
					this.getAppStoreSortConfigsMap()[storeName] = storeConfig;
					configItem = {};
					// Add listner handler
					store.addListener({
						scope: this,
						sort: this.storeSortListener
					});
				}
			}
		}
	},

	// Extract store sort config
	extractStoreSortConfig: function (store) {
		var config = [];
		var storeSortConfig = store.getSorters().items;
		var i;

		for (i = 0; i < storeSortConfig.length; i++) {
			// Get the direction and property config
			config.push({
				direction: storeSortConfig[i]._direction,
				property: storeSortConfig[i]._property
			});
		}

		return config;

	},
	// Store sort listener handler
	storeSortListener: function (store) {
		var storeSortConfigMap = this.getAppStoreSortConfigsMap();
		var storeId = store.getStoreId();
		// Check grid preferences before processing
		if ( !!TFAppConfig.preferences.appgridconfig && TFAppConfig.preferences.appgridconfig.length > 0 ) {

			if (storeSortConfigMap[storeId]) {
				this.getAppStoreSortConfigsMap()[storeId] = this.extractStoreSortConfig(store);
			}

			this.saveStoreSortPreference();
		}
	}

});
