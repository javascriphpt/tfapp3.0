'use strict';
Ext.define( 'TFCommon.controller.Locale', {
    extend: 'Ext.app.Controller',

    ready: false,

    /*
    * @version  3.0
    * @description Listen/handle to events emmitted by controllers instead of function calls
    */
    listen : {
        controller : {
            '#MainController' : {
                loadLocale : 'loadLocale'
            },

            '#LoginViewController' : {
                beforerendercombolocale : 'onBeforeRenderComboLocale',
                selectcombolocale       : 'onSelectComboLocale'
            },

            '#HeaderViewController' : {
                beforerendercombolocale : 'onBeforeRenderComboLocale',
                selectcombolocale       : 'onSelectComboLocale'
            }
        }
    },

    init: function () {

        TFCommon.Locales = {
            userLocale    : TFAppConfig.locale,
            values        : {},
            getLocaleValue: this.getLocaleValue,
            getUserLocale : this.getUserLocale,
            getLocaleId   : this.getLocaleId,
            showByName    : this.showByName,
            showByCode    : this.showByCode
        };

    },

    /**
    * @version 2.2, update for 3.0
    * @param {Function} mainCb the callback to use after loading locales
    * @description  Loads the locale to use
    *               Loads the Ext and Server source locale
    */
    loadLocale: function (mainCb) {
        var that = this;
        var locale = this.getLocale();

        Ext.log( { indent : 1 }, 'load locale handler' );

        async.series({
                local : Ext.bind( that.loadLocaleFromLocal, that ),
                server: Ext.bind( that.loadLocaleFromServerAsync, that )
            },
            function( err, msg ) {
                Ext.log( { outdent : 1 }, 'end load locale handler' );
                mainCb(err, msg);
            }
        );
    },

    /**
     * @version 3.0
     * @param  {Function} async callback load locale from local
     */
    loadLocaleFromLocal : function( cb ) {
        var locale = this.getLocale(),
            that   = this;

        // Load ext locale
        that.extLocaleLoader.apply(that, [function ( res ) {

            if ( res.error ) {
                Ext.Msg.alert(
                    that.getLocaleValue( 'Locale.Error.Title', 'Locale Error'),
                    that.getLocaleValue( 'Locale.Error.Message', 'Error loading Ext locale for ') +' '+ locale
                );
            } else {
                cb( null );
            }

        }, locale]);
    },

    /**
     * @version 3.0
     * @param  {Function} async callback load locale from server
     */
    loadLocaleFromServerAsync : function( cb ) {
        var locale = this.getLocale(),
            that   = this;

        // Load locale from server
        that.loadLocaleFromServer.apply(that, [function (err, res) {
            if (err === null) {
                // Set the locale values
                Ext.log('set local values here');
                that.setAppLocaleValues(res);
                cb(null);
            } else {
                // Error on loading locale
                cb(err, res);
            }
        }, locale]);    
    },

    /**
     * @version 2.2, update for 3.0
     * @description Setup combobox locale
     * @param  {Object} combo Combobox
     */
    onBeforeRenderComboLocale: function ( combo ) {
        combo.getStore().loadData( this.getAvailableLocales() );
        combo.setValue( this.getUserLocale(), true );
    },

    /**
     * @version 2.2, update for 3.0
     * @description combobox locale select listener
     * @param  {Object} combo combobox
     * @param  {Object} record model
     */
    onSelectComboLocale: function ( config ) {
        
        var queryString = Ext.Object.toQueryString(
            Ext.apply(Ext.Object.fromQueryString(location.search), {"lang": config.rec.get( 'localeCode' )})
        );

       location.search = queryString;
    },






    

    

    // Gets the available locale supported for the running app
    // @return - (array) list of locales
    getAvailableLocales: function () {
        var localesArr = [];
        var localeItem;

        if ( typeof TFAppConfig.locales === 'object' ) {
            for ( localeItem in TFAppConfig.locales ) {

                if ( TFAppConfig.locales.hasOwnProperty( localeItem ) ) {

                    localesArr[localesArr.length] = {
                        localeId  : TFAppConfig.locales[localeItem].id,
                        localeCode: localeItem,
                        localeName: TFAppConfig.locales[localeItem].label
                    };

                }

            }
        }

        return localesArr;
    },

    // Gets the corresponding locale value cached from the server
    // @key - the key to lookup
    // @defaultVal - the default value to use if key value is not found
    // @return - (str) the locale value
    getLocaleValue: function ( key, defaultVal ) {

        var val = '';

        if ( TFCommon.Locales.values.hasOwnProperty( key ) ) {

            val = TFCommon.Locales.values[key];

            return Ext.isEmpty( val ) ? (Ext.isDefined( defaultVal ) ? defaultVal : 'VAL_MISSING_' + key) : val;

        } else {

            if ( Ext.isDefined( defaultVal ) ) {
                return defaultVal;
            } else {
                return 'VAL_MISSING_' + key;
            }

        }

    },

    // Sets the selected user locale for the session
    setUserLocale: function ( locale ) {
        // Set the root class style
        Ext.getBody().addCls( 'tf tf-' + locale );

        TFCommon.Locales.userLocale = locale;
    },

    // Gets the user locale
    getUserLocale: function () {
        return TFCommon.Locales.userLocale;
    },

    // Gets the locale Id from the config based on the user locale
    getLocaleId : function() {
        var locales = TFAppConfig.locales,
            userLocale  = TFCommon.Locales.userLocale,
            localeId = 0;

        if(locales.hasOwnProperty(userLocale)) {
            localeId = locales[userLocale].id;
        }

        return localeId;
    },

    // Gets the browser supported locale
    // @return - the browser locale in 2 characters
    getBrowserLocale: function () {
        var browserLocale = window.navigator.userLanguage || window.navigator.language;

        if ( browserLocale ) {
            browserLocale = browserLocale.substr( 0, 2 );
        }

        return browserLocale;

    },

    // Determines the locale to use
    // A. If url has a locale param
    // B. Use the browser locale
    // @return - (str) the locale determined to use
    getLocale: function () {

        // get the selected language code parameter from url (if exists)
        var params = Ext.urlDecode( window.location.search.substring( 1 ) );
        var browserLocale = this.getBrowserLocale();
        var locale = TFAppConfig.locale;

        if ( params.lang ) {
            // Use the browser location lang
            if ( TFAppConfig.locales.hasOwnProperty( params.lang ) ) {
                this.setUserLocale( params.lang );
                locale = params.lang;
            }

        } else if ( TFAppConfig.locales.hasOwnProperty( browserLocale ) ) {
            // Use the default browser lang
            this.setUserLocale( browserLocale );
            locale = browserLocale;
        }

        return locale;
    },

    // Checks if locale will use 'name' for values
    // Example: Japanese
    // @return - boolean
    showByName: function () {
        return TFAppConfig.locales[this.getUserLocale()].showByName;
    },

    // Checks if locale will use 'code' for values
    // Example: English
    // @return - boolean
    showByCode: function () {
        return !TFAppConfig.locales[this.getUserLocale()].showByName;
    },

    // Ext File locale loader
    // @cb - callback method
    // @locale - the locale to load
    extLocaleLoader: function ( cb, locale ) {

        var formattedResourcePath = Ext.util.Format.format( "TFCommon/ext/locale/ext-locale-{0}.js", locale );
        var url = Ext.getResourcePath( formattedResourcePath, 'shared');

        Ext.log( 'Load local locale' );

        Ext.Loader.loadScript( {
            url    : url,
            onLoad : cb,
            onError: cb
        } );

    },

    // Locale loader from server
    // @cb - callback method
    // @locale - the locale to load
    loadLocaleFromServer: function (cb, locale) {

        var that = this;

        Ext.log( 'Load server locale' );

        Ext.Ajax.request({
            url    : TFApps.config.dataRequest.url + '/Resource/GetResources',
            params : {
                locale : locale,
                keyType: TFAppConfig.localeKeyType
            },
            method : 'POST',
            success: function (response) {

                var results = Ext.decode(response.responseText, true);

                if (results && results.total) {
                    cb(null, results);
                } else {
                    cb(false, {
                        showMsgBox: true,
                        title     : that.getLocaleValue( 'Locale.Error.Title', 'Locale Error'),
                        msg       : that.getLocaleValue( 'Locale.Error.LoadingApp.Message', 'Error loading app locale.' )
                    });
                }

            },

            failure: function () {

                cb(false, {
                    showMsgBox: true,
                    title     : that.getLocaleValue( 'Locale.Error.Title', 'Locale Error'),
                    msg       : that.getLocaleValue( 'Locale.Error.LoadingApp.Message', 'Error loading app locale.' )
                });

            }
        });

    },

    // Sets the server locale values to our common object
    // @response - the server response for locale
    setAppLocaleValues: function (response) {

        var localeVals = (response.data) ? response.data[0] : {};

        Ext.log( 'Set app local values' );
        TFCommon.Locales.values = localeVals;
    }

} );
