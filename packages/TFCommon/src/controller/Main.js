'use strict';
Ext.define( 'TFCommon.controller.Main', {
    extend: 'Ext.app.Controller',
    id    : 'MainController',


    refs: [
        {
            ref     : 'mainViewport',
            selector: 'app-mainViewport'
        }
    ],

    /**
    * @version  3.0
    * @description Listen/handle to events emmitted by controllers instead of function calls
    */
    listen : {
        controller : {
            '#MainViewController' : {
                setupViewport : 'onSetupViewport'
            },

            '#SessionController' : {
                showloginpanel : 'showLoginPanel',
                createmainapppanel : 'createMainAppPanel'
            }
        }
    },

    init: function () {

        // Sets the page title
        this.setPageTitle();

    },

    setPageTitle: function () {
        document.title = TFAppConfig.appTitle;
    },

    /**
    * @version 3.0
    * @description Use function binds
    */
    onSetupViewport: function () {
        var that       = this,
            showMsgBox = false,
            alertTitle, alertMsg;

        Ext.log({ indent : 1 }, 'setup viewport');
        // Load data before starting the application
        // This is before the session starts
        async.series({
            loadStartupData: Ext.bind( that.loadStartupData, that )
        }, Ext.bind( that.setupViewportCallback, that ) );
    },

    /**
     * @version 3.0
     * @param  {Function} cb async callback
     * @description Load data needed for starting up the application
     */
    loadStartupData: function (cb) {
        // Load locale
        Ext.log( 'Load Startup Data' );

        // Locale.js controller should be listening to this event
        this.fireEvent( 'loadLocale', cb );
    },

    /**
     * @version 3.0
     * @description setup view port async callback
     * @param  {Boolean} err check for errors
     * @param  {Object} msg the result
     */
    setupViewportCallback : function (err, msg) {

        if (err) {

            Ext.Object.each(msg, function (key, val) {
                if (val) {
                    showMsgBox = val.showMsgBox;
                    alertTitle = val.title;
                    alertMsg = val.msg;
                    return false;
                }
            });

            if (showMsgBox) {
                Ext.Msg.alert(alertTitle, alertMsg);
            }

        } else {

            // Check the user session
            this.checkUserSession();

        }

        Ext.log({ outdent : 1 }, 'end setup viewport');
    },

    // Checks the user session on the Session controller
    /**
     * @version 2.2, update for 3.0
     * @description check user session
     */
    checkUserSession: function () {
        Ext.log( 'Check user session' );

        // Session.js controller should be listening to this event
        this.fireEvent( 'checkusersession' );
    },

    /**
     * @version 2.2, update for 3.0
     * @description Create and show the login panel
     */
    showLoginPanel: function () {

        Ext.log( { indent : 1 }, 'Start show login panel' );
        this.getMainViewport().remove( this.getMainViewport().child() );

        this.getMainViewport().add( {
            xtype: 'app-loginPanel'
        } );

        Ext.log( { outdent : 1 }, 'End show login panel' );
    },

    /**
     * Create Main App Panel
     * @version 2.2
     */
    createMainAppPanel: function () {
        this.getMainViewport().remove( this.getMainViewport().child() );

        this.getMainViewport().add( {
            xtype: 'app-main'
        } );
    },

    destroyItems: function () {
        this.getMainViewport().child().destroy();
    }
} );
