'use strict';
Ext.define( 'TFCommon.controller.Notifications', {
    extend  : 'Ext.app.Controller',

    requires: ['TFCommon.ux.window.Toast'],

    config  : {
        // notificationList: null
    },

    // Contains a lists of notification @id and @signalTypeId for notifications.
    // @id = developer defined id
    // @signalTypeId = it's defined on DB side
    notificationTypes: {},

    init     : function () {
        this.messageWindowPreview = false;

        // set a reference for getting locale
        this.getLocale = TFCommon.Locales.getLocaleValue;

        // init available notifictionLists
        this.initPreferenceTypeConfig( {
            name: 'notifications'
        } );
    },

    // It initializes the notification config to list all the available notification for this app
    initPreferenceTypeConfig: function () {

        var notificationCfg = TFAppConfig.preferenceType;
        var dataRequestCfg  = TFAppConfig.dataRequestIds;
        var preferenceType;
        var index;

        // this is to prevent error if app didn't specify a preferenceType config
        if (!notificationCfg) {
            notificationCfg = {};
        }

        // Set the notificationList if it is not yet set
        if ( Ext.Object.isEmpty( this.notificationTypes ) ) {
            for ( index in notificationCfg ) {
                // a preferenceType value
                preferenceType = notificationCfg[index];

                // Checks if data request name exist
                if ( dataRequestCfg[preferenceType] ) {

                    // A custom notification config for 8 = {orderUpdate and fillUpdate}
                    if ( parseInt(index, 10) === 8 ) {
                        // Make two order and fill in one array
                        this.notificationTypes[index] = Ext.Array.merge( dataRequestCfg.orderUpdate, dataRequestCfg.fillUpdate );
                    } else {
                        this.notificationTypes[ index ] = dataRequestCfg[preferenceType];
                    }
                }
            }
        }

    },

    // @return - Returns the priority code if set
    // by default, the value is 0 if it is not set
    getPreferencePriorityCode: function () {
        // notification preference
        var notificationPref = TFAppConfig.preferences.appnotifconfig;
        var priorityCode = 2;

        if ( notificationPref && notificationPref.hasOwnProperty('showPopup') ) {
            priorityCode = notificationPref.showPopup;
        }

        // if alert priority is not set, by default, set to 2 => info
        return priorityCode;
    },

    // It will display allowed messages of that specific alert priority code from notification preference ( show alert priority )
    // Legend: I => info; W => warn; E => error
    // @return - Returns allowed alert messages
    getAllowedAlerts: function () {

        var alertPriorityCode = this.getPreferencePriorityCode();
        var allowedAlerts;

        switch ( alertPriorityCode ) {
            // none
            case 0: allowedAlerts = [];
                break;
            // info
            case 2: allowedAlerts = ['I', 'W', 'E'];
                break;
            // warn
            case 3: allowedAlerts = ['W', 'E'];
                break;
            // error
            case 4: allowedAlerts = ['E'];
                break;
        }

        return allowedAlerts;
    },

    // formats the content base on the tags from the database
    formatContent: function ( response, key ) {
        var content;
        var resourceKey = key;
        var resourceVal = TFCommon.Locales.getLocaleValue( resourceKey, resourceKey );

        if ( resourceVal ) {
            content = Ext.create( 'Ext.Template',
                resourceVal,
                { compiled: true }
            );

            content = content.apply( response );
        } else {
            content = resourceKey;
        }

        return content;
    },

    // Returns the reference from the pushHubMgr
    getPushHub   : function () {
        return this.getController( 'PushHubMgr' );
    },

    // Returns the available notifications for this application
    getActionCode: function () {
        return this.notificationTypes;
    },

    // Standard title base on tag from db
    getTitle     : function ( response ) {
        return this.formatContent( response, response.SummaryResourceKey );
    },

    // Standard content base on tag from db
    getContent   : function ( response ) {
        return this.formatContent( response, response.ResourceKey );
    },

    // A handler which will be triggered when there is a received message from signalR
    pushCallback : function () {
        var that = this;

        return function ( response ) {
            that.processMsg.call( that, response );
        };
    },

    // Routes the notification to its specific signalTypeIds
    routeNotification: function ( response ) {

        var id            = parseInt( response.SubscriptionId, 10 );
        var pushHubStore  = Ext.getStore('PushHubs');
        var record        = pushHubStore.findRecord('subscriptionId', id);
        var dataRequestId = record && record.get('dataRequestId');

        if ( dataRequestId ) {
            // route the notification based on the datarequestId
            switch( dataRequestId ) {
                case 509:
                case 508:
                    // OrderUpdate notification
                    // This method implement from the ClientMon/Controller/Notifications.js
                    if ( this.showOrderToastWindow ) {
                        // Add a dataRequestId for reference of orderUpdate and fillUpdate
                        response._dataRequestId = dataRequestId;
                        // toast a message on frontend
                        this.showOrderToastWindow( response, record );
                    }

                    break;

                default:
                    // default notification toast message
                    this.showToast( response );
            }
        }

    },

    // Process the received response
    processMsg   : function ( response ) {
        // route the notification to its specific destination
        this.routeNotification( response );
        // // Process the message
        // this.showToast( response );
    },

    // Creates a sound when sound configuration on my account it set
    // This will trigger every toast messages
    playSound    : function () {
        var sound;

        if ( TFAppConfig.preferences.appnotifconfig.playSound === 1 ) {

            sound = this.getPushHub().playSound;

            if ( sound ) {
                sound.play();
            }
        }
    },

    // Shows the default notifiction toast message
    showToast   : function ( response, callback ) {
        var that          = this;
        var allowedAlerts = this.getAllowedAlerts();
        var priorityCode;

        that.showToastCallback = null;

        if ( Ext.isObject( response ) && Ext.isDefined( response ) ) {
            // if the message doesn't contain a Pri key or empty value, it should be consider as I => info
            priorityCode = response.Pri || 'I';

            that.showToastCallback = callback ? callback : null;

            // Checks the SignalR alert config if enable or disable
            if ( priorityCode && Ext.Array.contains( allowedAlerts, priorityCode ) ) {

                this.createToastMessage( {
                    response     : response,
                    content      : this.getContent( response ),
                    title        : this.getTitle( response ),
                    config       : {
                        listeners: {
                            afterrender: function ( toast ) {
                                that.onAfterRenderToastWindow( toast, response );
                            }
                        }
                    }
                } );

            } else {
                // this triggers when the alert priority is silent => 0
                if ( priorityCode && !allowedAlerts.length ) {
                    if (that.showToastCallback !== null) {
                        that.showToastCallback();
                    }
                }
            }

        } else {
            console.log( 'parameter message is invalid.' );
        }
    },

    // @param data {Object}
    //      @content - html
    //      @title  - title
    //      @response - the object reference
    // @defintion - Create a toast message
    //            - We can also extend the listener from the data using the {listeners} property
    createToastMessage: function ( data ) {

        var listeners =  {
            beforeshow : this.playSound,
            scope: this
        };

        // extend the listeners if there is a defined listeners in data param
        listeners = data.config && data.config.listeners ?
                        Ext.merge( listeners, data.config.listeners ) : listeners;


        Ext.ux.toast( {
            html          : data.content,
            title         : data.title,
            width         : 500,
            stickOnClick  : false,
            hideDuration  : 1800,
            autoCloseDelay: 10500,
            closeAction   : 'destroy',
            bodyStyle     : {
                background: '#FFFFFF'
            },
            listeners     : listeners
        } );
    },

    createWindowMessagePreview: function () {
        var initWindow;

        initWindow = Ext.create( {
            xtype       : 'window',
            closeAction : 'hide',
            minHeight   : 200,
            width       : 400,
            constrain   : true,
            modal       : true,
            bodyStyle   : {
                background : '#FFFFFF',
                padding    : '20px'
            }
        } );

        return initWindow;
    },

    onAfterRenderToastWindow: function ( self, response ) {
        var initialConfig = self.getInitialConfig(), that = this, delayClickTask;

        // added delay to prevent the interuption of the
        // animated toast window when click to the body
        delayClickTask = new Ext.util.DelayedTask( function () {

            self.body.on( 'dblclick', function () {

                self.stopAnimation().close();

                if ( that.messageWindowPreview === false ) {
                    that.messageWindowPreview = that.createWindowMessagePreview();
                }

                that.messageWindowPreview.setConfig( {
                    title : initialConfig.title,
                    html  : initialConfig.html
                } );

                if ( that.messageWindowPreview.isVisible() === false ) {
                    that.messageWindowPreview.show();
                }

            } );
        } );

        delayClickTask.delay( self.slideInDuration + 200 );

        if ( that.showToastCallback !== null ) {
            that.showToastCallback();
        }
    },

    // VERSION 2
    // For subscribing to notifications
    // Checks user preferences
    subscribe: function () {
        var actionCode = this.getActionCode(),
            notificationsPreferences = this.getController( 'Preferences' ).getPreferenceValue('appnotifconfig');

        Ext.Object.each(actionCode, function (actionCodeKey, actionCodeValue) {
            var i = 0,
                subscribe = false;

            // Check user preference
            // Compare with the action code
            if ( notificationsPreferences && Ext.Array.contains(notificationsPreferences, parseInt(actionCodeKey))) {
                subscribe = true;
            }

            // Loop through the actionCode data requests
            for (i; i < actionCodeValue.length; i += 1) {
                if (subscribe) {
                    this.subscribeToPushNotifications( actionCodeValue[i] );
                } else {
                    this.unsubscribeToPushNotifications( actionCodeValue[i] );
                }
            }

        }, this);

    },

    // Subscribes individual notification data request
    // Builds the subscdription config
    subscribeToPushNotifications: function ( dataRequestId ) {
        var subscriptionConfig = {
            dataRequestId   : dataRequestId,
            subscriptionType: 'notifications',
            extDataType     : 'toast',
            callback        : this.pushCallback()
        };


        console.log(subscriptionConfig);

        this.getPushHub().subscribe(subscriptionConfig);
    },

    // Unsubscdribes a push notification
    unsubscribeToPushNotifications: function (dataRequestId) {
        var subscriptionRec = this.getPushHub().isDataRequestSubscribed(dataRequestId);

        if (subscriptionRec) {
            this.getPushHub().unsubscribe(subscriptionRec);
        }
    }

} );