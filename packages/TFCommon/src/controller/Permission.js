'use strict';
Ext.define( 'TFCommon.controller.Permission', { //optimise
    extend: 'Ext.app.Controller',

    listen : {
        controller : {
            '#SessionController' : {
                showappselectorwindow : 'showAppSelectorWindow',
                userhasapppermission  : 'userHasAppPermission'
            }
        }
    },

    /**
     * Shows a window listing the apps the user has permission
     * List item is clickable and will redirect to the application
     * @param  {Boolean} closable closable indicator
     */
    showAppSelectorWindow: function (closable) {

        Ext.log( 'Show app selector window' );
        var userApps = this.getUserApps(),
            appList = [], appWindow, i;

        // Loop through the user apps model and make an array usable for the grid
        for (i = 0; i < userApps.length; i++) {
            appList.push( [ userApps[i].get('MenuItemName'), userApps[i].get('URL') ] );
        }

        // Create the app list window
        appWindow = Ext.create('Ext.window.Window', {
            title: 'Your Applications',
            height: 200,
            width: 300,
            layout: 'fit',
            closable: closable,
            items: {
                xtype:       'grid',
                border:      false,
                hideHeaders: true,
                columns:     [{
                    header:    'Applications',
                    dataIndex: 'text',
                    flex:      1,
                    renderer:  function(value, p, model) {
                        // Function creates a link for the user apps
                        var queryString, url;

                        queryString = Ext.Object.toQueryString(
                            Ext.apply(Ext.Object.fromQueryString(location.search) )
                        );

                        url = '../' + model.get('link') + '/?'+ queryString;

                        return Ext.String.format(
                            '<b><a href="{0}" target="_blank">{1}</a></b>',
                            url,
                            value
                        );
                    }
                }],
                store:       Ext.create('Ext.data.ArrayStore', {
                    fields: ['text', 'link'],
                    data: appList
                })
            },
            buttons: [
                '->', {
                    text: 'Close',
                    hidden: !closable,
                    handler: function () {
                        appWindow.close();
                    }
                }
            ]
        });

        // Show the window
        appWindow.show();
    },

    /**
     * Returns the list of user application
     * @return {Array} list of UserMenu data model
     */
    getUserApps: function () {
        var store = Ext.getStore( 'UserMenu' );
        var appsList = [];

        store.each( function ( rec ) {
            if ( rec.get( 'MenuType' ) === 'TF.Apps' ) {
                appsList.push(rec);
            }
        } );

        return appsList;
    },

    /**
     * Check if user has permission for this app
     * @version 2.2
     * @param  {String}   userApp [description]
     * @param  {Function} cb      [description]
     */
    userHasAppPermission: function ( config ) {

        var getLocale = TFCommon.Locales.getLocaleValue;
        var userHasPermission = false;
        var that = this;

        // Lookup if user has app permission
        if (this.lookupCheckAppPermission()) {
            userHasPermission = true;
        }

        if (userHasPermission) {
            // User does not have permission
            // Show message
            config.cb(false);
        } else {
            config.cb(true, {
                showMsgBox: true,
                title     : getLocale( 'User.Error.Permission.Title', 'Permission Error' ),
                msg       : getLocale( 'User.Error.Permission.Message', 'Sorry you don\'t have permission for this application.' ),
                msgCb     : function () {
                    return that.showAppSelectorWindow();
                }
            });
        }


    },

    /**
     * Lookup the UserMenu store and check if user has app permission
     * @version 2.2
     * @return {Mixed} userhaspermission
     */
    lookupCheckAppPermission: function () {

        var store = Ext.getStore('UserMenu');
        var userHasPermission = false;

        store.findBy(function (rec) {
            if (rec.get('MenuType') === 'TF.Apps' && rec.get('MenuItemName') === TFAppConfig.AppName) {
                userHasPermission = rec;
            }
        });

        return userHasPermission;
    },

    // Checks if user has permission ( action )
    //  - returns a record
    // @actionId ( Integer ) - specifies the id
    findUserAction: function ( actionId ) {
        var rec = Ext.getStore( 'UserActions' ).findRecord( 'ActionId', actionId );

        return rec;
    },

    // Checks if user has permission ( action )
    //  - returns a boolean
    // @actionId ( Integer ) - specifies the id
    userHasAction : function ( actionId ) {
        var rec = Ext.getStore( 'UserActions' ).findRecord( 'ActionId', actionId );

        return (rec) ? true : false;
    },

    // Finds user menu
    //  - returns a record
    // @menu - name of the menu
    findUserMenu  : function ( menu ) {
        var menuType = TFAppConfig.roles.MenuType;
        var rec      = Ext.getStore( 'UserMenu' ).findRecord( 'MenuItemCode', menu );

        if ( rec && rec.get( 'MenuType' ) === menuType ) {
            return rec;
        } else {
            return null;
        }
    },

    // Checks if user has menureturns boolean
    //  - returns boolean
    // @menu - name of the menu
    userHasMenu   : function ( menu ) {
        var menuType = TFAppConfig.roles.MenuType;
        var rec      = Ext.getStore( 'UserMenu' ).findRecord( 'MenuItemCode', menu );

        if ( rec && rec.get( 'MenuType' ) === menuType ) {
            return true;
        } else {
            return false;
        }
    }

    
} );