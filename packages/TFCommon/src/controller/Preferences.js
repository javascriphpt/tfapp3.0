'use strict';

Ext.define( 'TFCommon.controller.Preferences', {
    extend: 'Ext.app.Controller',

    listen : {
        controller : {
            '#SessionController' : {
                setupstorelistener      : 'setupStoreListener',
                loadstore               : 'loadStore',
                setdefaultapppreference : 'setDefaultAppPreference'
            }
        }
    },

    init: function () {
        this._userPreferencesStore = Ext.getStore('UserPreferences');
        this._sortSeq              = 1;
        this._cb                   = Ext.emptyFn;
        this._itemId               = null;
    },

    /**
     * set up beforeload and load events of the preferences store
     * @version 2.2, upgrade in 3.0
     */
    setupStoreListener : function(){

        Ext.log( 'Set preferences store listeners' );
        if( !this._userPreferencesStore ) {
            this._userPreferencesStore = Ext.getStore('UserPreferences');
        }

        this._userPreferencesStore.on( 'beforeload', this.onStoreBeforeLoad, this );
        this._userPreferencesStore.on( 'load', this.onStoreLoad, this );
    },
    
    /**
     * load user preferences store
     * @version 2.2, upgrade in 3.0
     * @param  {Function} cb     async callback
     * @param  {String}   itemId [optional]
     */
    loadStore : function( cb, itemId ) {

        Ext.log( 'Load preferences store' );
        this._cb = cb;
        this._itemId = itemId;

        if( this._userPreferencesStore ) {
            this._userPreferencesStore.load();
        }
    },

    /**
     * set default app preference
     * @version 2.2
     * @param {Function} cb 
     */
    setDefaultAppPreference: function ( cb ) {

        Ext.log( 'set default app preference' );
        
        this._cb = cb;

        this.createDefaultPreferences();

    },

    getUserPreferencesStore : function(){
        return this._userPreferencesStore;
    },


    // [onStoreBeforeLoad callback for the onBeforeLoad event of user preferences store]
    // @see \setupStoreListener
    // @param  Ext.data.Store
    // @param  Ext.data.operation.Operation
    onStoreBeforeLoad : function( store, op ){
        var getLocaleId = TFCommon.Locales.getLocaleId,
            filter      = [{
            "SetOp": "OR",
            "FilterSet": [{
                "Field"    : "ItemId",
                "Operator" : "LIKE",
                "Value"    : ['appnotifconfig'],
                "ElementOp": "OR"
            }, {
                "Field"    : "ItemId",
                "Operator" : "LIKE",
                "Value"    : ['appgridconfig'],
                "ElementOp": "OR"
            }/*, {
                "Field"    : "ItemId",
                "Operator" : "LIKE",
                "Value"    : ['appdeskdashboards'],
                "ElementOp": "OR"
            }*/]
        }, {
            "SetOp": "AND",
            "FilterSet": [{
                "Field"    : "LocaleId",
                "Operator" : "LIKE",
                "Value"    : [getLocaleId()],
                "ElementOp": "AND"
            }]
        }];

        op.setParams( {
            filter: Ext.encode( filter )
        } );
    },


    // [onStoreLoad callback for onLoad event of user preferences store]
    // @see \setupStoreListener
    // @param  Ext.data.Store
    // @param  Ext.data.Model[]
    // @param  Boolean
    onStoreLoad : function( store, records, successful ){
        var storeLastRecord,
            sortSeq,
            preferences     = TFAppConfig.preferences,
            recLen          = records.length,
            ctr             = recLen,
            itemIds         = [],
            itemId          = this._itemId,
            record,
            prevConfigAlert;

        if ( successful ) {
            if ( records && recLen ) {

                storeLastRecord = store.last();
                sortSeq         = ( storeLastRecord )? storeLastRecord.get( 'SortSeq' ) : 0;

                // set sort sequence
                this._sortSeq = ( sortSeq ) ? ( sortSeq + 1 ) : 1;

                // initialize appnotifconfig
                if ( preferences && !preferences.appnotifconfig ) {
                    // format data for notification
                    preferences.appnotifconfig = {
                        itemId   : [],
                        playSound: 0
                    };
                }

                prevConfigAlert = Ext.clone( TFAppConfig.preferences.appnotifconfig );

                for( ctr = recLen; ctr--;) {
                    record = records[ctr];

                    if( !Ext.Array.contains( itemIds, record.get('ItemId'))) {
                        itemIds.push( record.get('ItemId') );
                        this.setPreferencesConfig( record );
                    }
                }
            }
        }

        this._cb( null );

        if ( itemId && itemId === 'appnotifconfig' ) {
            TFAppConfig.getApp().getApplication().getController( 'Notifications' ).subscribe();
        }

        if ( !TFAppConfig.preferences.hasOwnProperty( 'appgridconfig' ) ) {
            TFAppConfig.preferences.appgridconfig = [1];

        }
    },

    

    // set preferences config. This can be overridden per app
    // @param Ext.data.Model
    setPreferencesConfig : function( record ) {
        TFAppConfig.preferences[record.get( 'ItemId' )] = Ext.JSON.decode( record.get( 'ItemValue' ) );
    },

    // Gets all the preference data
    // @itemId - the preference itemId
    getPreference: function (itemId) {
        // Todo: should be a store query
        var preference = TFAppConfig.preferences[itemId];

        if (preference) {
            return preference;
        } else {
            return null;
        }
    },

    // Gets only the preference value and decoded
    // @itemId - the preference itemId
    getPreferenceValue: function (itemId) {
        // Todo: should be a store query
        var preference = TFAppConfig.preferences[itemId];

        if (preference) {
            return preference.itemId;
        } else {
            return null;
        }
    },

    // Save user preferences
    // @param int preference type id
    // @param int item id
    // @param JSON value for this item
    // @param boolean flag whether there will be a pop up notification for success/failed request on saving user preferences
    // @param Object custom function callback
    saveUserPreferences: function ( preferencetypeId, itemId, itemValue, notify, cb ) {
        var sortSeq      = this._sortSeq,
            notif        = notify || false,
            callback     = cb || null,
            that         = this,
            userId       = null,
            data         = {},
            getLocaleId  = TFCommon.Locales.getLocaleId,
            getUserStore = Ext.getStore( 'User' ),
            results;

        if ( getUserStore.getCount() ) {

            userId = getUserStore.first().get('UserId');

            data = {
                'UserId'            : userId,
                'UserPreferencesSeq': 1,
                'SortSeq'           : sortSeq,
                'AppId'             : TFAppConfig.AppId,
                'ItemId'            : itemId,
                'PreferenceTypeId'  : preferencetypeId,
                'LocaleId'          : getLocaleId(),
                'ItemValue'         : JSON.stringify( itemValue )
            };

            //send request to server to save user preferences
            Ext.Ajax.request( {
                url    : TFApps.config.dataRequest.url + '/TFData/Submit',
                method : 'POST',
                params : {
                    id    : 694,
                    action: 'Update',
                    record: Ext.JSON.encode( data )
                },
                success: function ( response ) {
                    results = Ext.decode( response.responseText );

                    if ( notif ) {
                        if ( callback !== null ) {
                            callback( that, results );
                        } else {
                            that.defaultSuccessSavePrefCallback( results, itemId );
                        }
                    }  else {
                        that.loadStore( Ext.emptyFn, itemId);
                    }
                },
                failure: function ( response ) {
                    if( !Ext.isEmpty( response.responseText ) ) {
                        results = Ext.decode( response.responseText );
                        Ext.Msg.alert( 'Config', results.errorMessage );
                    }
                }
            } );
        }
    },

    removeSavedGridPreferences : function() {
        var userId       = null,
            data         = {},
            getUserStore = Ext.getStore( 'User' ),
            results;

        if ( getUserStore.getCount() ) {

            userId = getUserStore.first().get('UserId');

            data = {
                'UserId'            : userId,
                'PreferenceTypeId'  : 1
            };
            Ext.Ajax.request( {
                url    : TFApps.config.dataRequest.url + '/TFData/Submit',
                method : 'POST',
                params : {
                    id    : 694,
                    action: 'Delete',
                    record: Ext.JSON.encode( data )
                },
                success: function ( response ) {
                    results = Ext.decode( response.responseText );
                },
                failure: function ( response ) {
                    results = Ext.decode( response.responseText );
                    Ext.Msg.alert( 'Config', results.errorMessage );
                }
            } );
        }
    },

    // Default callback on success save preferences
    // @params Object XHR response
    // @params mixed item id
    defaultSuccessSavePrefCallback : function( response, itemId ) {
        var getLocale = TFCommon.Locales.getLocaleValue;

        Ext.Msg.alert( getLocale( 'CltUserEditView.UserPreferences.Title', 'Preferences' ), getLocale( response.messagekey, response.message ) );

        this.loadStore( Ext.emptyFn, itemId);
    },

    destroy : function() {
        this.superclass.destroy.apply(this, arguments);

        this._userPreferencesStore.clearListeners();
        this._userPreferencesStore.removeAll();
        this._userPreferencesStore.destroy();

        this._sortSeq = null;
        this._cb      = null;
        this._itemId  = null;
    },

    // An abstract function - override this method from the child class.
    // Logic for defining list of default preference values.
    createDefaultPreferences: function () {
        // Make sure you consider the this._cb callback
        this._cb( null );
    }
});