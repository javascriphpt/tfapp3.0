'use strict';
Ext.define( 'TFCommon.controller.PushHubMgr', {
    extend: 'Ext.app.Controller',
    alias : 'controller.PushHubManager',
    // Contains events that other views has subscribed to this class
    appSubscribers: {},

    listen : {
        controller : {
            '#SessionController' : {
                'stopsignalrconnection' : 'stopSignalRConnection'
            }
        }
    },

    init      : function () {

        this.proxy = TFCommon.data.SignalR;

        // reference to create sound
        this.playSound = false;
        this.setupSound();

        this.debug = TFApps.config.pushHub.debug;

        // VERSION 2

        // PushHub store
        this.pushHubStore = Ext.getStore('PushHubs');

        // Connection status
        // values: disconnected, connected, connecting
        this.connectionStatus = 'disconnected';

        // Subscription collection
        // Using DataRequestId as key
        this.subcriptionCollection = new Ext.util.MixedCollection();
        // Subscription callback list for faster invoking
        // Using SubscriptionID as key
        this.subscriptionKeyToCallback = [];

        // Add SignalR proxy listeners
        this.setupProxyListeners();

    },

    // setup sound manager
    setupSound: function () {
        var that = this;

        soundManager.setup( {
            // location: path to SWF files, as needed (SWF file name is appended later.)
            url: TFApps.config.libraries.webUrl + '/soundmanager2/swf/',
            // flashVersion: 9, // default is 8

            debugMode: false,

            onready: function () {
                that.playSound = soundManager.createSound( {
                    url: TFApps.config.getResourceUrl() + '/sounds/hello.mp3'
                } );
            },

            ontimeout: function () {
                console.log( 'Missing SWF? Flash blocked? No HTML5 audio support?' );
            }
        } );
    },

    // Simulates the push notifications for faster testing
    // @pushData = accepts only json string data
    simutePushedEvents: function ( pushData ) {
        this.proxy.fireEvent('messageReceived', pushData );
    },

    // *********
    // VERSION 2
    // *********

    // CONNECTIONS
    // Starts SignalR push data
    startSignalRConnection   : function ( ) {

        if ( this.connectionStatus !== 'connecting' ) {
            this.proxy.connect();
            this.connectionStatus = 'connecting';
        }

    },

    // Stops SignalR push data
    stopSignalRConnection: function () {
        this.proxy.stopConnection();
    },

    // EO - CONNECTIONS

    // SUBSCRIPTIONS
    // To subscribe for push data
    // This will start SignalR if it's not started
    // Then add the subscription to collection
    // @subscriptionConfig - config for subscribing
    subscribe: function (subscriptionConfig) {
        if (this.connectionStatus === 'disconnected') {
            // Start connection
                this.startSignalRConnection();
        }

        // Add subscription to collection
        this.addToSubscriptionCollection(subscriptionConfig);

    },

    // Do the push subscription
    // @subscriptionConfig - config for subscribing
    doSubscribe: function (subscriptionConfig) {
        var pushHubRec;
        // Check user permission
        if (this.getController('User').findUserAction( subscriptionConfig.dataRequestId )) {
            // Add to PushHub Store
            pushHubRec = this.addToPushHubStore(subscriptionConfig);
            // Set the subscribed status of this subscription
            subscriptionConfig.subscribed = false;
            // Set the Subscription ID to the record ID
            subscriptionConfig.subscriptionId = pushHubRec.getId();
            // Save the subscription id on the record
            pushHubRec.set('subscriptionId', subscriptionConfig.subscriptionId);
            // Add success callback
            subscriptionConfig.successCallback = this.subscribeSuccessCb(subscriptionConfig);
            // Add error callback
            subscriptionConfig.errorCallback = this.subscribeErrorCb(subscriptionConfig);

            // Map the subscription callback
            this.subscriptionKeyToCallback[subscriptionConfig.subscriptionId] = subscriptionConfig.callback;

            this.proxy.subscribeToSignal( subscriptionConfig );

            console.log('doSubscribe');
            console.log(subscriptionConfig);

        } else {

            this.noPermissionToSubscribe( subscriptionConfig );

        }
    },

    noPermissionToSubscribe : function( subscriptionConfig ) {
        console.log('User does not have permission to subscribe: ' + subscriptionConfig.dataRequestId);
    },

    // Add to subscription collection
    addToSubscriptionCollection: function (subscriptionConfig) {
        // Check if the data request is already on the colleciton
        if (!this.subcriptionCollection.containsKey(subscriptionConfig.dataRequestId)) {
            // Add to collection
            // Serves as a copy so subscription can be subscribed again when disconnected
            this.subcriptionCollection.add(subscriptionConfig.dataRequestId, subscriptionConfig);
        }

        // Check if PushHub is connected
        // If true, then invoke subscribe
        if (this.connectionStatus === 'connected') {

            if ( !this.isDataRequestSubscribed( subscriptionConfig.dataRequestId ) ) {
                this.doSubscribe(subscriptionConfig);
            }
        }
    },

    // Run each collection
    runSubscriptionCollection: function () {
        this.subcriptionCollection.each(function (subscriptionConfig) {
            this.doSubscribe(subscriptionConfig);
            return true;
        }, this);
    },

    // Subscribe success callback
    // @subscriptionConfig - subscription config
    subscribeSuccessCb: function (subscriptionConfig) {
        var that = this,
            record;
        // Returns a function that the SignalR can invoke
        return function () {
            console.log( 'Subscribe Sucess DataRequestId: ' + subscriptionConfig.dataRequestId );
            // Update the connection status of this subscription

            record = that.pushHubStore.findRecord('dataRequestId', subscriptionConfig.dataRequestId);
            record.set('subscribed', true, {dirty: false});

            if (record.get('subscribed') === true && that.subcriptionCollection.get(record.get('dataRequestId')).connectCallback) {
                // Invoke the disconnect callback
                that.subcriptionCollection.get(record.get('dataRequestId')).connectCallback();
            }

            if( record.get('subscribed') === true ) {
                that.fireEvent( 'pushMessageSubscribed', record );
            }
        };
    },

    // Subscribe error callback
    // @subscriptionConfig - subscription config
    subscribeErrorCb: function (subscriptionConfig) {
        // Returns a function that the SignalR can invoke
        return function (response) {
            console.log( 'Subscribe ERROR DataRequestId: ' + subscriptionConfig.dataRequestId );
            console.log( response );
        };
    },

    // Unsubscribe to a push service
    // @rec - PushHub model
    unsubscribe: function ( rec, callback ) {

        var subscriptionData = rec.getData(),
            cb = callback || Ext.emptyFn;

        // Call the proxy method for unsubscribing
        this.proxy.unSubscribeToSignal( {
            dataRequestId  : subscriptionData.dataRequestId,
            filter         : subscriptionData.filter,
            subscriptionId : subscriptionData.subscriptionId,
            successCallback: this.unsubscribeSuccessCb(rec, cb)
        } );

    },

    // Unsubscribe success callback
    // @pushHubStore - reference for the pushHubStore
    // @rec - PushHub model to remove
    unsubscribeSuccessCb: function (rec, cb) {
        var that = this;
        
        cb = cb || Ext.emptyFn;
        return function (response) {

            // reference for specific subscription collection
            var subscriptionRec = that.getSubscriptionCollectionRec( rec );

            console.log( '===========' );
            console.log( 'unusbscribe to signal ( ' + rec.get( 'dataRequestId' ) + ', ' + rec.getId() + ' ) ' );
            console.log(response);
            console.log( '===========' );

            // Delete callback
            delete that.subscriptionKeyToCallback[rec.get('subscriptionId')];

            // notifies the views that listens on the event name @unSubscribePushMessage


            // Remove from store
            that.pushHubStore.remove( rec );


            if ( subscriptionRec ) {
                // remove from subscription collection
                that.subcriptionCollection.remove( subscriptionRec );    
            }

            that.fireEvent( 'unSubscribePushMessage', rec );
            console.log( '@fired eventname unSubscribPUshMessage from pushHubMgr');
            cb();
        };
    },

    getSubscriptionCollectionRec: function ( rec ) {

        var subscriptionKey;
        
        if (rec.get('subscriptionType') === 'MarketDepth') {
            subscriptionKey = parseInt( rec.get('dataRequestId') + '' + rec.get('storeIdOrComponentId'), 10 );
        } else {
            subscriptionKey = rec.get('dataRequestId');
        }

        return this.subcriptionCollection.get( subscriptionKey );
    },

    // Checks if a data request is subscribed
    // @returns - false if not subscribed
    // @returns - PushHub model if subscribed
    isDataRequestSubscribed: function (dataRequestId) {
        var record = this.pushHubStore.findRecord('dataRequestId', dataRequestId),
            isSubscribed = false;

        if (record) {
            if (record.get('subscribed') === true) {
                isSubscribed = record;
            }
        }

        return isSubscribed;
    },

    // unsubscribes all the datarequests
    doUnSubscribeDataRequests: function () {
        this.pushHubStore.each(function (record) {
            // check if current datarequest is subscribed
            if (record.get('subscribed') === true ) {
                // unsubscribe specific datarequest
                this.unsubscribe( record );

                console.log( '@unsubscribe', record );
                
            }

        }, this);
    },

    // Invoke the disconnect callbacks on each subscriptions
    disconnectSubscriptionCallback: function () {
        this.pushHubStore.each(function (record) {
            // Check if connectfed
            // Check if there's a disconnect callback
            if (record.get('subscribed') === true && this.subcriptionCollection.get(record.get('dataRequestId')).disconnectCallback) {
                // Invoke the disconnect callback
                this.subcriptionCollection.get(record.get('dataRequestId')).disconnectCallback();
            }

        }, this);
    },

    // EO - SUBSCRIPTIONS

    // STORE
    // Add new record to Push Hub store
    // @data - the record data
    // @return - PushHub model
    addToPushHubStore: function (data) {
        var pushHubStore = Ext.getStore( 'PushHubs' ),
            rec;

        rec = pushHubStore.add({
            dataRequestId:        data.dataRequestId,
            filter:               data.filter,
            extDataType:          data.extDataType,
            storeIdOrComponentId: data.storeIdOrComponentId,
            subscribed:           data.subscribed,
            subscriptionType:     data.subscriptionType
        });

        return rec[0];
    },

    // Returns the DataRequestId by SubscriptionId
    // @subscriptionId - the subscription id to lookup
    getDataRequestIdBySubsId: function (subscriptionId) {
        var rec = this.pushHubStore.findRecord('subscriptionId', subscriptionId);

        if (rec) {
            return rec.get('dataRequestId');
        }
    },

    // Gets the Push Hub record by subscriptionId
    // @subscriptionId - the subscription id to lookup
    getPushHubRecBySubsId: function (subscriptionId) {
        var rec = this.pushHubStore.findRecord('subscriptionId', subscriptionId);

        if (rec) {
            return rec;
        }
    },

    // EO - Store

    // PROXY Handlers
    // Setup SignalR proxy listeners
    setupProxyListeners: function () {
        console.log('setupProxyListeners');

        this.proxy.on( {
            scope:           this,
            connected:       this.onProxyConnected,
            disconnected:    this.onProxyDisconnected,
            messageReceived : this.onProxyMessageReceived,
            reconnected     : this.onProxyConnected,
            userInitialized: this.onProxyUserInitialized
        } );
    },

    // On proxy Connected listener
    onProxyConnected: function ( connectionId, state ) {
        var connCtrl = this.getController('Connection');
        
        // set Connection controller flag to connected
        connCtrl.setIsDisconnected( false );
        connCtrl.setIsResumedFromDisconnection( true );

        console.log('Connected/Reconnected to server with ConnectionId: ', connectionId );

        this.connectionStatus = 'connected';

        // notifies viewController that listens to this event name @onConnectedToPushMessage
        this.fireEvent( 'pushMessageConnected' );

        if ( state && state === 'connected' ) {
            // Run all subscriptions in collection
            this.runSubscriptionCollection();
        }
    },

    // On proxy Disconnected listener
    onProxyDisconnected: function ( connectionId ) {
        console.log('Disconnected from server with ConnectionId: ', connectionId );

        // Update the status
        this.connectionStatus = 'disconnected';

        // Call the disconnect callbacks on each subscription
        this.disconnectSubscriptionCallback();

        // A fallback handler for appSubscriber that triggers when disconnected from the server
        this.appPushDataUnSubscribeAll();

        this.doUnSubscribeDataRequests();

        // Remove all PushHub records
        this.pushHubStore.removeAll();

        // Restart SignalR connection
        this.startSignalRConnection();
    },

    // On proxy Message Received listener
    // @pushResData - the push response data received
    onProxyMessageReceived: function ( pushResData ) {

        var subscriptionId;

        pushResData = Ext.decode( pushResData );

        if ( Ext.isObject( pushResData ) && pushResData.SubscriptionId) {

            subscriptionId = pushResData.SubscriptionId;

            if (TFApps.config.pushHub.debug) {
                console.group( 'Push message received - ' + this.getDataRequestIdBySubsId(pushResData.SubscriptionId) + ' - ' + Ext.Date.format(new Date(), 'Y-m-d H:i:s') );
                console.log( pushResData );
                console.groupEnd();
            }

            // Check if there's a callback for this reponse
            if ( this.subscriptionKeyToCallback[subscriptionId] ) {

                // publish the 'messageReceived' - which triggers the events from the appSubscribers
                // if the name is equal to messageReceived
                this.publishAppPushDataSubscriber( 'messageReceived', pushResData );

                // Invoke callback for this response
                this.subscriptionKeyToCallback[subscriptionId]( pushResData );
            }

        } else {

            if (TFApps.config.pushHub.debug) {
                console.group( 'Invalid received push message' );
                console.log( pushResData );
                console.groupEnd();
            }
        }

    },

    // Registers' events from specific app
    appPushDataSubscribe: function ( name, config ) {

        var token = Math.random().toString(36).substring(7);
        var defaultConfig = {
            token: token
        };

        defaultConfig = Ext.Object.merge( defaultConfig, config );

        if ( !this.appSubscribers[name] ) {
            this.appSubscribers[name] = [];
        }

        // Push events in appSubscribers
        this.appSubscribers[name].push( defaultConfig );

        return token;
    },

    // publish events from appSubscribers
    // @unsubscribeAllBool - if true, removes all the app pushHub subscriber.
    publishAppPushDataSubscriber: function ( name, response ) {

        var subscriptionId = parseInt( response.SubscriptionId, 10 );
        var record         = Ext.getStore('PushHubs').findRecord('subscriptionId', subscriptionId);

        // var appSubscribers = this.appSubscribers[name],
        //     len = appSubscribers ? appSubscribers.length : 0;

        // if ( record ) {
        //     while ( len-- ) {
        //         appSubscribers[len].callback.call( appSubscribers[len].scope, response, record );
        //     }
        // }

        // VERSION 2
        // Notifies viewController that listens to pushMessageReceived
        this.fireEvent( 'pushMessageReceived', response, record );
    },

    // trigger a fallback handler when disconnected from signalR
    appPushDataUnSubscribeAll: function () {
        console.log( '@disconnected from signalR. Firing event pushMessageReceivedDisconnected' );

        // VERSION 2
        // notifies viewController that listens on this eventname @pushMessageReceivedDisconnected
        this.fireEvent( 'pushMessageDisconnected' );

    },

    // unSubscribe the event from appSubscriber
    appPushDataUnSubscribe: function ( token ) {

        var appSubscribers = this.appSubscribers;

        for (var m in appSubscribers) {
            if (appSubscribers[m]) {
                for (var i = 0, j = appSubscribers[m].length; i < j; i++) {
                    if ( token ) {
                        if (appSubscribers[m][i].token === token) {
                            appSubscribers[m].splice(i, 1);
                            return token;
                        }
                    } else {
                        if (appSubscribers[m][i].disconnectCallback) {
                            appSubscribers[m][i].disconnectCallback.call( appSubscribers[m][i].scope );
                        }
                    }

                }
            }
        }

        return false;
    },

    // On proxy User Initialized listener
    onProxyUserInitialized: function ( username ) {
        console.log( 'User initialized: ' + username );
    },

    // EO - PROXY HANDLERS

    // Application subscriptions handler
    // Default common push for all apps
    startCommonSubscriptions: function () {
        // Subscribe for notifications
        this.getController( 'Notifications' ).subscribe();
    },

    // Application specific subscriptions
    appSubscriptions: Ext.emptyFn
} );