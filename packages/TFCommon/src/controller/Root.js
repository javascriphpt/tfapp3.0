/**
 * The main application controller. This is a good place to handle things like routes.
 */
Ext.define('TFCommon.controller.Root', {
    extend: 'Ext.app.Controller'
});
