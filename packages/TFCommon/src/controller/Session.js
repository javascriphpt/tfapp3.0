/**
 * The main application controller. This is a good place to handle things like routes.
 */
'use strict';
Ext.define( 'TFCommon.controller.Session', {
    extend: 'Ext.app.Controller',
    id    : 'SessionController',

    requires : [
        'TFCommon.util.ActivityMonitor'
    ],

    refs: [
        {
            ref     : 'mainViewport',
            selector: 'app-mainViewport'
        }
    ],

    /**
     * listens to events emitted by controllers
     * @type {Object}
     */
    listen  : {
        controller : {
            '#MainController' : {
                checkusersession : 'checkUserSession'
            },

            '#LoginViewController' : {
                startusersession : 'startUserSession'
            },

            '#HeaderViewController' : {
                userlogout : 'onClickUserLogoutBtn'
            }
        }
    },

    init: function () {

        this.proxy = TFCommon.data.SignalR;

        // Holder for this user session class variables
        this.userVars = {};

    },

    // SESSION
    /**
     * @version 2.2, update for 3.0
     * @description handler to check the user session
     */
    checkUserSession: function () {

        var that = this;

        Ext.log( { indent : 1 }, 'Start check user session' );

        // waits if promise is resolved
        this.isUserInSession().then( function( inSession ) {

            // start session if user is logged-in
            // otherwise, show login panel
            if (inSession) {
                Ext.log( 'In Session' );
                // Start the session
                that.startUserSession.apply(that);
            } else {
                Ext.log( 'Not in Session' );
                // Show the login panel.
                // The Main.js controller should be listeneing to this event
                that.fireEvent( 'showloginpanel' );
            }
        });

        Ext.log( { outdent : 1 }, 'End check user session' );
    },

    /**
     * @version 2.2, update to 3.0
     * @description Starts the user session
     * @param  {Object} loginResponse the login response. Empty if the user is already logged in
     */
    startUserSession: function (loginResponse) {
        var loginApp  = 'TF.Login',
            that      = this,
            getLocale = TFCommon.Locales.getLocaleValue,
            userInfo;

        ActivityMonitor.init({ verbose : false });
        ActivityMonitor.start();

        // Set the in user in session value
        this.userVars.inSession = true;

        //Check if user is in Session
        //A. if Yes, load required data
        //a. Load user info
        //b. Load user menu (permission)
        //c. Check if user has app permission
        //If yes, load user app
        //Start SignalR
        //if yes, show no access message
        //B. if No, show login panel
        

        Ext.log( { indent : 1 }, 'Start User Session' );

        async.series({
                loadUserRec             : Ext.bind( that.loadUserRec, that ),
                loadUserMenu            : Ext.bind( that.loadUserMenu, that ),
                loadUserActions         : Ext.bind( that.loadUserActions, that ),
                loadUserPreferences     : Ext.bind( that.loadUserPreferences, that ),
                // After loading preferences. Set default preference for the current App
                startSettingPreferences : Ext.bind( that.startSettingPreferences, that ),
                checkUserPermission     : Ext.bind( that.checkUserPermission, that, [ loginApp, loginResponse ], true ),
                createAppContainer      : Ext.bind( that.createAppContainer, that, [ loginApp ], true )
            },
            function (err, msg) {
                var showMsgBox = false,
                    alertTitle,
                    alertMsg,
                    msgCb;

                Ext.log( { outdent : 1 }, 'End Start User Session' );

                if (err) {

                    Ext.Object.each(msg, function (key, val) {
                        if (val) {
                            showMsgBox = val.showMsgBox;
                            alertTitle = val.title;
                            alertMsg = val.msg;
                            msgCb = val.msgCb || null;
                            return false;
                        }
                    });

                    if (showMsgBox) {
                        Ext.Msg.alert(alertTitle, alertMsg, msgCb);
                    }

                } else {
                    // Create the main application view
                    if (TFAppConfig.AppName !== loginApp) {
                        // User.js controller should listen to this event.
                        that.fireEvent( 'createmainappview' ); //---to start here
                    }

                }
            }
        );
    },

    /**
     * Load User records
     * @version 3.0
     * @param  {Function} mainUserCb Main Callback from Async series
     * @return {[type]}            [description]
     */
    loadUserRec : function ( mainUserCb ) {
        var that = this;

        // A. LOAD USER IN SESSION
        Ext.log( { indent : 1 }, 'Load User Rec' );

        async.waterfall([
                Ext.bind( that.loadUserInfo, that ),
                Ext.bind( that.loadActualUserRecord, that )
            ],
            function ( err, msg ) {
                Ext.log( { outdent : 1 }, 'End Load User Rec' );

                if (err) {
                    mainUserCb(true, {
                        showMsgBox: true,
                        title     : getLocale( 'User.Error.UserErr.Title', 'User Error' ),
                        msg       : getLocale( 'User.Error.UserErr.Message', 'Error loading user record.' )
                    });
                } else {
                    mainUserCb(null);
                }
            }
        );

    },

    /**
     * Load user info
     * @version 3.0
     * @param  {Function} userCb1
     */
    loadUserInfo : function ( userCb1 ) {
        // A.1. Load the user info
        // 
        var that = this;

        Ext.log( 'Load user info' );
        this.fireEvent( 'getuserinsessioninfo', {
            success : Ext.bind( that.loadUserInfoSuccessCb, that, [ userCb1 ], true ),
            fail    : Ext.bind( that.loadUserInfoFailCb, that, [ userCb1 ], true )
        } );
    },

    /**
     * Success callback for load user info
     * @version 3.0
     * @param  {Object} userData User Data
     * @param  {Object} userCb1  Async waterfall callback
     */
    loadUserInfoSuccessCb : function ( userData, userCb1 ) {
        // Success
        Ext.log( 'Success load user info' );

        // User.js controller should listen to this event
        this.fireEvent( 'setgetuserstore', userData );
        userCb1(null, userData);
    },

    /**
     * Failure callback for load user info
     * @version 3.0
     * @param  {Object} userData User Data
     * @param  {Object} userCb1  Async waterfall callback
     */
    loadUserInfoFailCb : function( userData, userCb1 ) {
        // Fail
        Ext.log( 'Failed load user info' );
        userCb1( true );
    },

    /**
     * load actual user record
     * @param  {Object} userData User Data
     * @param  {Function} userCb2  Async waterfall callback
     */
    loadActualUserRecord : function ( userData, userCb2 ) {
        // A.2. Then load the actual user record
        Ext.log( 'Load actual user record' );
        // User.js controller should listen to this event
        this.fireEvent( 'loadstoreforuserinsession', {
            userData : userData,
            storeCb  : function (records, operation, success) {
                if ( success ) {
                    userCb2( null );
                } else {
                    userCb2( true );
                }
            }
        });
    },

    /**
     * Load User Menu
     * @version 3.0
     * @param  {Function} cb Async callback
     */
    loadUserMenu : function (cb) {
        Ext.log( 'Load user menu' );

        // User.js controller should listen to this event
        this.fireEvent( 'getusermenu', cb );
    },

    /**
     * load user actions
     * @version 3.0
     * @param  {Function} cb Async callback
     */
    loadUserActions : function (cb) {
        Ext.log( 'Load user actions' );

        // User.js should listen to this event
        this.fireEvent( 'getuseractions', cb );
    },

    /**
     * Load user preferences
     * @version 3.0
     * @param  {Function} cb Async callback
     */
    loadUserPreferences: function (cb) {
        Ext.log( 'Load user preferences' );

        // Preference.js controller should listen to this event
        this.fireEvent( 'setupstorelistener' );
        this.fireEvent( 'loadstore', cb );
    },

    /**
     * start setting preferences
     * @param  {Function} cb Async callback
     */
    startSettingPreferences: function ( cb ) {

        Ext.log( 'start setting preferences' );
        // Preference.js controller should listen to this event
        this.fireEvent( 'setdefaultapppreference', cb );
    },

    /**
     * Check user permission
     * @version 3.0
     * @param  {Function} cb Async callback
     */
    checkUserPermission: function ( cb, loginApp, loginResponse ) {

        Ext.log( 'check user permission' );
        var userInfo = Ext.getStore( 'User' ).first(),
            appId    = userInfo.get( 'AppId' ),
            userApp  = {
                AppName: userInfo.get( 'AppName' ),
                AppId  : appId,
                Url    : userInfo.get( 'Url' )
            },
            that = this;

        // If we're on the login app then redirect to user's default application
        if (TFAppConfig.AppName === loginApp) {
            // Check if user has a default app
            if ((loginResponse && loginResponse.user.AppId !== '0') || appId ) {
                // Redirect to user default app
                // User.js controller should listen to this event
                this.fireEvent( 'redirecttouserdefaultapp', userApp );
            } else {
                // Inform user and show the user's applications list
                Ext.Msg.alert(
                    getLocale( 'Login.Alert.Error', 'Login.Alert.Error'),
                    getLocale( 'Login.Alert.ContactAdministrator', 'Sorry, you don\'t have a default application. Please contact your account administrator.'),
                    function () {
                        // Permission.js controller should listen to this event
                        that.fireEvent( 'showappselectorwindow' );
                    }
                );
            }

            cb(null);

        } else {
            // Check user app permission
            // Permission.js controller should listen to this event
            that.fireEvent( 'userhasapppermission', {
                userApp : userApp,
                cb : function (err, msg) {
                    if (err) {
                        cb(true, msg);
                    } else {
                        cb(null);
                    }
                }
            } );
        }
    },

    /**
     * Create App Container
     * @version 3.0
     * @param  {Function} cb
     */
    createAppContainer: function ( cb, loginApp ) {
        
        Ext.log( 'Create app container' );
        // Don't create the main panel if it's the login app
        if (TFAppConfig.AppName !== loginApp) {
            // Main.js controller should listen to this event.
            this.fireEvent( 'createmainapppanel' );
        }

        cb(null);
    },

    onClickUserLogoutBtn: function () {

        var mainViewport = this.getMainViewport();
        var getLocale = TFCommon.Locales.getLocaleValue;
        var that = this;

        mainViewport.setLoading( {
            msg: getLocale( 'Common.Message.LogOut', 'Logging out...' )
        } );

        if (TFAppConfig.features && TFAppConfig.features.SignalR === true) {

            if (this.proxy.getPushHubProxy() && this.proxy.isConnectedToPushHub === true) {
                // unsubscribes all the datarequest
                this.proxy.unSubscribeAllToSignal( {
                    successCallback: function () {
                        // ELI - Moved logout to bottom condition
                    }
                } );

                that.logoutRequest();
                
            } else {
                this.logoutRequest();
            }
        } else {
            this.logoutRequest();
        }
    },

    logoutRequest: function () {

        var that         = this;
        var mainViewport = this.getMainViewport();
        var loginApp     = '../TFLogin'+ window.location.search;

        Ext.Ajax.request( {
            url    : TFApps.config.dataRequest.url + '/Account/Logoff',
            method : 'POST',
            success: function () {

                mainViewport.setLoading( false );

                // stop push hub connection
                that.stopPushHubConnection();
                // Remove any common data
                that.removeCommonData();

                // Redirect to the login app
                window.location = loginApp;

            },
            failure: function () {
                mainViewport.setLoading( false );
                // Redirect to the login app
                window.location = loginApp;
            }
        } );
    },

    getLoggedInUser: function () {
        var record = Ext.getStore( 'User' ).first();

        return (record) ? record : null;
    },

    // Check if user is logged in
    // @return - promise
    isUserInSession: function () {

        return new Ext.Promise( function( resolve, reject ) {

            Ext.Ajax.request({
                url    : TFApps.config.dataRequest.url + '/Account/ValidateSession',
                method : 'POST',
                success: function (response) {

                    var results = Ext.decode(response.responseText, true);

                    if (results && results.success === 'true') {
                        resolve( true );
                    } else {
                        resolve( false );
                    }

                },

                failure: function () {
                    reject();
                }
            });
        });

    },

    // EO - SESSION

    // Remove common data used
    removeCommonData: function () {
        Ext.getStore( 'User' ).removeAll();
        Ext.getStore( 'UserMenu' ).removeAll();
        Ext.getStore( 'UserActions' ).removeAll();
        Ext.getStore( 'preferencetype' ).removeAll();
    },

    // Stops the Push data
    stopPushHubConnection: function () {

        // PushHubMgr.js should listen to this event
        this.fireEvent( 'stopsignalrconnection' );
        /*var pushHubController = this.getController( 'PushHubMgr' );

        if ( pushHubController ) {
            // stop the connection from the server when logout
            pushHubController.stopSignalRConnection();
        }
*/
    }
} );
