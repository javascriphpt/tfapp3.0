'use strict';
Ext.define( 'TFCommon.controller.User', {
    extend: 'Ext.app.Controller',

    refs  : [{
        ref     : 'appContainer',
        selector: '#app-subContainer'
    }],

    UserViewsCb      : [],
    UserCommonViewsCb: [],

    listen : {
        controller : {
            '#SessionController' : {
                getuserinsessioninfo      : 'getUserInSessionInfo',
                setgetuserstore           : 'setGetUserStore',
                loadstoreforuserinsession : 'loadStoreForUserInSession',
                getusermenu               : 'getUserMenu',
                getuseractions            : 'getUserActions',
                redirecttouserdefaultapp  : 'redirectToUserDefaultApp',
                createmainappview         : 'createMainAppView'
            }
        }
    },

    init: function () {
        // get the locale value
        this.getLocale = TFCommon.Locales.getLocaleValue;
        // Checks for push permission
        this.userHasPushPermission = false;
        // sort sequence for update password
        this.sortSeq = 1;
    },

    /**
     * Get User In Session Info
     * @version 2.2, update in 3.0
     * @param  {Object} cbConfig    {
     *                                  success : Function,
     *                                  fail : Function
     *                              }
     */
    getUserInSessionInfo: function ( cbConfig ) {

        Ext.log( 'Get user in session info' );
        Ext.Ajax.request({
            url   : TFApps.config.dataRequest.url + '/TFData/GetUser',
            method: 'POST',

            success: function (response) {
                var results = Ext.decode(response.responseText);

                if (results) {
                    cbConfig.success(results.data);
                } else {
                    cbConfig.fail(response);
                }
            },

            failure: function (response) {
                cbConfig.fail(response);
            },

            scope: this
        });
    },

    /**
     * store the information of the current user
     * @version 2.2
     * @param {Object} data User data
     */
    setGetUserStore: function ( data ) {

        var store = Ext.getStore('GetUser');

        if ( !Ext.Object.isEmpty( data ) ) {
            store.add( data );
        }
    },

    /**
     * Load store for user in session
     * @version 2.2, upgrade in 3.0
     * @param  {Object}   config    {
     *                                  userData : Object,
     *                                  storeCb  : Function
     *                              }
     */
    loadStoreForUserInSession: function ( config ) {

        Ext.log( 'Load store for user in session' );
        var store = Ext.getStore('User'), // to filter specific user: format can't be change
            filter = [
                {
                    SetOp    : 'AND',
                    FilterSet: [
                        {
                            Field    : 'UserId',
                            Operator : 'EQ',
                            Value    : [ config.userData.UserId ],
                            ElementOp: 'AND'
                        }
                    ]
                }
            ];

        store.load({
            params  : {
                filter: Ext.encode(filter)
            },
            callback: config.storeCb
        });
    },

    /**
     * Get User menu
     * @version 2.2
     * @param  {Function} callback Async callback
     */
    getUserMenu: function (callback) {

        var userMenuStore = Ext.getStore('UserMenu');
        var that = this;

        Ext.log( 'Get user menu' );

        // Load User Menu
        userMenuStore.load(function (recs, op, success) {

            if (success) {
                callback(null);
            } else {
                callback(true, {
                    showMsgBox: true,
                    title     : that.getLocale( 'User.Error.UserMenu.Title', 'User Menu Error' ),
                    msg       : that.getLocale( 'User.Error.UserMenu.Message', 'Failed to load user menu' )
                });
            }

        });
    },

    /**
     * Get user actions
     * @version 2.2
     * @param  {Function} callback
     */
    getUserActions: function (callback) {
        var userActionsStore = Ext.getStore('UserActions');
        var that = this;

        Ext.log( 'get user actions' );
        // Load user action
        userActionsStore.load(function (recs, op, success) {
            if (success) {
                callback(null);
            } else {
                callback(true, {
                    showMsgBox: true,
                    title     : that.getLocale( 'User.Error.UserPermission.Title', 'User Permissions Error ' ),
                    msg       : that.getLocale( 'User.Error.UserPermission.Message', 'Failed to load user permission' )
                });
            }
        });
    },

    /**
     * Redirect user to default application
     * @version 2.2
     * @param  {Object} userApp { AppName, AppId, Url}
     */
    redirectToUserDefaultApp: function ( userApp ) {

        Ext.log( 'Redirect to user default app' );

        if ( userApp.Url ) {
            window.location = '.' + userApp.Url + window.location.search;
        } else {
            return false;
        }

    },

    /**
     * Create app main view
     * @version 2.2
     */
    createMainAppView     : function () {

        var getLocale = TFCommon.Locales.getLocaleValue;
        var that = this;
        // Add a loading message
        this.getAppContainer().setLoading( {
            msg: getLocale( 'Common.Message.Loading', 'Loading...' )
        } );

        // Load app specific data first
        // or other app requirements
        async.series( [
            function ( callback ) {
                // for required app specific data
                that.loadAppRequiredData( callback );
            }, function ( callback ) {
                // For common SignalR push data
                that.startCommonPushDataSubscription(callback);
            }
        ], function ( ) { // @params:  ( err, res )
            // Create the allowed user views
            that.invokeUserViewsCb( that.UserViewsCb );
            // Remove the loading message
            that.getAppContainer().setLoading( false );

        } );


    },

    // User views
    createUserViews: function () {
       this.invokeUserCommonViewsCb( this.UserCommonViewsCb );
    },

    // Create the allowed user views
    // @cbList - list of views to create. This is per application
    invokeUserViewsCb: function ( cbList ) {
        Ext.Array.each( cbList, function ( cb, idx ) {
            // Invoke if it's a function
            if ( Ext.isFunction( cbList[idx] ) ) {
                cb();
            }

            // Iterate if it's a list
            if ( Ext.isArray( cbList[idx] ) ) {
                this.invokeUserViewsCb( cbList[idx] );
            }
        }, this );
    },

	 loadUserPreferences : function( cb, itemId ) {

        this.getUserPreferencesController().loadStore( cb, itemId );
    },



    getUserPreferencesController : function(){
        return TFAppConfig.getApp().getApplication().getController( 'Preferences' );
    },

    setupStoreListener : function() {
        this.getUserPreferencesController().setupStoreListener();
    },





    // Load any app required data
    // @cb - the async callback
    loadAppRequiredData: function ( cb ) {
        cb( null );
    },

    // Starts common SignalR push data
    // @cb - callback from async
    startCommonPushDataSubscription: function (cb) {
        // SignalR feature
        if ( TFAppConfig.features && TFAppConfig.features.SignalR ) {
            // Starts common SignalR subscriptions
            TFAppConfig.getApp().getApplication().getController( 'PushHubMgr' ).startCommonSubscriptions();
        }

        cb();

    },

    // Gets the User record
    // @return - User record data model
    getUserRecord: function () {
        return Ext.getStore( 'User' ).first();
    },

    // Finds user action
    // @actionId - The action id to find
    // @returns - UserActions record data model
    findUserAction: function ( actionId ) {
        var rec = Ext.getStore( 'UserActions' ).findRecord( 'ActionId', actionId );

        return rec;
    },

    // Checks if user has an action
    // @actionId - The action id to check
    // @return - (boolean) true if user has action
    userHasAction : function ( actionId ) {
        var rec = Ext.getStore( 'UserActions' ).findRecord( 'ActionId', actionId );

        return (rec) ? true : false;
    },

    // Finds a user menu
    // @menu - the menu to find
    // @return - null if no record found. UserMenu record if found
    findUserMenu  : function ( menu ) {
        var menuType = TFAppConfig.roles.MenuType;
        var rec      = Ext.getStore( 'UserMenu' ).findRecord( 'MenuItemCode', menu );

        if ( rec && rec.get( 'MenuType' ) === menuType ) {
            return rec;
        } else {
            return null;
        }
    },

    // Checks if user has a menu
    // @menu - the menu to check
    // @return - (boolean) true if user has the menu
    userHasMenu   : function ( menu ) {
        var menuType = TFAppConfig.roles.MenuType;
        var rec = Ext.getStore( 'UserMenu' ).findRecord( 'MenuItemCode', menu );

        if ( rec && rec.get( 'MenuType' ) === menuType ) {
            return true;
        } else {
            return false;
        }
    },

    applyGridChanges: function ( grid, alternateXType, callback ) {

        var store           = Ext.create( 'TFCommon.store.UserPreferences' );
        var gridDefaultCols = grid.headerCt.getGridColumns();
        var cmp             = alternateXType || grid.getXType();
        var filter;

        var getLocaleId = TFCommon.Locales.getLocaleId;

        if ( !!TFAppConfig.preferences.appgridconfig && TFAppConfig.preferences.appgridconfig.length > 0 ) {
            store.on( 'beforeload', function ( str, op ) {
                filter = [{
                    "SetOp": "AND",
                    "FilterSet": [{
                        "Field"    : "ItemId",
                        "Operator" : "LIKE",
                        "Value"    : [cmp],
                        "ElementOp": "AND"
                    }, {
                        "Field"    : "PreferenceTypeId",
                        "Operator" : "EQ",
                        "Value"    : [1],
                        "ElementOp": "AND"
                    }, {
                        "Field"    : "LocaleId",
                        "Operator" : "EQ",
                        "Value"    : [getLocaleId()],
                        "ElementOp": "AND"
                    }]
                }];

                op.setParams( {
                    filter: Ext.encode( filter )
                } );
            } );

            store.on( 'load', function ( obj, records ) {
                Ext.each( records, function ( record ) {
                    var cols = Ext.JSON.decode( record.data.ItemValue );
                    var colIndex = 0;

                    for ( colIndex in cols ) {
                        var col        = cols[colIndex];
                        var defaultCol = gridDefaultCols[colIndex];

                        if ( typeof defaultCol !== 'undefined' ) {

                            if ( grid.columns[colIndex].hideable === false && grid.columns[colIndex].resizable === false ) {
                                continue;
                            }

                            if( grid.columns[colIndex].flex ) {
                                delete grid.columns[colIndex].flex;
                            }

                            grid.columns[colIndex].setWidth( col.width );

                            if ( col.hidden === true ) {
                                grid.columns[colIndex].setVisible( false );
                            } else {
                                grid.columns[colIndex].setVisible( true );
                            }
                        }
                    }
                } );

                store.destroy();

                if( callback ) {
                    callback();
                }
            } );

            store.load();
        }
    },

    formatGridChanges: function ( component, hdrContainer ) {

        var gridColumnIndex;
        var gridColumns = hdrContainer.getGridColumns();
        var itemValue   = [];


        for ( gridColumnIndex in gridColumns ) {
            var col = gridColumns[gridColumnIndex];

            var colConfig = {
                'dataIndex': col.dataIndex,
                'width'    : col.width,
                'hidden'   : col.hidden
            };

            itemValue.push( colConfig );
        }

        if ( !!TFAppConfig.preferences.appgridconfig && TFAppConfig.preferences.appgridconfig.length > 0 ) {
            this.saveUserPreferences( 1, component, itemValue );
        }

    },

    // deprecated
    _saveUserPreferences: function ( preferencetypeId, itemId, itemValue, notify, cb ) {

        var sortSeq      = TFAppConfig.getApp().getApplication().getController('User').sortSeq;
        var notif        = notify || false;
        var callback     = cb || null;
        var that         = this;
        var userId       = null;
        var data         = {};
        var getLocaleId  = TFCommon.Locales.getLocaleId;
        var getUserStore = Ext.getStore( 'GetUser' );


        if ( getUserStore.getCount() ) {

            userId = getUserStore.first().get('UserId');

            data = {
                'UserId'            : userId,
                'UserPreferencesSeq': 1,
                'SortSeq'           : sortSeq,
                'AppId'             : TFAppConfig.AppId,
                'ItemId'            : itemId,
                'PreferenceTypeId'  : preferencetypeId,
                'LocaleId'          : getLocaleId(),
                'ItemValue'         : JSON.stringify( itemValue )
            };

            //send request to server to save user preferences
            Ext.Ajax.request( {
                url    : TFApps.config.dataRequest.url + '/TFData/Submit',
                method : 'POST',
                params : {
                    id    : 694,
                    action: 'Update',
                    record: Ext.JSON.encode( data )
                },
                success: function ( response ) {
                    var results = Ext.decode( response.responseText );

                    if ( notif ) {
                        if ( callback !== null ) {
                            callback( that, results );
                        } else {
                            that.callback( that, results, itemId );
                        }
                    }  else {
                        that.loadUserPreferences( function() {}, itemId);
                    }
                },
                failure: function ( response ) {
                    var results = Ext.decode( response.responseText );
                    Ext.Msg.alert( 'Config', results.errorMessage );
                }
            } );
        }

    },

    saveUserPreferences: function ( preferencetypeId, itemId, itemValue, notify, cb ) {
        this.getUserPreferencesController().saveUserPreferences( preferencetypeId, itemId, itemValue, notify, cb );
    },

    callback: function ( ctx, response, itemId ) {
        var getLocale = TFCommon.Locales.getLocaleValue;

        Ext.Msg.alert( getLocale( 'CltUserEditView.UserPreferences.Title', 'Preferences' ), getLocale( response.messagekey, response.message ) );

        ctx.loadUserPreferences( function () {
        }, itemId );
    }
} );