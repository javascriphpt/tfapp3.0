﻿//'use strict';
Ext.define( 'TFCommon.data.SignalR', {
    extend: 'Ext.util.Observable',

    singleton: true,

    constructor: function () {
        // Connection state from the server
        this.isConnectedToPushHub = false;

        // Reference to hub
        this.hubConnection = false;

        // Reference to pushHub proxy
        this.pushHubProxy = false;

        // Set a global func for getting resource locale
        // this.getLocale = TFCommon.Locales.getLocaleValue;

        this.callParent();
    },

    connect: function () {

        if ( !this.hubConnection ) {
            this.hubConnection = $.hubConnection( TFApps.config.pushHub.hubConnectionUrl, { useDefaultPath: false });
        }

        // $.connection.pushHub;
        this.pushHubProxy = this.createPushHubProxy( 'pushHub' );

        // Setup a listener for receiving notifications
        this.setupPushHubListener();

        this.startConnection();
    },

    getHubConnection: function () {
        return this.hubConnection;
    },

    createPushHubProxy: function () {
        var connection = this.getHubConnection(), pushHubProxy;

        if ( connection ) {
            pushHubProxy = connection.createHubProxy( 'pushHub' );
        }

        return pushHubProxy;
    },

    getPushHubProxy: function () {
        return this.pushHubProxy;
    },

    // start the connection to server
    startConnection: function () {
        var that = this;


        this.getHubConnection().starting( function () {
            that.disconnected();
        } );

        this.getHubConnection().start().done( function () {

            that.fireEvent( 'connected', that.getPushHubProxy().connection.id, 'connected' );

            // set connection state
            that.isConnectedToPushHub = true;

        } ).fail( function () {
            Ext.MessageBox.show( {
                title: 'Error',
                msg  : TFCommon.Locales.getLocaleValue( 'SignalR.Connection.FailedToConnect', 'Could not connect to the server!' ),
                icon : Ext.MessageBox.ERROR
            } );
        } );

        this.getHubConnection().reconnected( function() {
            that.fireEvent('reconnected', that.getPushHubProxy().connection.id, 'reconnected' );
            that.isConnectedToPushHub = true;
        });
    },

    // stop the connection to server
    stopConnection : function () {

        // Change the state if disconnected to server
        this.isConnectedToPushHub = false;

        if ( this.getHubConnection() ) {
            this.getHubConnection().stop();
        }

    },

    removePushHubListener: function () {
        // var that = this;

        // var listeners = [
        //     { serverCbName: 'setUser',  clientCbName: 'userInitialized' },
        //     { serverCbName: 'pushData', clientCbName: 'messageReceived' }
        // ];

        // Ext.each( listeners, function ( item ) {
        //     that.getPushHubProxy().un( item.serverCbName, function ( value ) {
        //         // user initialize and message received
        //         that.fireEvent( item.clientCbName, value );
        //     } );
        // } );

        console.log('@Removing pushHub listener');

        this.getPushHubProxy().off( 'setUser', this.pushHubCbListener( 'userInitialized' ) );

        this.getPushHubProxy().off( 'pushData', this.pushHubCbListener( 'messageReceived' ) );
    },

    setupPushHubListener: function () {
        // var that = this;

        // var listeners = [
        //     { serverCbName: 'setUser',  clientCbName: 'userInitialized' },
        //     { serverCbName: 'pushData', clientCbName: 'messageReceived' }
        // ];

        // Ext.each( listeners, function ( item ) {
        //     that.getPushHubProxy().on( item.serverCbName, function ( value ) {

        //         that.fireEvent( item.clientCbName, value );
        //     } );
        // } );

        // Ext.each( listeners, function ( item ) {
        //     that.getPushHubProxy().on( item.serverCbName, that.pushHuhCbListener( item.clientCbName ) );
        // } );

        console.log('@attaching pushHub listener');

        this.getPushHubProxy().on( 'setUser', this.pushHubCbListener( 'userInitialized' ) );

        this.getPushHubProxy().on( 'pushData', this.pushHubCbListener( 'messageReceived' ) );


    },

    pushHubCbListener: function ( clientCbName ) {

        var that = this;

        // user initialize and message received
        return function ( value ) {
            that.fireEvent( clientCbName, value );
        };
    },

    // Checks if disconnected to server
    disconnected        : function () {
        var that = this;
        var connectionId;

        // $.connection.hub.disconnected( function () {
        this.getHubConnection().disconnected( function () {

            connectionId = that.getPushHubProxy().connection.id;

            // Removing the previous attached listener on hubProxy
            that.removePushHubListener();

            // set push hub reference to null
            that.hubConnection = false;
            that.pushHubProxy  = false;

            that.fireEvent( 'disconnected', connectionId );

            // ELI - remove auto connect
            // if ( that.isConnectedToPushHub === true ) {

            //     // Reconnect to the server if disconnected from the server after 5 secs
            //     taskConnection = new Ext.util.DelayedTask( function () {
            //         that.connect();

            //         // that.fireEvent('reconnected', '');
            //     } );

            //     taskConnection.delay( 5000 );
            // }

        } );
    },

    subscribeToSignal: function ( data ) {

        var subscribe;

        try {
            subscribe = this.getPushHubProxy().invoke( 'subscribeToSignal', data.dataRequestId, data.subscriptionId, data.filter );

            subscribe.done( function ( response ) {
                if ( !response.StatusOk ) {
                    if ( data.errorCallback ) {
                        data.errorCallback( response );
                    }
                } else {
                    if ( data.successCallback ) {
                        data.successCallback( response );
                    }
                }
            } );
        } catch ( e ) {
            console.log( 'subscribe to signal: ', e );
        }
    },

    unSubscribeToSignal: function ( data ) {

        var unSubscribe;

        try {
            unSubscribe = this.getPushHubProxy().invoke( 'unSubscribeToSignal', data.dataRequestId, data.subscriptionId, data.filter );

            unSubscribe.done( function ( response ) {
                if ( data.successCallback ) {
                    data.successCallback( response );
                }
            } );
        } catch ( e ) {
            console.log( 'unsubscribe to signal: ', e );
        }
    },

    unSubscribeAllToSignal: function ( data ) {

        var unSubscribe;

        try {
            unSubscribe = this.getPushHubProxy().invoke( 'unSubscribeAllToSignal' );

            unSubscribe.done( function ( response ) {
                if (response.StatusOk === true) {
                    if ( data.successCallback ) {
                        data.successCallback( response );
                    }
                } else {
                    console.log( 'UnsubscribeAllToSignal( Error-Reason ): ', response.Reason );
                }

            } );
        } catch ( e ) {
            console.log( 'unsubscribe all to signal: ', e );
        }
    }
} );