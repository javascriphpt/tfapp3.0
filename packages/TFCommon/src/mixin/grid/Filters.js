Ext.define( 'TFCommon.mixin.grid.Filters', {
    extend: 'Ext.Mixin',

    mixinConfig: {

    },

    buildGridFilters: function ( filters, multiSet, encode ) {

        var formattedFilters;
        var multiFilterSet = [];
        var singleFilterSet = [
            {
                "SetOp"    : "AND",
                "FilterSet": []
            }
        ];

        // Iterate through all the filters
        if ( Ext.isArray( filters ) ) {

            Ext.each( filters, function ( filter ) {

                if ( Ext.isArray( filter ) || Ext.isObject( filter ) ) {
                    // Multiset set filter
                    // Filter is an array
                    if ( multiSet ) {

                        if ( Ext.isArray( filter ) ) {
                            Ext.each( filter, function ( item ) {
                                multiFilterSet.push( item );
                            } );
                        } else {
                            multiFilterSet.push( filter );
                        }
                    } else {

                        // Single set
                        // Filters are on a single filter object set
                        if ( Ext.isArray( filter ) ) {
                            Ext.each( filter, function ( item ) {
                                singleFilterSet[0].FilterSet.push( item );
                            } );
                        }
                    }
                }

            } );

        } else {
            singleFilterSet[0].FilterSet.push( filters );
        }

        if ( multiSet ) {
            formattedFilters = multiFilterSet;
        } else {
            formattedFilters = singleFilterSet;
        }

        if ( encode ) {
            return (formattedFilters.length) ? Ext.encode( formattedFilters ) : '';
        } else {
            return filters;
        }

    },

    buildColumnFilters: function ( op ) {

        var filterSet = [];

        var columnFilters = op._filters;

        // Apply column filters
        if ( columnFilters ) {
            this.buildColumnFilterType( columnFilters, filterSet );
        }

        return (filterSet.length) ? filterSet : null;
    },

    buildColumnFilterType: function ( columnFilters, filterSet ) {

        var value, type, op, col, i, f, dfmt, len = columnFilters.length;
        var cm = this.view.columnManager || this.gridCm, property;

        this.filteredColumn = this.filteredColumn || [];

        for ( i = 0; i < len; i += 1 ) {

            f = columnFilters[i];
            col = cm.getHeaderByDataIndex( f._property );

            value = f._value;
            type = col.filter.type;
            op = f._operator.toUpperCase();

            switch ( type ) {
                case 'decimal':
                    value = f._value;
                    break;
                case 'numeric':
                    value = f._value;
                    break;
                case 'string':
                    value = Ext.String.format( '%{0}%', f._value );
                    break;
                case 'list':
                    op = 'IN';
                    //value = Ext.String.format( '({0})', f._value );
                    value = f._value;
                    break;
                case 'boolean':
                    op = 'IS';
                    value = f._value;
                    break;
                case 'date':
                    dfmt = (col.filter.dateFormat) ? col.filter.dateFormat : Ext.Date.defaultFormat;
                    value = Ext.Date.format( new Date( f._value ), dfmt );
                    break;
            }

            if( this.colsToSetSortParamsName || this.colsToSetSortParamsCode ) {
                if( TFCommon.Locales.showByName() ) {
                    property = this.colsToSetSortParamsName[ f._property ];
                } else {
                    property = this.colsToSetSortParamsCode[ f._property ];
                }
            }

            console.log('property', property);

            // push column filter for reference
            this.filteredColumn.push( f._property );

            filterSet.push( {
                Field    : ( property ) ? property : f._property,
                Operator : op,
                Value    : (op === 'IN') ? value : [value],
                ElementOp: 'AND'
            } );
        }

    }

} );
