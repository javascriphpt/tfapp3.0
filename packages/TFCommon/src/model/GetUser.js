/**
 * Created on 12/12/2014.
 */
Ext.define( 'TFCommon.model.GetUser', {
    extend: 'Ext.data.Model',
    fields: [
        'UserId',
        'UserCode',
        'UserName',
        'UserRoleCode',
        'LglEntityCode',
        'LglEntityName',
        'LglEntityKana',
        'AppId',
        'AppCode',
        'AppName',
        'Url',
        'UrlMobile',
        'UserRoleHome',
        'IsMobileDevice'
    ]
} );