'use strict';

Ext.define('TFCommon.model.Notification', {
        extend: 'Ext.data.Model',

        fields : [
                'AlertId',
                'AlertTypeId',
                'NodeId',
                'AdditionalText',
                'AlertTime',
                'ProcessName',
                'ProcessCode',
                'ProcessId',
                'RefId',
                'ResourceId',
                'ResourceKey',
                'Pri'
        ]
});