'use strict';
Ext.define( 'TFCommon.model.PreferenceType', {
    extend    : 'Ext.data.Model',
    fields: [
        'id',
        'name',
        'apptype'
    ]
} );
