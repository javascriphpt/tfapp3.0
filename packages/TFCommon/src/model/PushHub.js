'use strict';
Ext.define( 'TFCommon.model.PushHub', {
    extend    : 'Ext.data.Model',
    idProperty: 'subscriptionId',
    identifier: {
        type: 'sequential',
        seed: 1000
    },
    fields    : [
        {
            type: 'int',
            name: 'subscriptionId'
        },
        {
            type: 'int',
            name: 'dataRequestId'
        },
        {
            type: 'string',
            name: 'filter'
        },
        {
            type: 'string',
            name: 'extDataType'
        },
        {
            type: 'bool',
            name: 'subscribed'
        },
        'storeIdOrComponentId',
        'subscriptionType'
    ]
} );
