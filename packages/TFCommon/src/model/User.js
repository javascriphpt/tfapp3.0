﻿Ext.define( 'TFCommon.model.User', {
    extend    : 'Ext.data.Model',
    idProperty: 'UserId',
    fields    : [
        'ParentLglEntityId',
        'LglEntityId',
        'UserId',
        'UserCode',
        'UserName',
        'UserRoleId',
        'UserRoleCode',
        'UserRoleName',
        'SortSeq',
        'Email',
        'LocaleId',
        'LocaleCode',
        'TimezoneId',
        'TimezoneName',
        'UserSeq',
        'Password',
        'Password1',
        'LglEntityCode',
        'LglEntityName',
        'LglEntityKana'
    ]
} );