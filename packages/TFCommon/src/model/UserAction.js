'use strict';
Ext.define( 'TFCommon.model.UserAction', {
    extend    : 'Ext.data.Model',
    fields    : [
        'UserActionType',
        'ActionId'
    ],
    idProperty: 'MenuItemID'
} );