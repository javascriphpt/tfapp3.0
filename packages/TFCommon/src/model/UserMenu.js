﻿'use strict';
Ext.define( 'TFCommon.model.UserMenu', {
    extend    : 'Ext.data.Model',
    fields    : [
        'UserRoleId',
        'UserRoleCode',
        'MenuItemCode',
        'MenuItemName',
        'URL',
        'SortSeq',
        'MenuItemID',
        'FunctionType'
    ],
    idProperty: 'MenuItemID'
} );