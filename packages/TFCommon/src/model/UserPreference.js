'use strict';
Ext.define( 'TFCommon.model.UserPreference', {
    extend    : 'Ext.data.Model',
    fields: [
        {
            name: 'AppId',
            type: 'int'
        },
        'ItemId',
        'ItemValue',
        'LocaleCode',
        {
            name: 'LocaleId',
            type: 'int'
        },
        {
            name: 'PreferenceTypeId',
            type: 'int'
        },
        {
            name: 'SortSeq',
            type: 'int'
        },
        {
            name: 'UserId',
            type: 'int'
        },
        {
            name: 'UserPreferencesSeq',
            type: 'int'
        }
    ]
} );