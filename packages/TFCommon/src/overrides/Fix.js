// fix hide submenu (in chrome 43)


Ext.define('TFCommon.overrides.Fix', {
    extend: 'Ext.app.Controller',

    init: function() {
        // fix for menu ( chrome version >= 43 )
        if (Ext.isChrome && Ext.chromeVersion >= 43) {
            this.overrideMenu();
        }

        this.overrideBoundListKeyNav();
        this.overrideElement();
        // this.overrideHtmlEditor();
        this.overrideDataStore();
        this.overrideGridTriFilter();

    },

    overrideMenu: function () {
        Ext.override(Ext.menu.Menu, {
            onMouseLeave: function(e) {
                var me = this;
                var visibleSubmenu;


                // BEGIN FIX
                visibleSubmenu = false;
                me.items.each(function(item) {
                    if (item.menu && item.menu.isVisible()) {
                        visibleSubmenu = true;
                    }
                });

                if (visibleSubmenu) {
                    //console.log('apply fix hide submenu');
                    return;
                }
                // END FIX


                me.deactivateActiveItem();


                if (me.disabled) {
                    return;
                }


                me.fireEvent('mouseleave', me, e);
            }
        });
    },

    overrideBoundListKeyNav : function() {
        Ext.override(Ext.view.BoundListKeyNav, {
            highlightAt:function (index) {
            this.setPosition( index );
        },

        setPosition: function(recordIndex, keyEvent, suppressEvent, fromSelectionModel) {
            var me = this,
                view = me.boundList,
                selModel = view.getSelectionModel(),
                dataSource = view.dataSource,
                newRecord,
                newRecordIndex;

            if (recordIndex == null) {
                me.record = me.recordIndex = null;
            } else {
                if (typeof recordIndex === 'number') {
                    newRecordIndex = Math.max(Math.min(recordIndex, dataSource.getCount() - 1), 0);
                    newRecord = dataSource.getAt(recordIndex);
                }
                // row is a Record
                else if (recordIndex.isEntity) {
                    newRecord = recordIndex;
                    newRecordIndex = dataSource.indexOf(recordIndex);
                }
                // row is a grid row
                else if (recordIndex.tagName) {
                    newRecord = view.getRecord(recordIndex);
                    newRecordIndex = dataSource.indexOf(newRecord);
                }
                else {
                    newRecord = newRecordIndex = null;
                }
            }

            // No movement; return early. Do not push current position into previous position, do not fire events.
            if (newRecordIndex === me.recordIndex) {
                return;
            }

            if (me.item) {
                me.item.removeCls(me.focusCls);
            }

            // Track the last position.
            // Used by SelectionModels as the navigation "from" position.
            me.previousRecordIndex = me.recordIndex;
            me.previousRecord = me.record;
            me.previousItem = me.item;

            // Update our position
            me.recordIndex = newRecordIndex;
            me.record      = newRecord;

            // Maintain lastFocused, so that on non-specific focus of the View, we can focus the correct descendant.
            if (newRecord) {
                me.focusPosition(me.recordIndex);
            } else {
                me.item = null;
            }

            if (!suppressEvent) {
                selModel.fireEvent('focuschange', selModel, me.previousRecord, me.record);
            }

            // If we have moved, fire an event
            if (!fromSelectionModel && keyEvent && me.record !== me.previousRecord) {
                me.fireNavigateEvent(keyEvent);
            }
        },

        /**
         * @private
         * Focuses the currently active position.
         * This is used on view refresh and on replace.
         */
        focusPosition: function(recordIndex) {
            var me = this;

            if (recordIndex != null && recordIndex !== -1) {
                if (recordIndex.isEntity) {
                    recordIndex = me.boundList.dataSource.indexOf(recordIndex);
                }
                me.item = me.boundList.all.item(recordIndex);
                if (me.item) {
                    me.lastFocused = me.record;
                    me.focusItem(me.item);
                } else {
                    me.record = null;
                }
            } else {
                me.item = null;
            }
        },

        focusItem: function(item) {
            var me = this,
                boundList = me.boundList;

            if (typeof item === 'number') {
                item = boundList.all.item(item);
            }
            if (item) {
                item = item.dom;
                boundList.highlightItem(item);
                boundList.getOverflowEl().scrollChildIntoView(item, false);
            }
        }
        });
    },

    overrideElement : function(){
        Ext.override( Ext.dom.Element, {
            scrollChildIntoView: function(child, hscroll) {
            // scrollFly is used inside scrollInfoView, must use a method-unique fly here
            Ext.fly(child).scrollIntoView(this, hscroll);
        }
        });
    },

    overrideHtmlEditor : function() {
        Ext.override(Ext.form.field.HtmlEditor,
        {
            markInvalid: function(  )
            {   
                Ext.form.field.Text.superclass.markInvalid.apply(this, arguments );
                this.bodyEl.addCls( 'custom-htmleditor-invalid' );
            },
          

            alignErrorIcon: function()
            {
                try{
                    this.errorIcon.alignTo(this.wrap, 'tl-tr', [2, 0]);
                }
                catch(err){}
            },

            clearInvalid: function()
            {
                if(!this.rendered || this.preventMark)
                {
                    return;
                }
                Ext.form.field.Text.superclass.clearInvalid.apply(this, arguments);
                this.bodyEl.removeCls( 'custom-htmleditor-invalid' );
                // Ext.get(this.iframe).removeClass(this.invalidClass);
                //return Ext.form.TextArea.superclass.clearInvalid.call(this);
            },

            validate:function(){
                if(!this.rendered || this.preventMark)
                {
                    return;
                }
                if(this.allowBlank==false && !this.readOnly)
                {
                    if(!this.isEmpty())
                    {
                        this.clearInvalid();
                        return true;
                    }
                    else
                    {
                        this.markInvalid("This field is required")
                        return false;
                    }
                }
                else
                {
                    this.clearInvalid();
                    return true;
                }
            },

            isEmpty:function(){
                var value =this.getValue();
                value = value.replace(/&nbsp;/gi,"");
                value = value.replace(/<p>/gi,"");
                value = value.replace(/<p align=left>/gi,"");
                value = value.replace(/<p align=right>/gi,"");
                value = value.replace(/<p align=center>/gi,"");
                value = value.replace(/<.p>/gi,"");
                value = value.replace(/<br>/gi,"");
                value = value.trim();
                if(value != '')
                    return false;
                return true;
            },

            setError: function(error){
                var me = this,
                    msgTarget = me.msgTarget,
                    prop;
                    
                if (me.rendered) {
                    if (msgTarget == 'title' || msgTarget == 'qtip') {
                        prop = msgTarget == 'qtip' ? 'data-errorqtip' : 'title';
                        me.getActionEl().dom.setAttribute(prop, error || '');
                    } else {
                        me.updateLayout();
                    }
                }
            }
        });
    },

    overrideDataStore : function() {
        var that = this;

        Ext.override( Ext.data.Store, {
            load : function( options ) {
                var me = this,
                    pageSize = me.getPageSize(),
                    connCtrl = that.getController('Connection'),
                    session;

                if( connCtrl.getIsDisconnected() ) {
                    me.fireEvent('load', me, [], false );
                    return;
                }

                if (typeof options === 'function') {
                    options = {
                        callback: options
                    };
                } else if( typeof options !== 'undefined' && options.hasOwnProperty('_proceedToStoreReq') && !options._proceedToStoreReq ){
                    me.fireEvent('load', me, [], false );
                    return;
                } else {
                    options = Ext.apply({}, options);
                }

                // Only add grouping options if grouping is remote
                if (me.getRemoteSort() && !options.grouper && me.getGrouper()) {
                    options.grouper = me.getGrouper();
                }

                if (pageSize || 'start' in options || 'limit' in options || 'page' in options) {
                    options.page = options.page || me.currentPage;
                    options.start = (options.start !== undefined) ? options.start : (options.page - 1) * pageSize;
                    options.limit = options.limit || pageSize;
                }

                options.addRecords = options.addRecords || false;

                if (!options.recordCreator) {
                    session = me.getSession();
                    if (session) {
                        options.recordCreator = session.recordCreator;
                    }
                }

                if( connCtrl.getIsResumedFromDisconnection() ) {
                    setTimeout( function() {
                        connCtrl.setIsResumedFromDisconnection( false );
                        me.superclass.load.call( me, options );
                    }, 3000);
                } else {
                    return me.callParent([options]);
                }
            }
        });
    },

    overrideGridTriFilter : function() {

        Ext.override( Ext.grid.filters.filter.TriFilter, {
            setValue: function (value, suppressEvents) {
                var me = this,
                    owner = me.owner,
                    menuItem = owner.activeFilterMenuItem,
                    fields = me.fields,
                    filters = me.filter,
                    add = [],
                    remove = [],
                    active = false,
                    filterCollection = me.store.getFilters(),
                    field, filter, v, i, len;

                if (me.settingValue) {
                    return;
                }

                me.settingValue = true;

                if ('eq' in value) {
                    if (filters.lt.getValue()) {
                        remove.push(fields.lt);
                    }

                    if (filters.gt.getValue()) {
                        remove.push(fields.gt);
                    }

                    if (value.eq) {
                        v = me.convertValue(value.eq, /*convertToDate*/ false);

                        add.push(fields.eq);
                        filters.eq.setValue(v);
                    } else {
                        remove.push(fields.eq);
                    }
                } else {
                    if (filters.eq.getValue()) {
                        remove.push(fields.eq);
                    }

                    if ('lt' in value) {
                        if (value.lt) {
                            v = me.convertValue(value.lt, /*convertToDate*/ false);

                            add.push(fields.lt);
                            filters.lt.setValue(v);
                        } else {
                            remove.push(fields.lt);
                        }
                    }

                    if ('gt' in value) {
                        if (value.gt) {
                            v = me.convertValue(value.gt, /*convertToDate*/ false);

                            add.push(fields.gt);
                            filters.gt.setValue(v);
                        } else {
                            remove.push(fields.gt);
                        }
                    }
                }

                if (remove.length || add.length) {
                    filterCollection.beginUpdate();

                    if (remove.length) {
                        for (i = 0, len = remove.length; i < len; i++) {
                            field = remove[i];
                            filter = field.filter;

                            field.setValue(null);
                            filter.setValue(null);
                            me.removeStoreFilter(filter);
                        }
                    }
                    
                    if (add.length) {
                        for (i = 0, len = add.length; i < len; i++) {
                            me.addStoreFilter(add[i].filter);
                        }

                        active = true;
                    }

                    filterCollection.endUpdate();
                }

                if (!active && filterCollection.length) {
                    active = me.hasActiveFilter();
                }

                if (!active || !me.active) {
                    // me.setActive(active, suppressEvents || false );
                    if (me.active !== active) {
                        me.active = active;
                        
                        if (active) {
                            me.activate( true );
                        } else {
                            me.deactivate();
                        }

                        // Make sure we update the 'Filters' menu item.
                        if (menuItem && menuItem.activeFilter === me) {
                            menuItem.setChecked(active, suppressEvents);
                        }

                        me.column[active ? 'addCls' : 'removeCls'](owner.filterCls);

                        // TODO: fire activate/deactivate
                    }
                }

                me.settingValue = false;
            }
        });

        Ext.override( Ext.grid.filters.filter.Date, {
            onMenuSelect: function (picker, date) {
                var fields = this.fields,
                    field = fields[picker.itemId],
                    gt = fields.gt,
                    lt = fields.lt,
                    eq = fields.eq,
                    v = {};

                field.up('menuitem').setChecked(true, /*suppressEvents*/ true);

                if (field === eq) {
                    lt.up('menuitem').setChecked(false, true);
                    gt.up('menuitem').setChecked(false, true);
                } else {
                    eq.up('menuitem').setChecked(false, true);
                    if (field === gt && (+lt.value < +date)) {
                        lt.up('menuitem').setChecked(false, true);
                    } else if (field === lt && (+gt.value > +date)) {
                        gt.up('menuitem').setChecked(false, true);
                    }
                }

                v[field.filterKey] = date;
                this.setValue(v, true);

                picker.up('menu').hide();
            }
        });
    }
});