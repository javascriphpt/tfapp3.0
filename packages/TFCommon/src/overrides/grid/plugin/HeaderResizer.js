// a fix for column header resize
Ext.define('TFCommon.overrides.grid.plugin.HeaderResizer', {
    extend: 'Ext.app.Controller',

    init: function() {
            this.initOverrideHeaderResizeCol();
    },

    initOverrideHeaderResizeCol: function () {

        Ext.override(Ext.grid.plugin.HeaderResizer, {
            // get the region to constrain to, takes into account max and min col widths
            getConstrainRegion: function() {
                var me       = this,
                    dragHdEl = me.dragHd.el,
                    rightAdjust = 0,
                    nextHd,
                    lockedGrid,
                    ownerGrid = me.ownerGrid,
                    widthModel = ownerGrid.getSizeModel().width,
                    // maxColWidth = me.headerCt.getWidth() - me.headerCt.visibleColumnManager.getColumns().length * me.minColWidth;
                    maxColWidth = widthModel.shrinkWrap ? me.headerCt.getWidth() - me.headerCt.visibleColumnManager.getColumns().length * me.minColWidth : me.maxColWidth,
                    result;

                // If forceFit, then right constraint is based upon not being able to force the next header
                // beyond the minColWidth. If there is no next header, then the header may not be expanded.
                if (me.headerCt.forceFit) {
                    nextHd = me.dragHd.nextNode('gridcolumn:not([hidden]):not([isGroupHeader])');
                    if (nextHd && me.headerInSameGrid(nextHd)) {
                        rightAdjust = nextHd.getWidth() - me.minColWidth;
                    }
                }

                // If resize header is in a locked grid, the maxWidth has to be 30px within the available locking grid's width
                else if ((lockedGrid = me.dragHd.up('tablepanel')).isLocked) {
                    rightAdjust = me.dragHd.up('[scrollerOwner]').getTargetEl().getWidth() - lockedGrid.getWidth() - (lockedGrid.ownerLockable.normalGrid.visibleColumnManager.getColumns().length * me.minColWidth + Ext.getScrollbarSize().width);
                }

                result = me.adjustConstrainRegion(
                    dragHdEl.getRegion(),
                    0,
                    0,
                    0,
                    me.minColWidth - me.xDelta
                );

                // fix for adjusting column
                result.right = dragHdEl.getX() + maxColWidth;

                return result;
            }
        });
    }
});