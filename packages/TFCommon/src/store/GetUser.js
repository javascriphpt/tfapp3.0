/**
 * Created on 12/12/2014.
 *
 * Contains only and only single record that reference to the user used by the application
 */
'use strict';
Ext.define( 'TFCommon.store.GetUser', {
    extend  : 'Ext.data.Store',
    model   : 'TFCommon.model.GetUser',
    storeId : 'GetUser',
    proxy   : {
        type: 'memory',
        reader: {
            type: 'json'
        }
    }
} );