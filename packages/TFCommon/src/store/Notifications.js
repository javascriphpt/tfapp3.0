'use strict';

Ext.define( 'TFCommon.store.Notifications', {
    extend  : 'Ext.data.Store',
    model   : 'TFCommon.model.Notification',
    autoLoad: false,
    pageSize: -1,
    storeId : 'Notifications',
    proxy   : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url + '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id    : 807,
            filter: ''
        },
        reader       : {
            rootProperty   : 'data',
            totalProperty  : 'total',
            messageProperty: 'message',
            successProperty: 'success'
        }
    }
} );