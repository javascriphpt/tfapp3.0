'use strict';
Ext.define( 'TFCommon.store.PreferenceType', {
    extend  : 'Ext.data.Store',
    model   : 'TFCommon.model.PreferenceType',
    storeId : 'preferencetype',
    alias   : 'store.preferencetype',
    autoload: true,
    data    : [
        {
            id      : 1,
            name    : 'Notifications',
            apptype : 'appnotifconfig'
        }, {
            id      : 2,
            name    : 'Grid',
            apptype : 'appgridconfig'
        }
    ]
} );