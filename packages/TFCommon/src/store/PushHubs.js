'use strict';
Ext.define( 'TFCommon.store.PushHubs', {
    extend: 'Ext.data.Store',
    model : 'TFCommon.model.PushHub',
    storeId : 'PushHubs',
    proxy : {
        type  : 'memory',
        reader: {
            type: 'json'
        }
    }
} );