﻿'use strict';
Ext.define( 'TFCommon.store.User', {
    extend   : 'Ext.data.Store',
    model    : 'TFCommon.model.User',
    storeId  : 'User',
    pageSize : 0,
    autoLoad : false,
    proxy    : {
        type         : 'ajax',
        url: TFApps.config.dataRequest.url + '/TFData/Get',
        extraParams: {
            id: 624
        },
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },

        reader: {
            rootProperty   : 'data',
            totalProperty  : 'total',
            messageProperty: 'message',
            successProperty: 'success'
        }
    }

} );