'use strict';
Ext.define( 'TFCommon.store.UserActions', {
    extend   : 'Ext.data.Store',
    model    : 'TFCommon.model.UserAction',
    storeId  : 'TFUserAction',
    pageSize: 0,
    autoLoad : false,
    proxy    : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url + '/TFData/GetSubActions',
        extraParams  : {
            id: 624
        },
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },

        reader: {
            rootProperty   : 'data',
            totalProperty  : 'total',
            messageProperty: 'message',
            successProperty: 'success'
        }
    }
} );