﻿'use strict';
Ext.define( 'TFCommon.store.UserMenu', {
    extend  : 'Ext.data.Store',
    model   : 'TFCommon.model.UserMenu',
    storeId : 'TFUserMenu',
    pageSize: 0,
    autoLoad: false,
    proxy   : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url + '/TFData/GetUserMenu',
        extraParams  : {
            id: 624
        },
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },

        reader: {
            rootProperty   : 'data',
            totalProperty  : 'total',
            messageProperty: 'message',
            successProperty: 'success'
        }
    }
} );