'use strict';
Ext.define( 'TFCommon.store.UserPreferences', {
    extend  : 'Ext.data.Store',
    model   : 'TFCommon.model.UserPreference',
    storeId : 'UserPreferences',
    autoload:false,
    remoteSort: true,
    sorters   : {
        property : 'SortSeq',
        direction: 'ASC'
    },
    pageSize  : -1,
    proxy     : {
        type         : 'ajax',
        url          : TFApps.config.dataRequest.url + '/TFData/Get',
        actionMethods: {
            create : 'POST',
            read   : 'POST',
            update : 'POST',
            destroy: 'POST'
        },
        extraParams  : {
            id    : 693,
            filter: ''
        },
        reader       : {
            rootProperty   : 'data',
            totalProperty  : 'total',
            messageProperty: 'message',
            successProperty: 'success'
        }
    }
} );