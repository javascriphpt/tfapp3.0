'use strict';

Ext.define('TFCommon.util.ActivityMonitor', {
	singleton   : true,

	alternateClassName: ['ActivityMonitor'],

	requires :  [
		'TFCommon.view.util.Timeout'
	],

    ui          : null,
    
    ready       : false,
    verbose     : false,
    maxInactive : (1000 * 60 * 60 ), //60 min

    timeout 	: null,
    
    init : function(config) {
        if (!config) { config = {}; }
        
        Ext.apply(this, config, {
            ui			: Ext.getBody(),
            maxInactive	: config.maxInactive || this.maxInactive
        });
        
        this.ready = true;
    },
    
    isReady : function() {
        return this.ready;
    },
    
    isActive   : Ext.emptyFn,

    isInactive : function() {

    	Ext.create('TFCommon.view.util.Timeout').show();

    },
    
    start : function() {

        if (!this.isReady()) {
            this.log('Please run ActivityMonitor.init()');
            return false;
        }
        
        this.ui.on('mousemove', this.captureActivity, this);
        this.ui.on('keydown', this.captureActivity, this);
        
        this.log('ActivityMonitor has been started.');

        this.captureActivity();
        
    },
    
    captureActivity : function(eventObj, el, eventOptions) {

    	var that = this;

        this.log('USER HAS EVENT/S');

        if( this.timeout ) {
        	clearTimeout( this.timeout );
        }

        this.timeout = setTimeout( function() {
        	if (!that.isReady()) {
	            that.log('Please run ActivityMonitor.init()');
	            return false;
	        }
	        
	        that.ui.clearListeners();
	        /*that.ui.un('mousemove', that.captureActivity);
	        that.ui.un('keydown', that.captureActivity);*/
	        
	        if( that.timeout ) {
	        	clearTimeout( that.timeout );
	        }

	        that.log('ActivityMonitor has been stopped.');

	        that.isInactive();
        }, this.maxInactive );
    },
    
    log : function(msg) {
        if (this.verbose) {
            window.console.log(msg);
        }
    }
});