'use strict';
Ext.define( 'TFCommon.util.FileDownloader', {
    extend: 'Ext.Component',
    alias : 'widget.FileDownloader',

    autoEl: {
        tag: 'iframe',
        cls: 'x-hidden',
        src: Ext.SSL_SECURE_URL
    },

    loadByPost: function ( config ) {

        config = config || {};

        var url    = config.url,
            method = 'POST',
            params = config.params || {};


        var form = Ext.create('Ext.form.Panel', {
            standardSubmit: true,
            url           : url,
            method        : method
        } );

        form.submit( {
//            target: '_blank',
            params: params
        } );

        Ext.defer( function() {
            form.close();
        }, 100);
    },

    loadByGet: function ( config ) {

        var e = this.getEl();
        var errorMsg = 'The document was not found on the server.';

        e.dom.src = config.url + (config.params ? '?' + Ext.urlEncode( config.params ) : '');

        e.dom.onload = function () {

            if ( e.dom.contentDocument.body.childNodes[0].wholeText === '404' ) {

                if ( config.errorMsg ) {
                    errorMsg = config.errorMsg;
                }

                Ext.Msg.show( {
                    title  : 'Download',
                    msg    : errorMsg,
                    buttons: Ext.Msg.OK,
                    icon   : Ext.MessageBox.ERROR
                } );

            }

        };
    }
} );