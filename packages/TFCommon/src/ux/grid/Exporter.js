'use strict';

Ext.define( 'TFCommon.ux.grid.Exporter', {
    singleton : true,
    alternateClassName: ['Export'],

    store : null,
    grid : null,

    csvHeaders : [],
    gridColHeaders : [],

    maxRecordsPerFile : 10000,

    forReset : false,

    init : function() {
        this.initialized = true;

        this.characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
        this.fromCharCode = String.fromCharCode;

        this.csvSeparator = ',';
        this.csvDelimiter = ",";
        this.csvNewLine = "\r\n";

        this.uri = {
            excel: 'data:application/vnd.ms-excel;charset=UTF-8;base64,',
            csv: 'data:application/csv;charset=UTF-8;base64,'
        };

        this.template = {
            excel: '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
        };



        this.INVALID_CHARACTER_ERR = this.invalidCharacterError();
        this.initEncoder();
        this.initDecoder();
    },

    base64 : function( string ) {
        return window.btoa( window.unescape( encodeURIComponent( string ) ) );
    },

    format : function(s, c) {
        return s.replace( new RegExp("{(\\w+)}", "g"), function( m, p ) {
            return c[p];
        });
    },

    invalidCharacterError : function() {
        try {
            document.createElement('$');
        } catch (error) {
            return error;
        }
    },

    initEncoder : function() {
        if (!window.btoa) {
            window.btoa = this._btoa;
        }
    },

    _btoa : function (string) {
        var i       = 0,
            len     = string.length,
            max     = Math.max,
            result  = '',
            a,
            b,
            b1,
            b2,
            b3,
            b4,
            c;

        while (i < len) {
            a = string.charCodeAt(i++) || 0;
            b = string.charCodeAt(i++) || 0;
            c = string.charCodeAt(i++) || 0;

            if (max(a, b, c) > 0xFF) {
                throw this.INVALID_CHARACTER_ERR;
            }

            b1 = (a >> 2) & 0x3F;
            b2 = ((a & 0x3) << 4) | ((b >> 4) & 0xF);
            b3 = ((b & 0xF) << 2) | ((c >> 6) & 0x3);
            b4 = c & 0x3F;

            if (!b) {
                b3 = b4 = 64;
            } else if (!c) {
                b4 = 64;
            }

            result += this.characters.charAt(b1) + this.characters.charAt(b2) + this.characters.charAt(b3) + this.characters.charAt(b4);
        }
        return result;
    },

    initDecoder : function() {
        if (!window.atob) {
            window.atob = this._atob;
        }
    },

    _atob : function(string) {
        string = string.replace(new RegExp("=+$"), '');

        var i       = 0,
            len     = string.length,
            chars   = [],
            a,
            b,
            b1,
            b2,
            b3,
            b4,
            c;

        if (len % 4 === 1) {
            throw this.INVALID_CHARACTER_ERR;
        }

        while (i < len) {
            b1 = this.characters.indexOf(string.charAt(i++));
            b2 = this.characters.indexOf(string.charAt(i++));
            b3 = this.characters.indexOf(string.charAt(i++));
            b4 = this.characters.indexOf(string.charAt(i++));

            a = ((b1 & 0x3F) << 2) | ((b2 >> 4) & 0x3);
            b = ((b2 & 0xF) << 4) | ((b3 >> 2) & 0xF);
            c = ((b3 & 0x3) << 6) | (b4 & 0x3F);

            chars.push(fromCharCode(a));
            b && chars.push(fromCharCode(b));
            c && chars.push(fromCharCode(c));
        }

        return chars.join('');
    },

    fixCSVField : function(value) {
        var fixedValue = value;
        var addQuotes = (value.indexOf( this.csvDelimiter) !== -1) || (value.indexOf('\r') !== -1) || (value.indexOf('\n') !== -1);
        var replaceDoubleQuotes = (value.indexOf('"') !== -1);

        if (replaceDoubleQuotes) {
            fixedValue = fixedValue.replace(/"/g, '""');
        }
        if (addQuotes || replaceDoubleQuotes) {
            fixedValue = '"' + fixedValue + '"';
        }
        return fixedValue;
    },

    to : function( config ) {
        var type            = config.type,
            filename        = config.filename,
            delimiter       = config.delimiter,
            newLine         = config.newLine,
            store           = config.store,
            dataRequestId   = config.dataRequestId,
            filter          = config.filter,
            grid            = config.grid,
            that            = this,
            page            = 1,
            file,
            newStore,
            storeRecordsCount,
            numIteration,
            timeOut,
            newFileName;


        this.grid = grid;

        if( !this.initialized ) {
            this.init();
        }

        newStore = Ext.create( Ext.getClassName( store ), {
            pageSize    : that.maxRecordsPerFile,

            extraParams : {
                id      : dataRequestId
            }
        });

        newStore.on( 'beforeload', function( store, op ) {
            op.setParams( {
                filter: filter,
                page   : page,
                start  : (page - 1) * this.maxRecordsPerFile,
                limit  : this.maxRecordsPerFile
            } );

        }, this);

        storeRecordsCount = this._getStoreTotalRecordsCount( store );

        if( storeRecordsCount > this.maxRecordsPerFile ) {
            numIteration = this._getNumIterations( storeRecordsCount );

            this.gridColHeaders = [];

            newStore.on( 'load', function( store, records, successful ) {

                if( successful && !Ext.isEmpty( records ) ) {
                    if( page == numIteration ) {
                        this.forReset = true;
                    } else {
                        this.forReset = false;
                    }

                    file        = filename.split('.');
                    file[0]     = file[0].concat('_').concat( page );
                    newFileName = file.join('.');

                    switch( type ) {
                        case 'csv':
                            that._toCSV.call( that, newFileName, delimiter, newLine );
                            break;
                    }

                    page++;

                    if( timeOut ) {
                        clearTimeout( timeOut );
                    }

                    if( page <= numIteration ) {

                        timeOut = setTimeout( function(){

                            that.gridColHeaders = [];

                            newStore.load();
                        }, 500);
                    } else {
                        newStore.clearListeners();
                    }
                } else {
                    TFCommon.util.Exception.logError(
                        TFAppConfig.error.msgView.PROMPT,
                        TFAppConfig.error.code.SESS_ERR,
                        'Download Failed',
                        'Error downloading file/s. Please check network connection.',
                        function () { }
                    );

                }

            }, this);

            newStore.load();

        } else {
            this.forReset = true;
            newStore.on( 'load', function( store, records, successful ) {

                if( successful && !Ext.isEmpty( records ) ) {
                    switch( type ) {
                        case 'csv':
                            that._toCSV.call( that, filename, delimiter, newLine );
                            break;
                    }
                } else {
                    TFCommon.util.Exception.logError(
                        TFAppConfig.error.msgView.PROMPT,
                        TFAppConfig.error.code.SESS_ERR,
                        'Download Failed',
                        'Error downloading file/s. Please check network connection.',
                        function () { }
                    );
                }

                newStore.clearListeners();
            }, this);

            newStore.load();
        }

        that.store = newStore;
    },

    _getStoreTotalRecordsCount : function( store ) {
        return store.getTotalCount();
    },

    _getNumIterations  : function( storeRecordsTotalCount ) {
        var numIterations = 0;

        numIterations = parseInt( storeRecordsTotalCount / this.maxRecordsPerFile );

        if( storeRecordsTotalCount % this.maxRecordsPerFile > 0 ) {
            numIterations++;
        }

        return numIterations;
    },

    getStoreToCsvData : function() {
        var csvHeaders = this.getGridHeaders(),
            store = this.store,
            records = store.getRange(),
            len = records.length,
            csvContent = [],
            csvData = [],
            record,
            index,
            property,
            content;

        csvData.push( csvHeaders );

        for( index = 0; index < len; index++ ) {
            record = records[ index ];
            csvContent = [];

            for( property in this.gridColHeaders ) {
                if( property in this.gridColHeaders ) {

                    content = record.get( this.gridColHeaders[ property ].dataIndex );

                    if( content && content !== null && content !== undefined ) {
                        content = this.fixCSVField( content + '' );
                    } else {
                        content = '';
                    }

                    csvContent.push( content );
                }
            }

            csvContent = csvContent.join( this.csvDelimiter );


            csvData.push( csvContent );
        }

        csvData = csvData.join( this.csvNewLine );

        return csvData;
    },

    getGridHeaders : function() {
        var colHeaders = this.grid.query('gridcolumn:not([hidden]):not([isGroupHeader])'),
            colHeader = {},
            len = colHeaders.length,
            csvHeaders = [],
            index,
            text,
            dataIndex,
            xtype;


        for( index = 0; index < len; index++ ) {
            text = colHeaders[ index ].text;
            dataIndex = colHeaders[ index ].dataIndex;
            xtype = colHeaders[ index ].xtype;

            if( xtype !== 'actioncolumn' && xtype !== 'checkcolumn' && dataIndex ) {

                colHeader = {
                    text : text,
                    dataIndex : dataIndex
                };

                this.gridColHeaders.push( colHeader );

                csvHeaders.push( text );
            }
            
        }

        csvHeaders = csvHeaders.join( this.csvDelimiter );

        return csvHeaders;
    },

    _toCSV : function( filename, delimiter, newLine ) {

        var csvData;

        if (delimiter !== undefined && delimiter) {
            this.csvDelimiter = delimiter;
        }

        if (newLine !== undefined && newLine) {
            this.csvNewLine = newLine;
        }

        csvData = '\uFEFF' + this.getStoreToCsvData();

        if ( Ext.browser.is.IE || Ext.browser.is.Safari ) {
            // IE specific code here
            this._exportToCSVIE( filename, csvData );
        } else {
            this._exportToCSVWebkit( filename, csvData );
        }

        if( this.forReset ) {
            this.resetData();
        }
    },

    _exportToCSVIE : function( filename, csvData ) {

        if( window.navigator.msSaveOrOpenBlob ) {
            var fileData    = [ csvData ];
            var blobObject  = new Blob( fileData );

            window.navigator.msSaveOrOpenBlob(blobObject, filename);
        } else {
            var win = window.open();
            var doc = win.document;

            doc.open( 'application/csv','replace' );
            doc.charset = "UTF-8";
            doc.write(  'sep=' + this.csvDelimiter + this.csvNewLine + csvData );
            doc.close();

            doc.execCommand( 'SaveAs', false, filename );

            win.close();
        }
    },

    _exportToCSVWebkit : function( filename, csvData ) {
        var link        = document.createElement('a');
        var hrefvalue   = this.uri.csv + this.base64( csvData );
        
        link.download   = filename;
        link.href       = hrefvalue;
        link.target   = '_blank';

        document.body.appendChild( link );

        link.click();

        document.body.removeChild( link );
    },

    resetData : function() {

        this.initialized = false;
        this.store = null;
        this.grid = null;

        this.gridColHeaders = [];
    }
});