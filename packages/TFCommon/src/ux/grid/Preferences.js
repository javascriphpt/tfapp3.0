'use strict';

Ext.define( 'TFCommon.ux.grid.Preferences', {
    extend: 'Ext.AbstractPlugin',
    alias : 'plugin.tf-gridPreferences',

    pluginId: 'tf-gridPreferences',

    init : function( component ) {

        this.gridChangesApplied = false;
        this.submitGridChangeEnabled = false;

        component.on('columnresize', this.onGridColumnsChanged, this);
        component.on('columnschanged', this.onGridColumnsChanged, this);

        this.refreshTimeout = null;

    },

    getGridPreferencesController : function(){
        return TFAppConfig.getApp().getApplication().getController( 'GridPreferences' );
    },

    loadGridChanges : function( cb ) {

        var cmp = this.getCmp(),
            xtype = cmp.getXType(),
            alternateXType,
            callback;

        if( xtype === 'gridpanel' || xtype === 'grid' || cmp._dynamicColumns) {
            alternateXType = cmp.getReference();
        } else {
            alternateXType = null;
        }

        if( !cmp._dynamicColumns || alternateXType !== null ) {

            this.gridChangesApplied = false;
            this.submitGridChangeEnabled = false;

            callback = cb || this.afterGridChangesLoaded;

            this.getGridPreferencesController().loadGridChanges( this, cmp, alternateXType, callback );
        }
    },

    afterGridChangesLoaded : function( that ){
        setTimeout( function() {
            that.gridChangesApplied = true;
            that.submitGridChangeEnabled = true;
        }, 500);
    },

    setSubmitGridChangeEnabled : function( value ) {
        this.submitGridChangeEnabled = value;
    },

    onGridColumnsChanged: function ( ct ) {

        var cmp = this.getCmp(),
            xtype = cmp.getXType(),
            condition;

        if( xtype === 'gridpanel' || xtype === 'grid' || cmp._dynamicColumns ) {
            cmp = cmp.getReference();
        } else {
            cmp = xtype;
        }


        // Eventhough the implementation inside this condition is similar, they have different purposes.
        // this.submitGridChangeEnabled is only enabled on firstload of the grid. At first load, this will check if
        // Save All Grid changes from Account preferences is set, then this.submitGridChangeEnabled will be set to true.
        // However, if user will uncheck Save All Grid changes from Account preferences, this.submitGridChangeEnabled will
        // not be set to true since this is only triggered on first load. So for the preferences to take effect, a condition
        // is added to check if checkbox is really checked or not
        // 
        //      !!TFAppConfig.preferences.appgridconfig && TFAppConfig.preferences.appgridconfig.length > 0 
        //  
        //  Therefore, if submitGridChangeEnabled is true and checkbox for grid config is checked, then save to preferences.
        //  If submitGridChangeEnabled is true, but checkbox for grid config is unchecked (unchecked on the way), then don't save to preferences.
        //  
        //  If submitGridChangeEnabled is false, don't save preferences, but if  submitGridChangeEnabled is false and then
        //  checkbox for grid config is checked (checked on the way), then save to preferences.

        condition = [
            this.submitGridChangeEnabled,
            TFAppConfig.preferences.appgridconfig,
            TFAppConfig.preferences.appgridconfig.length > 0
        ];

        // This is the original condition, not removing this for rollover purposes
        // if( 
        //     (
        //         this.submitGridChangeEnabled    && 
        //         ( !!TFAppConfig.preferences.appgridconfig && TFAppConfig.preferences.appgridconfig.length > 0 )
        //     )   ||
        //     (
        //         !this.submitGridChangeEnabled   &&
        //         ( !!TFAppConfig.preferences.appgridconfig && TFAppConfig.preferences.appgridconfig.length > 0 )
        //     )
        // ) {
        //     this.getGridPreferencesController().saveGridChanges( cmp, ct );
        // }

        if( !Ext.Array.contains( condition, false ) ) {
            this.getGridPreferencesController().saveGridChanges( cmp, ct );
        }
    },

    refresh : function( config, loadConfig ) {
        var refreshBtnRef = config.refreshBtn,
            store         = config.store,
            interval      = config.interval,
            addConditions = config.addConditions || [ true ],
            that          = this,
            conditions;

        loadConfig = loadConfig || {};

        if( this.refreshTimeout ) {
            clearTimeout( this.refreshTimeout );
        }
        
        this.refreshTimeout = setTimeout( function() {

            if ( that.getCmp() && !Ext.isEmpty( refreshBtnRef ) ) {

                conditions = [
                    that.getCmp().isVisible(),
                    refreshBtnRef.isVisible(),
                    refreshBtnRef.pressed,
                    !refreshBtnRef.isDisabled()
                ];


                // console.log( '@screen', that.getCmp() , that.getCmp().isVisible() );

                conditions = Ext.Array.merge( conditions, addConditions );

                if(  !Ext.Array.contains( conditions, false ) ) {
                    
                    store.load( loadConfig );    
                }
            }
        }, interval );
    }

} );
