'use strict';
Ext.define( 'TFCommon.view.app.HeaderController', {
    extend: 'Ext.app.ViewController',
    alias : 'controller.headerController',
    id    : 'HeaderViewController',

    init: function () {

    },

    config: {
        control: {
            '#': {
                beforerender: 'onBeforeRenderPanel'
            },

            '#userAccountBtn' : {
                beforerender : 'onMyAccountBtnBeforeRender'
            }
        }
    },

    onBeforeRenderPanel: function () {

        this.setPanelViewModelData();

        // setup header label help
        if ( this.setupHeaderHelpBtn && Ext.typeOf( this.setupHeaderHelpBtn ) === 'function') {
            this.setupHeaderHelpBtn();
        }
    },

    onMyAccountBtnBeforeRender : function( btn ) {
        var getLocale = TFCommon.Locales.getLocaleValue,
            userStore = Ext.getStore('User').first(),
            btnText;

        if ( TFCommon.Locales.showByName() ) {
            btnText = userStore.get( 'LglEntityName' ) + ' - ' + userStore.get( 'UserName' ) + ' - ' + getLocale( 'Header.Actions.MyAccount', 'My Account' );
        } else {
            btnText = userStore.get( 'LglEntityCode' ) + ' - ' + userStore.get( 'UserCode' ) + ' - ' + getLocale( 'Header.Actions.MyAccount', 'My Account' );
        }

        btn.setText( btnText );
    },

    setPanelViewModelData: function () {

        var getLocale = TFCommon.Locales.getLocaleValue;

        this.getViewModel().setData( {
            labelHelp     : getLocale( 'Header.Actions.Help', 'Help' ),
            labelLogout   : getLocale( 'Header.Actions.LogOut', 'Logout' ),
            labelTheme    : getLocale( 'Theme.Label', 'Theme' )
        } );

    },

    

    onGetUserInfoSuccess : function( result ) {

        var appTitle    = TFAppConfig.appHeaderTitle;
        var getLocale   = TFCommon.Locales.getLocaleValue;
        var headerLogo  = this.view.lookupReference('logo');
        var headerCls;
        var headerKey;

        if( result.hasOwnProperty( 'UserRoleHome' ) && result.UserRoleHome != null && !Ext.isEmpty(result.UserRoleHome) ) {
            headerKey = result.UserRoleHome.split(',');
            headerCls = headerKey[0];

            if( headerKey[1].indexOf('ptrader') > -1 ) {
                appTitle = getLocale( headerKey[1], 'Web Front');
            } else if( headerKey[1].indexOf('rtrader') > -1 ) {
                appTitle = getLocale( headerKey[1], 'My Page');
            } else {
                appTitle = getLocale( TFAppConfig.defaultHeader.key, TFAppConfig.defaultHeader.value);
            }
        } else {
            if ( TFAppConfig && TFAppConfig.defaultHeader ) {
                headerCls = TFAppConfig.defaultHeader.cls;
                appTitle = getLocale( TFAppConfig.defaultHeader.key, TFAppConfig.defaultHeader.value);
            }
        }

        headerLogo.removeCls('defaultLogo');
        headerLogo.addCls(headerCls);

        console.log('appTitle', appTitled);
        this.view.getViewModel().setData( {
            appTitle: appTitle
        } );
    },

    onGetUserInfoFailure : function() {

    },

    // App switcher menu

    onRenderMenuAppSwitch: function ( menu ) {
        var userApps = TFAppConfig.getApp().getApplication().getController( 'Permission' ).getUserApps();
        var appMenu = [];

        Ext.Array.each(userApps, function (appRec) {
            appMenu.push({
                'MenuItemName': appRec.get( 'MenuItemName' ),
                'URL'         : appRec.get( 'URL' )
            } );
        });

        this.loadMenuSwitchItems( appMenu, menu );

    },

    loadMenuSwitchItems: function ( menuRecords, menuCmp ) {

        var that = this;

        menuCmp.removeAll();

        Ext.each(menuRecords, function ( menu ) {
            menuCmp.add( [
                {
                    text     : menu.MenuItemName,
                    glyph: null,
                    listeners: {
                        click: function () {
                            that.onClickMenuAppSwitch( menu.URL );
                        },
                        scope: that
                    }
                }
            ] );
        } );

    },

    onClickMenuAppSwitch: function ( url ) {

        var queryString = Ext.Object.toQueryString(
            Ext.apply(Ext.Object.fromQueryString(location.search) )
        );

        window.open( '../' + url + '/?'+ queryString );

    },

    onMyAccountBtnClick : function() {
        var userRecord = Ext.getStore( 'User' ).first();

        if ( userRecord !== null ) {
            // show Account form panel
            Ext.widget( 'User' ).show();
        }
    },

    getSessionController : function () {
         return TFAppConfig.getApp().app.getController('Session');
     },

    onSelectComboLocale  : function ( combo, rec ) {
        // Locale.js controller should listen to this one
        this.fireEvent( 'selectcombolocale', {
            combo : combo,
            rec   : rec
        });
    },

    onBeforeRenderComboLocale: function ( combo ) {
        // Locale.js controller should listen to this event
        this.fireEvent( 'beforerendercombolocale', combo );
    },

    onClickUserLogoutBtn : function () {

        // Session.js controller should listen to this event.
        this.fireEvent( 'userlogout' );
    },

    onBeforeRenderThemeSwitcherCombo: function ( cbo ) {
        var theme = location.href.match(/theme=([\w-]+)/);
            theme = (theme && theme[1]) || 'standard';

        var getLocale = TFCommon.Locales.getLocaleValue;
        var themes    = [],
            that = this,
            selected = 'standard';

        var currentAppThemes = [], currentTheme;

        if ( TFAppConfig.theme.show === true ) {

            // by default: cbo is hidden, it only show when config theme.show is true
            cbo.show();

            if ( Ext.typeOf( TFAppConfig.theme.themes ) === 'array' ) {

                Ext.each( TFAppConfig.theme.themes, function ( item ) {

                    currentTheme = item.defaultValue.toLowerCase();

                    if ( item.defaultTheme === true ) {
                        that.defaultTheme = currentTheme;
                    }

                    if ( !Ext.Array.contains( currentAppThemes, currentTheme )  ) {
                        // this holds the current available themes of the application
                        currentAppThemes.push( currentTheme );

                        themes.push( {
                            name : item.defaultValue.toLowerCase(),
                            value: getLocale( item.resourceKey, item.defaultValue )
                        } );
                    }

                } );

                if ( themes.length ) {
                    // load available themes
                    cbo.getStore().loadData( themes );

                    if ( Ext.Array.contains( currentAppThemes, theme ) ) {
                        selected = theme;
                    }

                    // set default theme
                    cbo.setValue( selected );

                }
            }

        }
    },

    onSelectThemeSwitcher: function ( cbo ) {
        var theme   = cbo.getValue();
        var defaultTheme = this.defaultTheme || 'standard';

        if ( theme !== defaultTheme ) {
            this.setQueryParam( {theme: theme} );
        } else {
            this.removeQueryParam( 'theme' );
        }
    },

    setQueryParam: function ( param ) {
        var queryString = Ext.Object.toQueryString(
            Ext.apply(Ext.Object.fromQueryString(location.search), param)
        );

        location.search = queryString;
    },

    removeQueryParam: function ( paramName ) {
        var params = Ext.Object.fromQueryString( location.search );

        delete params[paramName];

        location.search = Ext.Object.toQueryString( params );

    },

    onToggleNavigationSize: function () {
        var me = this,
            // refs = me.getReferences(),
            // navigationList = refs.navigationTreeList,
            // wrapContainer = refs.mainContainerWrap,
            navigationList = Ext.ComponentQuery.query('#monitorNavMenu')[0],
            wrapContainer = Ext.ComponentQuery.query('#mainContainerWrap')[0],
            // senchaLogo = Ext.ComponentQuery.query('#monitorLeftPanel'),
            collapsing = !navigationList.getMicro(),
            new_width = collapsing ? 64 : 250;


        if (Ext.isIE9m || !Ext.os.is.Desktop) {
            Ext.suspendLayouts();

            // refs.senchaLogo.setWidth(new_width);

            navigationList.setWidth(new_width);
            navigationList.setMicro(collapsing);

            Ext.resumeLayouts(); // do not flush the layout here...

            // No animation for IE9 or lower...
            wrapContainer.layout.animatePolicy = wrapContainer.layout.animate = null;
            wrapContainer.updateLayout();  // ... since this will flush them
        }
        else {
            if (!collapsing) {
                // If we are leaving micro mode (expanding), we do that first so that the
                // text of the items in the navlist will be revealed by the animation.
                navigationList.setMicro(false);
            }

            // Start this layout first since it does not require a layout
            // refs.senchaLogo.animate({dynamic: true, to: {width: new_width}});

            // Directly adjust the width config and then run the main wrap container layout
            // as the root layout (it and its chidren). This will cause the adjusted size to
            // be flushed to the element and animate to that new size.
            navigationList.width = new_width;
            wrapContainer.updateLayout({isRoot: true});
            navigationList.el.addCls('nav-tree-animating');

            // We need to switch to micro mode on the navlist *after* the animation (this
            // allows the "sweep" to leave the item text in place until it is no longer
            // visible.
            if (collapsing) {
                navigationList.on({
                    afterlayoutanimation: function () {
                        navigationList.setMicro(true);
                        navigationList.el.removeCls('nav-tree-animating');
                    },
                    single: true
                });
            }
        }
    }
} );