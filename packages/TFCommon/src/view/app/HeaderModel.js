'use strict';
Ext.define( 'TFCommon.view.app.HeaderModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.headerModel',

    data: {
        appTitle: ''
    }
} );