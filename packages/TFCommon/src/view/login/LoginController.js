'use strict';
Ext.define( 'TFCommon.view.login.LoginController', {
    extend: 'Ext.app.ViewController',
    alias : 'controller.loginController',
    id : 'LoginViewController',

    requires : [
        'TFCommon.util.Cookies'
    ],

    config: {
        control: {
            'form'                     : {
                afterrender            : 'onAfterRenderForm'
            },
            'form #fieldLabelUserName' : {
                afterrender            : 'onAfterRenderFieldUserName'
            }
        }
    },

    init: function () {
        // var loginField = /^[a-zA-Z0-9]+([\!\.\_]*[a-zA-Z0-9]*)+$/i;
        var loginField = /^[a-zA-Z0-9\#\$\@\,\!\.\_]+$/i;
        var getLocale = TFCommon.Locales.getLocaleValue;

        Ext.apply(Ext.form.field.VTypes, {
            //  vtype validation function
            loginField: function( val ) {
                return loginField.test( val );
            },
            // vtype Text property: The error text to display when the validation function returns false
            loginFieldText: getLocale( 'Login.Error.InvalidANSIChar', 'Invalid input. Must be valid ANSI characters' )
        });
    },

    /**
     * @version 2.2, update for 3.0
     * @description setup username text field before render
     * @param  {Object} txtfield Textfield
     */
    onUsernameBeforeRender : function( txtfield ) {
        var appName = TFAppConfig.AppCode.replace('.','').toLowerCase(),
            uname   = TFCommon.util.Cookies.get('uname'),
            index,
            len;

        if( uname ) {
            uname = uname.split('%2C');
            len = uname.length;

            for( index = len; index--; ) {
                if( uname[ index ].indexOf( appName ) > -1 ) {
                    uname = uname[ index ].split('%3A');
                    txtfield.setValue( uname[1]);

                    break;
                }
            }
        }

    },

    /**
     * @version 2.2, update for 3.0
     * @description Setup locale combobox
     * @param  {Object} combo combobox
     */
    onBeforeRenderComboLocale: function ( combo ) {
        this.fireEvent( 'beforerendercombolocale', combo );
    },

    /**
     * @version 2.2, update for 3.0
     * @param  {Object} combo combobox
     * @param  {Object} rec record
     */
    onSelectComboLocale: function ( combo, rec ) {

        this.setUsernameSession();
        this.fireEvent( 'selectcombolocale', {
            combo : combo,
            rec   : rec
        } );
    },

    /**
     * @version 2.2, update for 3.0
     * @param {String} responseAppName optional
     */
    setUsernameSession : function( responseAppName ) {
        var appName = TFAppConfig.AppCode.replace('.','').toLowerCase(),
            fieldLabelUserName   = this.lookupReference( 'fieldLabelUserName' ),
            unameSess = appName + ':' + fieldLabelUserName.getValue();

        if( appName !== 'tflogin') {
            unameSess += ',tflogin:' + fieldLabelUserName.getValue();
        } else {
            if( responseAppName ) {
                unameSess += ',' + responseAppName.replace('.','').toLowerCase() + ':' + fieldLabelUserName.getValue();
            }
        }

        TFCommon.util.Cookies.set( 'uname', unameSess );
    },

    /**
     * @version 2.2, update to 3.0
     * @description setup theme combobox
     * @param  {Object} cbo combobox
     */
    onBeforeRenderThemeSwitcherCombo: function ( cbo ) {
        var theme = location.href.match(/theme=([\w-]+)/);
            theme = (theme && theme[1]) || 'standard';

        var getLocale = TFCommon.Locales.getLocaleValue;
        var themes    = [],
            that = this,
            selected = 'standard';

        var currentAppThemes = [], currentTheme;

        if ( TFAppConfig.theme.show === true ) {

            // by default: cbo is hidden, it only show when config theme.show is true
            cbo.show();

            if ( Ext.typeOf( TFAppConfig.theme.themes ) === 'array' ) {

                Ext.each( TFAppConfig.theme.themes, function ( item ) {

                    currentTheme = item.defaultValue.toLowerCase();

                    if ( item.defaultTheme === true ) {
                        that.defaultTheme = currentTheme;
                    }

                    if ( !Ext.Array.contains( currentAppThemes, currentTheme )  ) {
                        // this holds the current available themes of the application
                        currentAppThemes.push( currentTheme );

                        themes.push( {
                            name : item.defaultValue.toLowerCase(),
                            value: getLocale( item.resourceKey, item.defaultValue )
                        } );
                    }
                } );



                if ( themes.length ) {
                    // load available themes
                    cbo.getStore().loadData( themes );

                    if ( Ext.Array.contains( currentAppThemes, theme ) ) {
                        selected = theme;
                    }

                    // set default theme
                    cbo.setValue( selected );

                }
            }

        }
    },

    /**
     * @version 2.2, update to 3.0
     * @description handler for theme selection
     * @param  {Object} combobox
     */
    onSelectThemeSwitcher: function ( cbo ) {
        var theme = cbo.getValue();
        var defaultTheme = this.defaultTheme || 'standard';

        this.setUsernameSession();

        if ( theme !== defaultTheme ) {
            this.setQueryParam( {theme: theme} );
        } else {
            this.removeQueryParam( 'theme' );
        }
    },

    /**
     * @version 2.2, update to 3.0
     * @description handler for special keys on form fields
     *              Calls the form submit on pressing enter
     * @param  {Object} field
     * @param  {Event} event
     */
    onLoginFieldKeyPressSpecial: function ( field, e ) {
        // Check if the enter key is pressed
        if ( e.getKey() === e.ENTER ) {

            var form = this.lookupReference('loginForm');
            // Check if the form is valid
            if ( form.isValid() ) {
                // Call the form submit
                this.onFormSubmit();

            }

        }
    },

    /**
     * @version 2.2, update for 3.0
     * @description setup form after render
     */
    onAfterRenderForm: function () {

        // set the form view model
        this.setFormViewModel();

    },

    /**
     * @version 2.2
     * @description setup view model data
     */
    setFormViewModel: function () {
        var getLocale = TFCommon.Locales.getLocaleValue;

        this.getView().getViewModel().setData( {
            appTitle          : TFAppConfig.appTitle,
            fieldLabelUserName: getLocale( 'Login.Items.UserName', 'User Name' ),
            fieldLabelPassword: getLocale( 'Login.Items.Password', 'Password' ),
            fieldLabelLanguage: getLocale( 'Login.Actions.Language', 'Language' ),
            fieldLabelTheme   : getLocale( 'Theme.Label', 'Theme' ),
            btnLogin          : getLocale( 'Login.Actions.LogIn', 'LogIn' )
        } );
    },

    /**
     * @version 2.2
     * @description focus username text fields after render
     * @param  {Object} textfield
     */
    onAfterRenderFieldUserName: function ( field ) {
        field.focus( false, 500 );
    },

    // Handler for submitting the login form
    /**
     * @return {[type]}
     */
    onFormSubmit: function () {

        var loginForm = this.lookupReference('loginForm');
        var getLocale = TFCommon.Locales.getLocaleValue;

        // Mask the login form
        loginForm.setLoading( {
            msg: getLocale( 'Common.Message.LogginIn', 'Logging in. Please wait...' )
        } );

        this.setUsernameSession();

        // Submit the login form
        loginForm.submit( {
            url    : TFApps.config.dataRequest.url + '/Account/Logon',
            success: function ( form, action ) {
                // Call the login success handler
                this.loginSuccess(loginForm, form, action );
            },
            failure: function ( form, action ) {
                // Unmask the form
                loginForm.setLoading( false );
                // Call the fail login handler
                this.loginFail(form, action );
            },
            scope: this
        } );

    },

    /**
     * @version 2.2
     * @description login success handler
     * @param  {Object} loginFormPanel the login form panel
     * @param  {Object} form the basic form
     * @param  {Object} action the action object which performedt the operation
     */
    loginSuccess: function ( loginFormPanel, form, action ) {

        var response = Ext.decode( action.response.responseText, true );
        var getLocale = TFCommon.Locales.getLocaleValue;
        this.setUsernameSession( response.user.AppName );

        var userApp = {
            AppName: response.user.AppName,
            AppId  : response.user.AppId,
            Url    : response.user.Url
        };
        var that = this;

        if ( response.success ) {

            loginFormPanel.setLoading( {
                msg: getLocale( 'Common.Message.LoadingData', 'Loading user required data...')
            } );

            // Start the user Session
            // Session.js Controller should listen to this event
            this.fireEvent( 'startusersession', response );

            // TFAppConfig.getApp().app.getController( 'Session' ).startUserSession(response);

        } else {

            Ext.Msg.alert( getLocale( 'Login.Alert.TouchFire0', 'TouchFire 0 -> '), response.message );
            Ext.Msg.alert( getLocale( 'Login.Alert.FailureType', 'TouchFire:action.failureType -> '), action.failureType );

        }
    },

    /**
     * @version 2.2, update for 3.0
     * @description login fail handler
     * @param  {Object} form the basic form
     * @param  {Object} action the action the performed the operation
     */
    loginFail: function ( form, action ) {

        var response = Ext.decode( action.response.responseText, true );
        var getLocale = TFCommon.Locales.getLocaleValue;

        switch ( action.failureType ) {
            case Ext.form.action.Action.CLIENT_INVALID:
                Ext.Msg.alert(
                    getLocale( 'Login.Alert.Failure1', 'Failure 1 -> '),
                    getLocale( 'Login.Alert.InvalidValues', 'Form fields may not be submitted with invalid values')
                );
                Ext.Msg.alert( getLocale( 'Login.Alert.TouchFire1', 'TouchFire 1 -> '), response.message );
                break;
            case Ext.form.action.Action.CONNECT_FAILURE:
                Ext.Msg.alert(
                    getLocale( 'Login.Alert.Failure2', 'Failure 2 -> : CONNECT_FAILURE '),
                    getLocale( 'Login.Alert.CommuFailed', 'Ajax communication failed' )
                );
                Ext.Msg.alert( getLocale( 'Login.Alert.TouchFire2', 'TouchFire 2 -> ' ), response.message );
                break;
            case Ext.form.action.Action.SERVER_INVALID:
                Ext.Msg.alert( getLocale('Login.Alert.Error', 'Login.Alert.Error'), response.message );
                break;
            default:
                Ext.Msg.alert(
                    getLocale( 'Login.Alert.TouchFire4', 'TouchFire 4 -> ' ),
                    getLocale( 'Login.Alert.ErrSvrRequest', 'There was an error processing the server request' )
                );

        }
    },

    /**
     * @version 2.2
     * @description set the url query parameters
     * @param {Object} param the query parameter
     */
    setQueryParam: function ( param ) {
        var queryString = Ext.Object.toQueryString(
            Ext.apply(Ext.Object.fromQueryString(location.search), param)
        );

        location.search = queryString;
    },

    /**
     * @version 2.2
     * @description remove url query by name
     * @param  {String} paramName the url parameter query name
     */
    removeQueryParam: function ( paramName ) {
        var params = Ext.Object.fromQueryString( location.search );

        delete params[paramName];

        location.search = Ext.Object.toQueryString( params );

    }
} );