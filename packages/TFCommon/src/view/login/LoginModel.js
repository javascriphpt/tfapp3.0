'use strict';
Ext.define( 'TFCommon.view.login.LoginModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.loginModel',

    data: {
        appTitle          : '',
        fieldLabelUserName: '',
        fieldLabelPassword: '',
        fieldLabelLanguage: '',
        btnLogin          : ''
    }
} );