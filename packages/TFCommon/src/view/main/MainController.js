'use strict';

Ext.define( 'TFCommon.view.main.MainController', {
	extend  : 'Ext.app.ViewController',
	alias   : 'controller.main',
	id 		: 'MainViewController',

	control : {
		'#' : {
			afterrender : 'onAfterRenderMainView'
		}
	},

	onAfterRenderMainView : function() {
		document.getElementById('loading-parent').remove();
		this.fireEvent( 'setupViewport' );
	}
} );