'use strict';

Ext.define('TFCommon.view.util.TimeoutController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.timeoutController',

	config : {
		control : {
			'#' : {
				show : 'onWindowShow'
			}
		},

		startTime : 60
	},

	init : function() {
		this.timeout = null;
		console.log('init timeout controller');
		this.setViewModel();
	},

	onWindowShow : function() {
		this.countDown();
	},

	countDown : function() {
		var count = this.getStartTime(),
			logoffMessage = this.lookupReference( 'logoffMessage' );

		

		if( count > -1 ) {
			logoffMessage.setText('Will log off in ' + count + ' second(s)');
			count--;

			this.setStartTime( count );

			this.timeout = setTimeout( Ext.bind( this.countDown, this ), 1000);
		} else {

			clearTimeout( this.timeout );
			this.getView().close();

			this.getSessionController().onClickUserLogoutBtn();
		}
	},

	onCancelBtnClick : function( btn ) {
		clearTimeout( this.timeout );

		ActivityMonitor.start();
		btn.up('window').close();
	},

	getSessionController : function () {
        return TFAppConfig.getApp().app.getController('Session');
    },

    setViewModel: function () {
        var getLocale = TFCommon.Locales.getLocaleValue;

        this.getView().getViewModel().setData( {
            title          : getLocale( 'ActivityMonitor.Title.Idle', 'Idle' ),
            lblLogoffMsg   : getLocale( 'ActivityMonitor.Label.IdleLogOffMsg', 'Will logoff in 60 seconds' ),
            lblPressCancel : getLocale( 'ActivityMonitor.Label.PressCancel', 'Press \'Cancel\' to remain logged on' )
        } );
    },
});