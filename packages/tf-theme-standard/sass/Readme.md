# tf-theme-standard/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    tf-theme-standard/sass/etc
    tf-theme-standard/sass/src
    tf-theme-standard/sass/var
